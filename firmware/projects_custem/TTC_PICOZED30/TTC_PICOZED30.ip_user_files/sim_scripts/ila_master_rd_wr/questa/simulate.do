onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ila_master_rd_wr_opt

do {wave.do}

view wave
view structure
view signals

do {ila_master_rd_wr.udo}

run -all

quit -force
