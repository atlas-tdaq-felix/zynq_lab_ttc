library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

						
entity i2c_cont_si570 is
	port (
		clk: in std_logic; -- system 50MHz clk
		reset : in std_logic; -- system reset/power on reset
		clk_pls_n: in std_logic; -- negative slow clk pulse, less than 100kHz, this project is 50kHz, 1 cycle width of system clk
		clk_pls_p: in std_logic; -- positive slow clk pulse, less than 100kHz, this project is 50kHz, 1 cycle width of system clk
		slow_clk : in std_logic; -- 50kHz clk to output as smb_clk
		
		smb_clk : out std_logic; -- I2C bus control clk
		smb_data : inout std_logic; -- bidirectional data signal

		address : in std_logic_vector(6 downto 0); -- 7bits device address
		reg_add : in std_logic_vector(7 downto 0);
		write_read : in std_logic; -- 0: read   1:write
		process_done : out std_logic; -- 0: under process   1:process done
		
		data_in : in  std_logic_vector(7 downto 0);
		data_out : out std_logic_vector(7 downto 0)
	);
end i2c_cont_si570;

architecture rtl of i2c_cont_si570 is
	signal data_cont : std_logic; -- data output control signal, 0:hi-z output  1:output data from this device
	
	signal write_cnt, write_read_cnt : std_logic; -- enable/disable sub-function  0:disable  1:enable

	signal smb_clk_out_w, smb_clk_out_rw : std_logic; -- smb_clk. during write, select _w. During read, select _r.
	signal smb_data_out_w, smb_data_out_rw : std_logic; -- smb_data. during write, select _w. During read, select _r.
	signal data_cont_w, data_cont_rw : std_logic; -- data output control signal. during write, select _w. During read, select _r.
	signal send_cmd : std_logic_vector(7 downto 0); -- command value
	signal send_data : std_logic_vector(7 downto 0); -- write data value
	signal write_done : std_logic; -- write process done signal 0:under process  1:process has done
	signal smb_data_out : std_logic; -- smb_data shared with read/write sub-functions
	signal short_write : std_logic; -- enable short write sequence, used to send only cmd value 0: regular sequence  1:short sequence

	signal data_out_rw : std_logic_vector(7 downto 0);
	signal write_read_done : std_logic;
	
	component i2c_write
		port(
			clk: in std_logic;
			reset : in std_logic;
			clk_pls_n: in std_logic;
			clk_pls_p: in std_logic;
			slow_clk : in std_logic;
			address : in std_logic_vector(6 downto 0);
	
			smb_data_in : in std_logic;
			smb_clk_out : out std_logic;
			smb_data_out : out std_logic;
			data_cont : out std_logic;
			
			short_write : in std_logic;
			send_cmd : in std_logic_vector(7 downto 0);
			send_data : in std_logic_vector(7 downto 0);
			
			write_done : out std_logic
		);
	end component;
	
	component i2c_write_read
	port (
		clk: in std_logic;
		reset : in std_logic;
		clk_pls_n: in std_logic;
		clk_pls_p: in std_logic;
		slow_clk : in std_logic;
		address : in std_logic_vector(6 downto 0);

		smb_data_in : in std_logic;
		smb_clk_out : out std_logic;
		smb_data_out : out std_logic;
		data_cont : out std_logic;
		
		data_out : out std_logic_vector(7 downto 0);
		send_cmd : in std_logic_vector(7 downto 0);
		send_data : in std_logic_vector(7 downto 0);

		write_read_done : out std_logic
	);
	end component;
	
begin


	write_data : i2c_write  port map (
		clk, write_cnt, clk_pls_n, clk_pls_p, slow_clk, address, --inputs
		smb_data, smb_clk_out_w, smb_data_out_w, data_cont_w,
		short_write, send_cmd, send_data, 
		write_done);

	write_read_data : i2c_write_read  port map (
		clk, write_read_cnt, clk_pls_n, clk_pls_p, slow_clk, address, --inputs
		smb_data, smb_clk_out_rw, smb_data_out_rw, data_cont_rw, -- input
		data_out_rw,  -- output
		send_cmd, send_data, --input 
		write_read_done);

	

	-- data can go out only when data_cont is '1'
	-- when data_cont is 0, which is reading data from lm95235
	-- when it is reading data, it needs to tri-state
	process(reset, data_cont, smb_data_out)begin
		if(reset = '0')then
			smb_data <= 'Z';	
		elsif(data_cont = '1')then
			smb_data <= smb_data_out;
		else
			smb_data <= 'Z';
		end if;
	end process;


	process(clk, reset)begin
		if(reset = '0')then
			write_cnt <= '0';
			short_write <= '0';
			data_cont <= '0';
			process_done <= '0';
   			smb_clk <= '1'; -- added scl state during reset (undefefined) wv 17-12-2012
		elsif(clk'event and clk = '1')then
			case write_read is
			when '0' => -- read
				write_read_cnt <= '1';
				send_cmd <= reg_add; -- comman/register address
				send_data <= data_in; --dummy data
				smb_clk <= smb_clk_out_rw;
				smb_data_out <= smb_data_out_rw;
				data_cont <= data_cont_rw;
				if(write_read_done = '1')then
					data_out <= data_out_rw;
					write_read_cnt <= '0';
					process_done <= '1';
				end if;
			when '1' => -- write
				write_cnt <= '1';
				send_cmd <= reg_add;
				send_data <= data_in; -- data to write
				smb_clk <= smb_clk_out_w;
				smb_data_out <= smb_data_out_w;
				data_cont <= data_cont_w;
				short_write <= '0';
				if(write_done = '1')then
					write_cnt <= '0';
					process_done <= '1';
				end if;
			when others =>
			end case;
		end if;
	end process;
end rtl;

