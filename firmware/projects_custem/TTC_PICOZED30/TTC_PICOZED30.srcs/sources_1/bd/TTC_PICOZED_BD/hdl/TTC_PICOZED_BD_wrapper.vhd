--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
--Date        : Tue Jul 26 12:29:13 2022
--Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
--Command     : generate_target TTC_PICOZED_BD_wrapper.bd
--Design      : TTC_PICOZED_BD_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TTC_PICOZED_BD_wrapper is
  port (
    BCR_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BCR_out_0 : out STD_LOGIC;
    BUSY_EN : out STD_LOGIC;
    BUSY_PULSE_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BUSY_in_0 : in STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    ECR_out_0 : out STD_LOGIC;
    FCLK_CLK0_0 : out STD_LOGIC;
    FCLK_RESET0_N_0 : out STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    L1A_AUTO_HALT : out STD_LOGIC;
    L1A_AUTO_READY : in STD_LOGIC;
    L1A_COUNT_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_out_0 : out STD_LOGIC;
    LUT_CONT_EN : out STD_LOGIC;
    OCR_COUNT_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    USBIND_0_0_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USBIND_0_0_vbus_pwrfault : in STD_LOGIC;
    USBIND_0_0_vbus_pwrselect : out STD_LOGIC;
    bram_addr_a_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_clk_a_out : in STD_LOGIC;
    bram_en_a_out : in STD_LOGIC;
    bram_rddata_a_in : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rst_a_out : in STD_LOGIC;
    bram_we_a_out : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_wrdata_a_out : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end TTC_PICOZED_BD_wrapper;

architecture STRUCTURE of TTC_PICOZED_BD_wrapper is
  component TTC_PICOZED_BD is
  port (
    FCLK_CLK0_0 : out STD_LOGIC;
    FCLK_RESET0_N_0 : out STD_LOGIC;
    L1A_out_0 : out STD_LOGIC;
    BCR_out_0 : out STD_LOGIC;
    ECR_out_0 : out STD_LOGIC;
    BCR_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BUSY_in_0 : in STD_LOGIC;
    OCR_COUNT_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    LUT_CONT_EN : out STD_LOGIC;
    bram_addr_a_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_clk_a_out : in STD_LOGIC;
    bram_en_a_out : in STD_LOGIC;
    bram_rddata_a_in : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rst_a_out : in STD_LOGIC;
    bram_we_a_out : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_wrdata_a_out : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_AUTO_HALT : out STD_LOGIC;
    L1A_AUTO_READY : in STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    USBIND_0_0_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USBIND_0_0_vbus_pwrselect : out STD_LOGIC;
    USBIND_0_0_vbus_pwrfault : in STD_LOGIC;
    BUSY_EN : out STD_LOGIC;
    BUSY_PULSE_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component TTC_PICOZED_BD;
begin
TTC_PICOZED_BD_i: component TTC_PICOZED_BD
     port map (
      BCR_PERIOD_out_0(31 downto 0) => BCR_PERIOD_out_0(31 downto 0),
      BCR_out_0 => BCR_out_0,
      BUSY_EN => BUSY_EN,
      BUSY_PULSE_COUNT(31 downto 0) => BUSY_PULSE_COUNT(31 downto 0),
      BUSY_in_0 => BUSY_in_0,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      ECR_out_0 => ECR_out_0,
      FCLK_CLK0_0 => FCLK_CLK0_0,
      FCLK_RESET0_N_0 => FCLK_RESET0_N_0,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      L1A_AUTO_HALT => L1A_AUTO_HALT,
      L1A_AUTO_READY => L1A_AUTO_READY,
      L1A_COUNT_0(31 downto 0) => L1A_COUNT_0(31 downto 0),
      L1A_COUNT_out_0(31 downto 0) => L1A_COUNT_out_0(31 downto 0),
      L1A_PERIOD_out_0(31 downto 0) => L1A_PERIOD_out_0(31 downto 0),
      L1A_out_0 => L1A_out_0,
      LUT_CONT_EN => LUT_CONT_EN,
      OCR_COUNT_0(31 downto 0) => OCR_COUNT_0(31 downto 0),
      OCR_PERIOD_out_0(31 downto 0) => OCR_PERIOD_out_0(31 downto 0),
      TTC_CONFIG_out_0(31 downto 0) => TTC_CONFIG_out_0(31 downto 0),
      USBIND_0_0_port_indctl(1 downto 0) => USBIND_0_0_port_indctl(1 downto 0),
      USBIND_0_0_vbus_pwrfault => USBIND_0_0_vbus_pwrfault,
      USBIND_0_0_vbus_pwrselect => USBIND_0_0_vbus_pwrselect,
      bram_addr_a_out(31 downto 0) => bram_addr_a_out(31 downto 0),
      bram_clk_a_out => bram_clk_a_out,
      bram_en_a_out => bram_en_a_out,
      bram_rddata_a_in(31 downto 0) => bram_rddata_a_in(31 downto 0),
      bram_rst_a_out => bram_rst_a_out,
      bram_we_a_out(3 downto 0) => bram_we_a_out(3 downto 0),
      bram_wrdata_a_out(31 downto 0) => bram_wrdata_a_out(31 downto 0)
    );
end STRUCTURE;
