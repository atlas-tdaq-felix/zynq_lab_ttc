--!------------------------------------------------------------------------------
--!                                                             
--!           NIKHEF - National Institute for Subatomic Physics 
--!
--!                       Electronics Department                
--!                                                             
--!-----------------------------------------------------------------------------

library ieee, UNISIM, xpm ;
use xpm.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
use ieee.std_logic_1164.all;

entity edge_det is
    port (
      clk_160M    				: in     std_logic;
      sys_rst_n   				: in     std_logic;
      BCR_push    				: in     std_logic;
      ECR_push    				: in     std_logic;
      trigger_push 				: in     std_logic;
      busy_in     				: in     std_logic;
      ECR_r       				: out    std_logic;
      trigger_r    				: out    std_logic;
      BCR_r       				: out    std_logic;
      BCR_count_en				: in    std_logic;
	  BCR_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  OCR_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
      L1A_COUNT_IN          : in     STD_LOGIC_VECTOR ( 31 downto 0 );
	  L1A_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  TTC_CONFIG_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  LUT_CONT_EN			: in std_logic;
	  L1A_COUNT_read		: out 	 STD_LOGIC_VECTOR ( 31 downto 0 );
      L1A_AUTO_HALT 		: in STD_LOGIC;
      L1A_AUTO_READY 		: out STD_LOGIC;
      BUSY_EN				: in std_logic;
      BUSY_PULSE_COUNT		: out STD_LOGIC_VECTOR ( 31 downto 0 );
	  enb 					: OUT STD_LOGIC;
	  web 					: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	  addrb 				: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	  doutb 				: IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
end entity edge_det;


architecture structure of edge_det is

signal trigger_in : std_logic := '0';
signal ECR_in : std_logic := '0';
signal BCR_in : std_logic := '0';

signal trigger_in_d : std_logic := '0';
signal ECR_in_d : std_logic := '0';
signal BCR_in_d : std_logic := '0';

signal trigger_debounce : std_logic_vector(15 downto 0) := (others => '0');
signal ECR_debounce : std_logic_vector(15 downto 0) := (others => '0');
signal BCR_debounce : std_logic_vector(15 downto 0) := (others => '0');

signal trigger_push_debounce : std_logic_vector(15 downto 0) := (others => '0');
signal ECR_push_debounce : std_logic_vector(15 downto 0) := (others => '0');
signal BCR_push_debounce : std_logic_vector(15 downto 0) := (others => '0');

signal trigger_t : std_logic := '0';
signal ECR_t : std_logic := '0';
signal BCR_t : std_logic := '0';
signal trigger_push_t : std_logic := '0';
signal ECR_push_t : std_logic := '0';
signal BCR_push_t : std_logic := '0';
signal trigger			 :      std_logic;
signal ECR                 :      std_logic;
signal BCR                 :      std_logic;
signal sys_rst          :      std_logic;
signal BCR_mux             :      std_logic;
signal ECR_mux             :      std_logic;
signal triger_mux         :      std_logic;

signal trigger_auto      :      std_logic := '0';
signal BCR_auto         :      std_logic := '0';
signal TTC_CONFIG_IN_L1A_d  :      std_logic_vector(1 downto 0) := "00";
signal L1A_PERIOD_COUNT : std_logic_vector(31 downto 0) := (others => '0');
signal L1A_TRIGGER_COUNT : std_logic_vector(31 downto 0) := (others => '0');
signal BCR_PERIOD_COUNT : std_logic_vector(31 downto 0) := (others => '0');
signal L1A_LUT_COUNT : std_logic_vector(31 downto 0) := (others => '0');
constant L1A_LUT_COUNT_MAX : std_logic_vector(31 downto 0) := X"00001000";
signal L1A_LUT_COUNT_MAX_d : std_logic_vector(36 downto 0);
signal L1A_AUTO_HALT_d : std_logic:='0';
signal L1A_COUNT_read_d : std_logic_vector(31 downto 0);
signal trigger_lut : std_logic := '0';
signal  enb_t 	: STD_LOGIC;
signal  enb_t_d 	: STD_LOGIC;
signal  enb_t_dd 	: STD_LOGIC;
signal  addrb_t 	: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal  doutb_t 	: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal  CLK40M_en	: std_logic := '0';
signal lut_count : integer range 0 to 32 := 0;
signal busy_all : std_logic := '0';
signal  BUSY_PULSE_COUNT_t 	: STD_LOGIC_VECTOR(31 DOWNTO 0);
signal trigger_in_xpm : STD_LOGIC := '0';
signal ECR_in_xpm : STD_LOGIC := '0';

component ila_rd_8t49n242
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
   );
 end component;



begin

 xpm_cdc_single_trigger : xpm_cdc_single
   generic map (
      DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      SRC_INPUT_REG => 0   -- DECIMAL; 0=do not register input, 1=register input
   )
   port map (
      dest_out => trigger_in_xpm, -- 1-bit output: src_in synchronized to the destination clock domain. This output
      dest_clk => clk_160M, -- 1-bit input: Clock signal for the destination clock domain.
      src_clk => '0',   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in => trigger_push      -- 1-bit input: Input signal to be synchronized to dest_clk domain.
   );

xpm_cdc_single_ecr : xpm_cdc_single
   generic map (
      DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
      INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      SRC_INPUT_REG => 0   -- DECIMAL; 0=do not register input, 1=register input
   )
   port map (
      dest_out => ECR_in_xpm, -- 1-bit output: src_in synchronized to the destination clock domain. This output
      dest_clk => clk_160M, -- 1-bit input: Clock signal for the destination clock domain.
      src_clk => '0',   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in => ECR_push      -- 1-bit input: Input signal to be synchronized to dest_clk domain.
   );


trigger_in 	<= (trigger_auto or trigger_lut) 	when TTC_CONFIG_IN_L1A_d /= "00" else trigger_in_xpm;
ECR_in 		<= ECR_in_xpm;
BCR_in 		<= BCR_auto;

enb <= enb_t;
addrb <= addrb_t(29 downto 0) & "00";
web <= (others => '0');

--L1A_LUT_COUNT_MAX_d <= L1A_LUT_COUNT_MAX & "00000"; L1A_COUNT_IN
L1A_LUT_COUNT_MAX_d <= L1A_COUNT_IN & "00000"; 
CLK40M_en <= BCR_count_en;

L1A_COUNT_read <= L1A_COUNT_read_d;
L1A_AUTO_HALT_d <= L1A_AUTO_HALT;
busy_all <= BUSY_EN and (not busy_in);
BUSY_PULSE_COUNT <= BUSY_PULSE_COUNT_t;

process(clk_160M, sys_rst)
begin
	if(sys_rst_n = '0') then
	  BCR_in_d <= '0';
	  ECR_in_d <= '0';
	  trigger_in_d <= '0';
	  BCR_auto <= '0';
	  trigger_auto <= '0';
	  TTC_CONFIG_IN_L1A_d <= "00";
	  enb_t <= '1';
	  addrb_t <= (others => '0');
	  doutb_t <= doutb;
	  L1A_COUNT_read_d <= (others => '0');
	  L1A_AUTO_READY <= '0';
	  BUSY_PULSE_COUNT_t <= (others => '0');
	elsif(rising_edge(clk_160M)) then
	
		BCR_in_d <= BCR_in;
		if( BCR_in = '1' and BCR_in_d = '0')  then
		  BCR_r <= '1';
		else
		  BCR_r <= '0';
		end if;

		ECR_in_d <= ECR_in;
		if( ECR_in = '1' and ECR_in_d = '0')  then
		  ECR_r <= '1';
		else
		  ECR_r <= '0';
		end if;

		trigger_in_d <= trigger_in;
		if( trigger_in = '1' and trigger_in_d = '0')  then
		  trigger_r <= '1';
		else
		  trigger_r <= '0';
		end if;
		
		TTC_CONFIG_IN_L1A_d <= TTC_CONFIG_IN(1 downto 0);
		enb_t_d <= enb_t;
		enb_t_dd <= enb_t_d;
		case TTC_CONFIG_IN_L1A_d is
			when "00" =>
					L1A_LUT_COUNT <= (others => '0');
					L1A_TRIGGER_COUNT <= (others => '0');
				    L1A_PERIOD_COUNT <= (others => '0');
				    lut_count <= 0;
					trigger_auto <= '0';
					trigger_lut <= '0';
					addrb_t <= (others => '0');
					enb_t <= '1';
					doutb_t <= doutb;
				    L1A_AUTO_READY <= '0';
			when "01" =>
			    trigger_lut <= '0';
				if(CLK40M_en = '1' and L1A_AUTO_HALT_d = '0' and busy_all = '0') then
					if(L1A_COUNT_IN = X"00000000") then
						if(L1A_PERIOD_IN = X"00000000") then
							L1A_PERIOD_COUNT <= (others => '0');
							trigger_auto <= '0';
						else
							if(L1A_PERIOD_COUNT < L1A_PERIOD_IN) then
								L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT + 1;
								trigger_auto <= '0';
							else
								L1A_PERIOD_COUNT <= (others => '0');
								trigger_auto <= '1';
							end if;
						end if;	
					else
						if(L1A_PERIOD_IN = X"00000000") then
							L1A_PERIOD_COUNT <= (others => '0');
							L1A_TRIGGER_COUNT <= (others => '0');
							trigger_auto <= '0';
						else
							if(L1A_PERIOD_COUNT < L1A_PERIOD_IN) then
								L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT + 1;
								trigger_auto <= '0';
							else
								if(L1A_TRIGGER_COUNT < L1A_COUNT_IN) then
									L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT + 1;
									L1A_PERIOD_COUNT <= (others => '0');
									trigger_auto <= '1';
								else
									L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT;
									L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT;
								    L1A_AUTO_READY <= '1';
									trigger_auto <= '0';
								end if;    
							end if;	
						end if;
					end if;  
				else
					L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT;
				    L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT;
					trigger_auto <= '0';
				end if;	
			when "10" =>
				trigger_auto <= '0';
				if(CLK40M_en = '1' and L1A_AUTO_HALT_d = '0' and busy_all = '0') then
					if(doutb_t = X"00000000") then
						if(addrb_t < L1A_COUNT_IN) then
							L1A_LUT_COUNT <= (others => '0');
							addrb_t <= addrb_t + 1;
							enb_t <= '1';
						else
							L1A_LUT_COUNT <= L1A_LUT_COUNT;
							addrb_t <= addrb_t;
							enb_t <= '0';
					    end if;		
					elsif(L1A_LUT_COUNT < doutb_t - 1) then
						L1A_LUT_COUNT <= L1A_LUT_COUNT + 1;
						trigger_lut <= '0';
						enb_t <= '0';
					else
						if(addrb_t < L1A_COUNT_IN) then
							L1A_LUT_COUNT <= (others => '0');
							addrb_t <= addrb_t + 1;
							enb_t <= '1';
							trigger_lut <= '1';
						else
							if(LUT_CONT_EN = '0') then
								L1A_LUT_COUNT <= L1A_LUT_COUNT;
								addrb_t <= addrb_t;
								enb_t <= '0';
								trigger_lut <= '0';
							    L1A_AUTO_READY <= '1';
							else
								L1A_LUT_COUNT <= (others => '0');
								addrb_t <= (others => '0');
								enb_t <= '1';
								trigger_lut <= '1';
							end if;	
						end if;	
					end if;    
				else
					L1A_LUT_COUNT <= L1A_LUT_COUNT;
					addrb_t <= addrb_t;
					trigger_lut <= '0';
					enb_t <= '0';
					if(enb_t_dd = '1') then
						doutb_t <= doutb;
					else
						doutb_t <= doutb_t;
					end if;
				end if;	
			when others =>
					L1A_LUT_COUNT <= (others => '0');
				    L1A_PERIOD_COUNT <= (others => '0');
					trigger_lut <= '0';
					trigger_auto <= '0';
					addrb_t <= (others => '0');
					enb_t <= '0';
					L1A_TRIGGER_COUNT <= (others => '0');
		end case;	
       
		if(CLK40M_en = '1') then
			if(BCR_PERIOD_IN = X"00000000") then
				BCR_PERIOD_COUNT <= (others => '0');
				BCR_auto <= '0';
			else
				if(BCR_PERIOD_COUNT < BCR_PERIOD_IN - 1) then
					BCR_PERIOD_COUNT <= BCR_PERIOD_COUNT + 1;
					BCR_auto <= '0';
				else
					BCR_PERIOD_COUNT <= (others => '0');
					BCR_auto <= '1';
				end if; 
			end if;
		else
			BCR_PERIOD_COUNT <= BCR_PERIOD_COUNT;
			BCR_auto <= '0';
		end if;
		
		if(TTC_CONFIG_IN_L1A_d /= "00") then
		  if(trigger_auto = '1' or trigger_lut = '1') then
		    L1A_COUNT_read_d <= L1A_COUNT_read_d + 1;
		  else
		    L1A_COUNT_read_d <= L1A_COUNT_read_d;
		  end if;  
		else			
			L1A_COUNT_read_d <= (others => '0');
		end if;
		

		if(busy_all = '1' and CLK40M_en = '1') then
	  		BUSY_PULSE_COUNT_t <= BUSY_PULSE_COUNT_t + 1;
		else			
			BUSY_PULSE_COUNT_t <= BUSY_PULSE_COUNT_t;
		end if;
		
			
	end if;
end process;	

end architecture structure; 
