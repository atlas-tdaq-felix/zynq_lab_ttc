--------------------------------------------------------------------------------
-- Object        : Entity work.si570_master
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------



library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use xil_defaultlib.TTC709_pkg.all;

entity TTCBRAM_Interface is
  port (
    clka         : in     std_logic;
    Addra        : in     std_logic_vector(12 downto 0);
    Douta        : out    std_logic_vector(31 downto 0);
    Dina         : in     std_logic_vector(31 downto 0);
    ena          : in     std_logic;
    wea    		 : in     std_logic_vector(3 downto 0);
    rsta		 : in     std_logic;
    
    clkb         : in     std_logic;
    reg0         : out    std_logic_vector(7 downto 0);
    reg1         : out    std_logic_vector(7 downto 0);
    valid	 	 : out 	  std_logic);	
end entity TTCBRAM_Interface;

architecture rtl of TTCBRAM_Interface is

constant reg0_addr : std_logic_vector(12 downto 0) := '0' & X"000";
constant reg1_addr : std_logic_vector(12 downto 0) := '0' & X"001";

begin

	Douta <= (others => '1');
	valid <= '1';

	rd_proc:process(clka, rsta) 
	begin
		if(clka'event and clka = '1')then 
			if(rsta = '0') then
				if(wea(0) = '1' and ena = '1') then
					case Addra is
						when reg0_addr =>
							reg0 <= Dina(7 downto 0);
						when reg1_addr =>
							reg1 <= Dina(7 downto 0);
						when others => NULL;
					end case;
				end if;
			end if;
		end if;
	end process;
	
end architecture rtl ;
