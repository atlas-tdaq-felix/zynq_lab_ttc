vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xilinx_vip
vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm

vmap xilinx_vip questa_lib/msim/xilinx_vip
vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm

vlog -work xilinx_vip -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_2 -L axi_vip_v1_1_2 -L processing_system7_vip_v1_0_4 -L xilinx_vip "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/include" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_2 -L axi_vip_v1_1_2 -L processing_system7_vip_v1_0_4 -L xilinx_vip "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/include" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/home/mesfin/tools/xilinx/Vivado/2018.1/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_common_reset.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_common.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_gt_usrclk_source.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_support.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/TTC_GTX2ZYNQ7000/example_design/ttc_gtx2zynq7000_tx_startup_fsm.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/TTC_GTX2ZYNQ7000/example_design/ttc_gtx2zynq7000_rx_startup_fsm.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_init.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_cpll_railing.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_gt.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000_multi_gt.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/TTC_GTX2ZYNQ7000/example_design/ttc_gtx2zynq7000_sync_block.vhd" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/TTC_GTX2ZYNQ7000/ttc_gtx2zynq7000.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

