--------------------------------------------------------------------------------
-- Object        : Entity work.si570_master
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------



library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use xil_defaultlib.TTC709_pkg.all;

entity i2c_8t49n242_rd_wr is
  port (
		clk       : IN     STD_LOGIC;                    --system clock
		reset_n   : IN     STD_LOGIC;                    --active low reset
		ena       : OUT    STD_LOGIC;                    --latch in command
		addr      : OUT    STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
		rw        : OUT    STD_LOGIC;                    --'0' is write, '1' is read
		rd_en_pb  : in 	   std_logic;	
		wr_en_pb  : in     std_logic;    
		data_wr   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
		busy      : IN     STD_LOGIC;                    --indicates transaction in progress
		data_rd   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
		ack_error : IN     STD_LOGIC);                    --flag if improper acknowledge from slave
end entity i2c_8t49n242_rd_wr;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_master.rtl
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------


architecture rtl of i2c_8t49n242_rd_wr is
--	constant dev_address : std_logic_vector(6 downto 0):= "1110000"; --virtex eval kit
--	constant dev_address : std_logic_vector(6 downto 0):= "1011101"; --kintex eval kit
--	constant dev_address : std_logic_vector(6 downto 0):= "1101000"; --si5324 1101A(2)A(1)A(0)eval kit
	constant dev_address : std_logic_vector(6 downto 0):= "1101100"; --t49n242
    signal clk_8t49n242_reg : clk_8t49n242_reg_type := clk_8t49n242_init;
	signal wr_state: std_logic_vector(5 downto 0);
	signal rd_state: std_logic_vector(5 downto 0);



component ila_test_rd_8t49n242
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe6 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
     probe7 : IN STD_LOGIC_VECTOR(5 DOWNTO 0)
   );
 end component;

	
component ila_rd_8t49n242
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
     probe6 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
     probe7 : IN STD_LOGIC_VECTOR(255 DOWNTO 0)
   );
 end component;

SIGNAL  rd_wr_state : INTEGER := 0;
CONSTANT  idle : INTEGER := 0;
CONSTANT  read_data : INTEGER := 1;
CONSTANT  write_data : INTEGER := 2;
CONSTANT  read_cnt_max : INTEGER := 8;
SIGNAL busy_cnt : INTEGER := 0;
SIGNAL read_cnt : INTEGER := 0;

SIGNAL busy_prev : STD_LOGIC := '0';
SIGNAL i2c_busy  : STD_LOGIC := '0';
SIGNAL i2c_ena  : STD_LOGIC := '0';
SIGNAL slave_addr : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1101100";
SIGNAL i2c_rw  : STD_LOGIC := '0';
SIGNAL i2c_data_wr : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL data_to_write : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"44";
SIGNAL new_data_to_write : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"A5";
SIGNAL i2c_data_rd : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL data : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL rd_en_pb_d : STD_LOGIC := '0';
SIGNAL wr_en_pb_d : STD_LOGIC := '0';
SIGNAL addr_offset0 : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
SIGNAL addr_offset1 : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
SIGNAL i2c_addr : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1101100";
	       
signal i_cnt, j_cnt : integer;
signal i_wrcnt, j_wrcnt : integer;
constant i_cnt_max : integer := 24; -- max array size
constant j_cnt_max : integer := 32; -- max number of bytes
constant i_wrcnt_max : integer := 3; -- max array size
constant j_wrcnt_max : integer := 32; -- max number of bytes

signal wr_8t49n242_reg    : clk_8t49n242_reg_type := clk_8t49n242_init;
	       
	       
	       
	       
begin

--ila_rd_8t49n242_inst: ila_rd_8t49n242
--  PORT map(
--      clk => clk,
--      probe0(0) => Ready, 
--      probe1(0) => read_read,
--      probe2 => core_state,
--      probe3 => rd_8t49n242_reg(0),
--      probe4 => rd_8t49n242_reg(1),
--      probe5 => rd_8t49n242_reg(2),
--      probe6 => rd_8t49n242_reg(3),
--      probe7 => rd_8t49n242_reg(4)
--      );
--

--ila_test_rd_8t49n242_inst: ila_test_rd_8t49n242
--  PORT map(
--      clk => clk,
--      probe0(0) => reset_n, 
--      probe1(0) => ack_error,
--      probe2(0) => busy,
--      probe3(0) => rd_en_pb,
--      probe4(0) => wr_en_pb,
--      probe5 	=> data_rd,
--      probe6 	=> wr_state,
--      probe7 	=> rd_state
--      );

    i2c_busy <=  busy;
    ena <= i2c_ena;
    addr <= i2c_addr;
    rw <= i2c_rw;
    data_wr <= i2c_data_wr;
    i2c_data_rd <= data_rd;
    
	PROCESS(reset_n, clk)
	BEGIN
	 IF(reset_n = '0') then
	    rd_wr_state <= 0;
	    busy_cnt <= 0;
	    read_cnt <= 0;
	    i_wrcnt <= 0;
	    j_wrcnt <= 0;
	 ELSIF(RISING_EDGE(clk)) THEN
		busy_prev <= i2c_busy;                       --capture the value of the previous i2c busy signal
		rd_en_pb_d <= rd_en_pb;
		wr_en_pb_d <= wr_en_pb;
		CASE rd_wr_state IS
			WHEN idle =>                               
				i2c_ena <= '0';                            --initiate the transaction
				busy_cnt <= 0;
				read_cnt <= 0;
				i_wrcnt <= 0;
				j_wrcnt <= 0;
				IF(rd_en_pb = '1'  AND  rd_en_pb_d = '0' ) THEN
				 rd_wr_state <= read_data;
				ELSIF(wr_en_pb = '1'  AND  wr_en_pb_d = '0' ) THEN
				 rd_wr_state <= write_data;
				END IF; 
			WHEN read_data =>                               --state for conducting this transaction
				CASE busy_cnt IS                             --busy_cnt keeps track of which command we are on
					WHEN 0 =>                                  --no command latched in yet
						i2c_ena <= '1';                            --initiate the transaction
						i2c_addr <= slave_addr;                    --set the address of the slave
						i2c_rw <= '0';                             --command 1 is a write
						i2c_data_wr <= addr_offset0;              --data to be written
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							busy_cnt <= busy_cnt + 1;                    --counts the times busy has gone from low to high during transaction
						END IF;
					WHEN 1 =>                                  --1st busy high: command 1 latched, okay to issue command 2
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '0';                             --command 2 is a write (addr stays the same)
						i2c_data_wr <= addr_offset1;              --data to be written
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							busy_cnt <= busy_cnt + 1;                    --counts the times busy has gone from low to high during transaction
						END IF;
					WHEN 2 =>                                  --2nd busy high: command 2 latched, okay to issue command 3
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '1';                             --command 3 is a write
						i2c_data_wr <= data_to_write;          --data to be written
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							busy_cnt <= busy_cnt + 1;                    --counts the times busy has gone from low to high during transaction
						END IF;
					WHEN 3 =>                                  --3rd busy high: command 3 latched, okay to issue command 4
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '1';                             --command 4 is read (addr stays the same)
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							IF(read_cnt = read_cnt_max) THEN
							  busy_cnt <= 0;
							  rd_wr_state <= idle;                             --transaction complete, go to next state in design
							ELSE
								read_cnt <= read_cnt + 1;                    --counts the times busy has gone from low to high during transaction
							END IF;
						END IF;
					WHEN OTHERS => NULL;
				END CASE;
			WHEN write_data =>                               --state for conducting this transaction
				CASE busy_cnt IS                             --busy_cnt keeps track of which command we are on
					WHEN 0 =>                                  --no command latched in yet
						i2c_ena <= '1';                            --initiate the transaction
						i2c_addr <= slave_addr;                    --set the address of the slave
						i2c_rw <= '0';                             --command 1 is a write
						i2c_data_wr <= addr_offset0;              --data to be written
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							busy_cnt <= busy_cnt + 1;                    --counts the times busy has gone from low to high during transaction
						END IF;
					WHEN 1 =>                                  --1st busy high: command 1 latched, okay to issue command 2
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '0';                             --command 2 is a write (addr stays the same)
						i2c_data_wr <= addr_offset1;              --data to be written
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							busy_cnt <= busy_cnt + 1;                    --counts the times busy has gone from low to high during transaction
						END IF;
					WHEN 2 =>                                  --3rd busy high: command 3 latched, okay to issue command 4
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '0';                             --command 4 is read (addr stays the same)
						i2c_data_wr <= wr_8t49n242_reg(i_wrcnt)( (31 - j_wrcnt) * 8 + 7 downto (31 - j_wrcnt) * 8);
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							if(j_wrcnt < j_wrcnt_max - 1) then
									i_wrcnt <= i_wrcnt;
									j_wrcnt <= j_wrcnt + 1;
							else
									if(i_wrcnt < i_wrcnt_max - 1) then
										i_wrcnt <= i_wrcnt + 1;
										j_wrcnt <= 0;
									else
										j_wrcnt <= 0;
										busy_cnt <= busy_cnt + 1;
										i_wrcnt <= i_wrcnt + 1; -- only 4 reg are nedded
									end if;
							end if;
						END IF;
					WHEN 3 =>                                  --3rd busy high: command 3 latched, okay to issue command 4
						i2c_ena <= '1';                            --initiate the transaction
						i2c_rw <= '0';                             --command 4 is read (addr stays the same)
						i2c_data_wr <= wr_8t49n242_reg(i_wrcnt)( (31 - j_wrcnt) * 8 + 7 downto (31 - j_wrcnt) * 8);
						IF(busy_prev = '0' AND i2c_busy = '1') THEN  --i2c busy just went high
							if(j_wrcnt < j_wrcnt_max - 1 - 16) then -- no factory reg 0x7B
								i_wrcnt <= i_wrcnt;
								j_wrcnt <= j_wrcnt + 1;
							else
								j_wrcnt <= 0;
								i_wrcnt <= 0;	
								rd_wr_state <= idle;                             --transaction complete, go to next state in design
							end if;
						END IF;
					WHEN OTHERS => NULL;
				END CASE;
			WHEN OTHERS => NULL;
		END CASE;
	 END IF;
	END PROCESS;

end architecture rtl ; -- of 8t49n242_master
