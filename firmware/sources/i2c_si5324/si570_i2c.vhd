library ieee, work;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.TTCvi_pkg.all;

entity si5324_i2c is
  port (
    clk             : in     std_logic;
    iic_mux_reset_n : out    std_logic;
    reset_in        : in     std_logic;
    si570_ctrl      : in     si570_ctrl_record;
    si570_iic_clk   : inout  std_logic;
    si570_iic_data  : inout  std_logic;
    si570_reset     : out    std_logic;
    si570_status    : out    si570_status_record);
end entity si5324_i2c;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of si5324_i2c is

  signal start      : std_logic;
  signal stop       : std_logic;
  signal read       : std_logic;
  signal write      : std_logic;
  signal send_ack   : std_logic;
  signal mstr_din   : std_logic_vector(7 downto 0);
  signal ready      : std_logic;
  signal mstr_dout  : std_logic_vector(7 downto 0);
  signal free       : std_logic;
  signal rec_ack    : std_logic;
  signal core_state : std_logic_vector(5 downto 0);
  signal iic_reset  : std_logic;

  component i2c_master_v01
    generic(
      CLK_FREQ : natural := 25000000;
      BAUD     : natural := 100000);
    port (
      sys_clk    : in     std_logic;
      sys_rst    : in     std_logic;
      start      : in     std_logic;
      stop       : in     std_logic;
      read       : in     std_logic;
      write      : in     std_logic;
      send_ack   : in     std_logic;
      mstr_din   : in     std_logic_vector(7 downto 0);
      sda        : inout  std_logic;
      scl        : inout  std_logic;
      free       : out    std_logic;
      rec_ack    : out    std_logic;
      ready      : out    std_logic;
      core_state : out    std_logic_vector(5 downto 0);
      mstr_dout  : out    std_logic_vector(7 downto 0));
  end component i2c_master_v01;

  component ila_i2c
    port (
      clk    	 : in     std_logic;
      probe0    : in     std_logic_vector(0 downto 0);
      probe1    : in     std_logic_vector(0 downto 0);
      probe2    : in     std_logic_vector(0 downto 0));
  end component ila_i2c;

--  component si570_master
--    port (
--      clk          : in     std_logic;
--      core_state   : in     std_logic_vector(5 downto 0);
--      free         : in     std_logic;
--      iic_reset    : out    std_logic;
--      mstr_din     : out    std_logic_vector(7 downto 0);
--      mstr_dout    : in     std_logic_vector(7 downto 0);
--      read         : out    std_logic;
--      ready        : in     std_logic;
--      rec_ack      : in     std_logic;
--      reset_in     : in     std_logic;
--      send_ack     : out    std_logic;
--      si570_ctrl   : in     si570_ctrl_record;
--      si570_reset  : out    std_logic;
--      si570_status : out    si570_status_record;
--      start        : out    std_logic;
--      stop         : out    std_logic;
--      write        : out    std_logic);
--  end component si570_master;

--  component INV_wrapper
--    port (
--      O : out    std_ulogic;
--      I : in     std_ulogic);
--  end component INV_wrapper;

begin

  u1: i2c_master_v01
    generic map(
      CLK_FREQ => 100000000,
      BAUD     => 400000)
    port map(
      sys_clk    => clk,
      sys_rst    => iic_reset,
      start      => start,
      stop       => stop,
      read       => read,
      write      => write,
      send_ack   => send_ack,
      mstr_din   => mstr_din,
      sda        => si570_iic_data,
      scl        => si570_iic_clk,
      free       => free,
      rec_ack    => rec_ack,
      ready      => ready,
      core_state => core_state,
      mstr_dout  => mstr_dout);
--ENTITY i2c_master IS
--  GENERIC(
--    input_clk : INTEGER := 50_000_000; --input clock speed from user logic in Hz
--    bus_clk   : INTEGER := 400_000);   --speed the i2c bus (scl) will run at in Hz
--  PORT(
--    clk       : IN     STD_LOGIC;                    --system clock
--    reset_n   : IN     STD_LOGIC;                    --active low reset
--    ena       : IN     STD_LOGIC;                    --latch in command
--    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
--    rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
--    data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
--    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
--    data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
--    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
--    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
--    scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
--END i2c_master;
--
--  u1: i2c_master
--    generic map(
--      input_clk => 100000000,
--      bus_clk     => 400000)
--    port map(
--      clk    => clk,
--      reset_n    => not iic_reset,
--
--      start      => start,
--      stop       => stop,
--      read       => read,
--      write      => write,
--      send_ack   => send_ack,
--      mstr_din   => mstr_din,
--      sda        => si570_iic_data,
--      scl        => si570_iic_clk,
--      free       => free,
--      rec_ack    => rec_ack,
--      ready      => ready,
--      core_state => core_state,
--      mstr_dout  => mstr_dout);

  the_si5324_master: entity work.si5324_master
    port map(
      clk          => clk,
      core_state   => core_state,
      free         => free,
      iic_reset    => iic_reset,
      mstr_din     => mstr_din,
      mstr_dout    => mstr_dout,
      read         => read,
      ready        => ready,
      rec_ack      => rec_ack,
      reset_in     => reset_in,
      send_ack     => send_ack,
      si570_ctrl   => si570_ctrl,
      si570_reset  => si570_reset,
      si570_status => si570_status,
      start        => start,
      stop         => stop,
      write        => write);

--ila_i2c_inst: ila_i2c
--  PORT map(
--      clk => clk,
--      probe0(0) => si570_iic_data,
--      probe1(0) => si570_iic_clk,
--      probe2(0) => read
--  );

iic_mux_reset_n <= not iic_reset;
--  u2: INV_wrapper
--    port map(
--      O => iic_mux_reset_n,
--      I => iic_reset);
end architecture structure ; -- of si570_i2c
