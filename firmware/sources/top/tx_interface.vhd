--------------------------------------------------------------------------------
--
-- This VHDL file was generated by EASE/HDL 9.1 Revision 5 from HDL Works B.V.
--
-- Ease library  : design
-- HDL library   : design
-- Host name     : mesfin-Precision-T3600
-- User name     : mesfin
-- Time stamp    : Tue Jan 21 13:49:50 2020
--
-- Designed by   : 
-- Company       : 
-- Project info  : 
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Object        : Entity design.tx_interface
-- Last modified : Tue Jan 21 13:41:55 2020
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tx_interface is
  port (
    txdata_in     : out    std_logic_vector(15 downto 0);
    DATA_VALID_IN : out    std_logic;
    TTC_out_0     : in     std_logic);
end entity tx_interface;

--------------------------------------------------------------------------------
-- Object        : Architecture design.tx_interface.rtl
-- Last modified : Tue Jan 21 13:41:55 2020
--------------------------------------------------------------------------------


architecture rtl of tx_interface is

begin

end architecture rtl ; -- of tx_interface

