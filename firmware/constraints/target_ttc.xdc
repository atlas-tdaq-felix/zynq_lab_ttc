
#PB
set_property IOSTANDARD LVCMOS18 [get_ports rd_en_pb]
set_property IOSTANDARD LVCMOS18 [get_ports wr_en_pb]
#set_property IOSTANDARD LVCMOS18 [get_ports BCR_push_0]
#set_property IOSTANDARD LVCMOS18 [get_ports ECR_push_0]
#set_property IOSTANDARD LVCMOS18 [get_ports triger_push_0]

set_property PACKAGE_PIN T16 [get_ports rd_en_pb]
set_property PACKAGE_PIN AB18 [get_ports wr_en_pb]
#set_property PACKAGE_PIN AB19 [get_ports BCR_push_0]
#set_property PACKAGE_PIN AB22 [get_ports triger_push_0]
#set_property PACKAGE_PIN G2 [get_ports ECR_push_0]

#LED
set_property IOSTANDARD LVCMOS18 [get_ports {gpio2_io_o_0[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {gpio2_io_o_0[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {gpio2_io_o_0[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {gpio2_io_o_0[3]}]
#set_property IOSTANDARD LVCMOS18 [get_ports led_out[0]]
#set_property IOSTANDARD LVCMOS18 [get_ports led_out[1]]

set_property PACKAGE_PIN G3 [get_ports {gpio2_io_o_0[0]}]
set_property PACKAGE_PIN AA19 [get_ports {gpio2_io_o_0[1]}]
set_property PACKAGE_PIN AA20 [get_ports {gpio2_io_o_0[2]}]
set_property PACKAGE_PIN AB21 [get_ports {gpio2_io_o_0[3]}]
#set_property PACKAGE_PIN AA19 [get_ports led_out[0]]
#set_property PACKAGE_PIN AA20 [get_ports led_out[1]]


#MGT_TX_RX_REFCLK

set_property PACKAGE_PIN AB9 [get_ports gtxrxn_in]
set_property PACKAGE_PIN AB5 [get_ports gtxtxn_out]
set_property PACKAGE_PIN V5 [get_ports Q0_CLK1_GTREFCLK_PAD_N_IN]

#set_property PACKAGE_PIN Y6 [get_ports gtxrxn_in]
#set_property PACKAGE_PIN Y2 [get_ports gtxtxn_out]
#set_property PACKAGE_PIN V9 [get_ports Q0_CLK0_GTREFCLK_PAD_N_IN]


#CLOCK
#set_property IOSTANDARD LVCMOS18 [get_ports SYSCLK_IN]
#set_property PACKAGE_PIN Y18 [get_ports SYSCLK_IN]

#sd card detect
#set_property IOSTANDARD LVCMOS18 [get_ports SDIO_0_0_cdn]
#set_property PACKAGE_PIN D11 [get_ports SDIO_0_0_cdn]

#ETH0
#set_property IOSTANDARD LMCMOS18 [get_ports MDIO_ETHERNET_0_0_mdc]
#set_property IOSTANDARD LVCMOS18 [get_ports MDIO_ETHERNET_0_0_mdio_io]

#set_property PACKAGE_PIN D13 [get_ports MDIO_ETHERNET_0_0_mdc]
#set_property PACKAGE_PIN C11 [get_ports MDIO_ETHERNET_0_0_mdio_io]

#Clock i2c
set_property IOSTANDARD LVCMOS18 [get_ports iic_clk]
set_property PACKAGE_PIN H6 [get_ports iic_clk]
set_property IOSTANDARD LVCMOS18 [get_ports iic_data]
set_property PACKAGE_PIN H5 [get_ports iic_data]

set_property IOSTANDARD LVCMOS18 [get_ports FMC_JX2_20P]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_JX2_21P]

# pin 5 j15
set_property PACKAGE_PIN N6 [get_ports FMC_JX2_20P]
# pin 3 j15
set_property PACKAGE_PIN J8 [get_ports FMC_JX2_21P]

set_property IOSTANDARD LVCMOS18 [get_ports FMC_JX2_20N]
set_property IOSTANDARD LVCMOS18 [get_ports FMC_JX2_21N]

# pin 2 j15
set_property PACKAGE_PIN K8 [get_ports FMC_JX2_20N]
# pin 4 j15
set_property PACKAGE_PIN N5 [get_ports FMC_JX2_21N]


#set_property IOSTANDARD LVCMOS18 [get_ports PL_clk]
#set_property IOSTANDARD LVCMOS18 [get_ports FMC_JX2_20N]

#set_property PACKAGE_PIN D3 [get_ports PL_clk]
#set_property PACKAGE_PIN B4 [get_ports FMC_JX2_20N]

set_property IOSTANDARD LVCMOS18 [get_ports busy_in]
set_property IOSTANDARD LVCMOS18 [get_ports JA0_1_N]
set_property IOSTANDARD LVCMOS18 [get_ports JA2_3_P]

set_property PACKAGE_PIN AA14 [get_ports busy_in]
set_property PACKAGE_PIN AA15 [get_ports JA0_1_N]
set_property PACKAGE_PIN Y14 [get_ports JA2_3_P]







set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[3]/C}] -to [get_pins u2/BCR_in_d_reg/D] 6.250
set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[5]/C}] -to [get_pins u2/triger_in_d_reg/D] 6.250
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets JA0_1_N_OBUF]

set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[4]/C}] -to [get_pins u2/ECR_r_reg/D] 6.250
set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[4]/C}] -to [get_pins u2/ECR_in_d_reg/D] 6.250
set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[3]/C}] -to [get_pins u2/BCR_r_reg/D] 6.250
set_max_delay -from [get_pins {u1/TTC_PICOZED_BD_i/axi_gpio_0/U0/gpio_core_1/Dual.gpio_Data_Out_reg[5]/C}] -to [get_pins u2/triger_r_reg/D] 6.250
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets JA0_1_N_OBUF]
