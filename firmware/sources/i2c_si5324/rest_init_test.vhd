--------------------------------------------------------------------------------
-- Object        : Entity work.rest_init_test
-- Version       : 2250 (Subversion)
-- Last modified : Tue Nov 18 12:07:42 2014.
--------------------------------------------------------------------------------



library ieee, work;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

entity rest_init_test is
  port (
    Rst_out     : out    std_logic;
    clk100      : in     std_logic;
    rst_in      : in     std_logic;
    si570_reset : in     std_logic);
end entity rest_init_test;

--------------------------------------------------------------------------------
-- Object        : Architecture work.rest_init_test.rtl
-- Version       : 2250 (Subversion)
-- Last modified : Tue Nov 18 12:07:42 2014.
--------------------------------------------------------------------------------


architecture rtl of rest_init_test is
signal count : std_logic_vector(5 downto 0):=(others => '0');
constant count_max : std_logic_vector(5 downto 0):=(others => '1');

begin


process(clk100,si570_reset, rst_in)
begin
 if rising_edge(clk100) then
	if(rst_in = '1' ) then
		count <= (others => '0');
		Rst_out <= '1';
	elsif(count /= count_max) then
		count <= count + 1;
		Rst_out <= '1';
	else
		Rst_out <= '0';
	end if;
 end if;
end process;
end architecture rtl ; -- of rest_init_test

