library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"


entity TX_Interface is
port
(
txdata_in 		: out std_logic_vector(15 downto 0);
data_valid_in  	: out   std_logic;
ttc_out_0  		: in   std_logic
);

end entity;
    
architecture RTL of TX_Interface is
begin

txdata_in <= (others => ttc_out_0);
data_valid_in <= '1';
end RTL;

 
