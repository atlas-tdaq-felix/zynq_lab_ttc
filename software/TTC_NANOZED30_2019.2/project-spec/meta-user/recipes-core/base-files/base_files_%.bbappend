FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
        file://001fixdns \
        "
RDEPENDS_${PN} += "bash"

do_install_append() {
    install -d ${D}${sysconfdir}/network/if-up.d
    install -m 0755 ${WORKDIR}/001fixdns ${D}${sysconfdir}/network/if-up.d
}
