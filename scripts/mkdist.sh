#!/bin/bash

# This script creates a distribution ZIP file

if [ -z "$1" ]; then
    echo "mkdist.sh <zip-file>"
    echo "Creates a distribution with <zip-file> name"
    exit 0
fi

PETALINUX_DIR=../software/TTC_NANOZED30_2019.2/images/linux
#PETALINUX_DIR=/home/mesfin/felix/mgebyehu/test_zynq_repo/zynq_lab_ttc/software/TTC_NANOZED30_2019.2/images/linux
FULLPATH=$PWD/$1
TMPDIR=$(mktemp -d)

# Bail out if the temp directory wasn't created successfully.
if [ ! -e $TMPDIR ]; then
    echo "Failed to create temp directory"
    exit 1
fi

# Make sure it gets removed even if the script exits abnormally.
trap "exit 1"           HUP INT PIPE QUIT TERM
trap 'rm -rf "$TMPDIR"' EXIT

mkdir $TMPDIR/BOOT

cp $PETALINUX_DIR/BOOT.BIN $PETALINUX_DIR/image.ub $TMPDIR/BOOT/.
cp $PETALINUX_DIR/rootfs.tar.gz $TMPDIR/.

cd $TMPDIR

zip -r $FULLPATH *

 
