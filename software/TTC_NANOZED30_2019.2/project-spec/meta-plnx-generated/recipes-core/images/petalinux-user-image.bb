DESCRIPTION = "PETALINUX image definition for Xilinx boards"
LICENSE = "MIT"

require recipes-core/images/petalinux-image-common.inc 

inherit extrausers 
COMMON_FEATURES = "\
		ssh-server-dropbear \
		hwcodecs \
		"
IMAGE_LINGUAS = " "

IMAGE_INSTALL = "\
		kernel-modules \
		haveged \
		mtd-utils \
		canutils \
		openssh-sftp-server \
		gzip \
		pciutils \
		pkgconfig \
		autoconf \
		autoconf-dev \
		autoconf-dbg \
		automake \
		automake-dev \
		automake-dbg \
		binutils \
		binutils-dev \
		binutils-dbg \
		make \
		make-dev \
		python \
		python-terminal \
		python-xml \
		run-postinsts \
		boost \
		boost-random \
		boost-regex \
		boost-atomic \
		boost-thread \
		boost-serialization \
		boost-filesystem \
		boost-test \
		boost-system \
		boost-graph \
		boost-container \
		boost-date-time \
		boost-math \
		boost-signals \
		boost-wave \
		boost-chrono \
		boost-timer \
		boost-dev \
		boost-program-options \
		boost-iostreams \
		boost-dbg \
		udev-extraconf \
		libstdc++ \
		libstdc++-dev \
		gdb \
		gdb-dev \
		gdbserver \
		glib-2.0 \
		glib-2.0-dev \
		glibc \
		glibc-dev \
		packagegroup-core-boot \
		packagegroup-core-buildessential \
		packagegroup-core-buildessential-dev \
		packagegroup-core-ssh-dropbear \
		tcf-agent \
		bridge-utils \
		gpio-demo \
		myapp \
		peekpoke \
		"
EXTRA_USERS_PARAMS ?= "usermod -P picozed root;useradd -P zedboard mgeb;"
