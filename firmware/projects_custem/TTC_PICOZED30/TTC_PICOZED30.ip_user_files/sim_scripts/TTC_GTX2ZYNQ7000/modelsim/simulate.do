onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L xilinx_vip -L xil_defaultlib -L xpm -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.TTC_GTX2ZYNQ7000 xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {TTC_GTX2ZYNQ7000.udo}

run -all

quit -force
