library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use xil_defaultlib.TTC709_pkg.all;

entity sim_top_8t49n242 is
  port (
    sim_clk             		: in     std_logic;
    sim_iic_clk   				: inout  std_logic;
    sim_iic_data  				: inout  std_logic;
    sim_rd_en_pb	 			: in 	  std_logic;	
    sim_wr_en_pb     			: in       std_logic);    
end entity sim_top_8t49n242;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of sim_top_8t49n242 is

    signal wr_8t49n242_reg    : clk_8t49n242_reg_type := clk_8t49n242_init;
    signal mstr_ctri_io   : std_logic;
    signal mstr_cin_io    : std_logic;
	signal mstr_cout_io   : std_logic;
    signal mstr_dtri_io   : std_logic;
    signal mstr_din_io    : std_logic;
	signal mstr_dout_io   : std_logic;
 

  component clk_8t49n242_i2c
	port (
    clk             		: in     std_logic;
    iic_mux_reset_n 		: out    std_logic;
    clk_8t49n242_control    : in     clk_8t49n242_reg_type;
    rd_en_pb	 			: in 	  std_logic;	
    wr_en_pb     			: in       std_logic;    
	sda       				: INOUT  STD_LOGIC;
	scl       				: INOUT  STD_LOGIC;
    clk_8t49n242_status    	: out    clk_8t49n242_reg_type);
  end component clk_8t49n242_i2c;


begin

  sim_top_comp: clk_8t49n242_i2c
	port map (                
		clk             	  => sim_clk,  
		iic_mux_reset_n 	  => open,  
		clk_8t49n242_control  => wr_8t49n242_reg,  
		rd_en_pb	 		  => sim_rd_en_pb,  
		wr_en_pb     		  => sim_wr_en_pb,  
		sda       			  => sim_iic_clk,  
		scl       			  => sim_iic_data, 
		clk_8t49n242_status   =>   open); 

end architecture structure ; -- of 8t49n242_i2c
