set_property IOSTANDARD LVCMOS18 [get_ports sys_rst]
set_property VCCAUX_IO DONTCARE [get_ports sys_rst]
set_property PACKAGE_PIN AR40 [get_ports sys_rst]

set_property IOSTANDARD LVDS [get_ports clk_ref_p]
set_property PACKAGE_PIN E18 [get_ports clk_ref_n]
set_property IOSTANDARD LVDS [get_ports clk_ref_n]

set_property IOSTANDARD LVCMOS18 [get_ports triger]
set_property PACKAGE_PIN BB24 [get_ports triger]

set_property IOSTANDARD LVCMOS18 [get_ports ECR]
set_property PACKAGE_PIN BB21 [get_ports ECR]

set_property PACKAGE_PIN BB23 [get_ports BCR]
set_property IOSTANDARD LVCMOS18 [get_ports BCR]

set_property PACKAGE_PIN AU36 [get_ports TTC_out]
set_property IOSTANDARD LVCMOS18 [get_ports TTC_out]

set_property PACKAGE_PIN AY33 [get_ports triger_mux]
set_property IOSTANDARD LVCMOS18 [get_ports triger_mux]

set_property PACKAGE_PIN BA31 [get_ports ECR_mux]
set_property IOSTANDARD LVCMOS18 [get_ports ECR_mux]

set_property PACKAGE_PIN BA32 [get_ports BCR_mux]
set_property IOSTANDARD LVCMOS18 [get_ports BCR_mux]

set_property PACKAGE_PIN AV39 [get_ports triger_push]
set_property IOSTANDARD LVCMOS18 [get_ports triger_push]

set_property PACKAGE_PIN AU38 [get_ports ECR_push]
set_property IOSTANDARD LVCMOS18 [get_ports ECR_push]

set_property PACKAGE_PIN AW40 [get_ports BCR_push]
set_property IOSTANDARD LVCMOS18 [get_ports BCR_push]


set_property PACKAGE_PIN K29 [get_ports Tout_P[0]]
set_property PACKAGE_PIN K30 [get_ports Tout_N[0]]
set_property PACKAGE_PIN R30 [get_ports Tout_P[1]]
set_property PACKAGE_PIN P31 [get_ports Tout_N[1]]
set_property PACKAGE_PIN T29 [get_ports Tout_P[2]]
set_property PACKAGE_PIN T30 [get_ports Tout_N[2]]
set_property PACKAGE_PIN L29 [get_ports Tout_P[3]]
set_property PACKAGE_PIN L30 [get_ports Tout_N[3]]
set_property PACKAGE_PIN M28 [get_ports Tout_P[4]]
set_property PACKAGE_PIN M29 [get_ports Tout_N[4]]
set_property PACKAGE_PIN V30 [get_ports Tout_P[5]]
set_property PACKAGE_PIN V31 [get_ports Tout_N[5]]
set_property PACKAGE_PIN U31 [get_ports Tout_P[6]]
set_property PACKAGE_PIN T31 [get_ports Tout_N[6]]
set_property PACKAGE_PIN V29 [get_ports Tout_P[7]]
set_property PACKAGE_PIN U29 [get_ports Tout_N[7]]

set_property IOSTANDARD LVDS [get_ports Tout_P[0]]
set_property IOSTANDARD LVDS [get_ports Tout_N[0]]
set_property IOSTANDARD LVDS [get_ports Tout_P[1]]
set_property IOSTANDARD LVDS [get_ports Tout_N[1]]
set_property IOSTANDARD LVDS [get_ports Tout_P[2]]
set_property IOSTANDARD LVDS [get_ports Tout_N[2]]
set_property IOSTANDARD LVDS [get_ports Tout_P[3]]
set_property IOSTANDARD LVDS [get_ports Tout_N[3]]
set_property IOSTANDARD LVDS [get_ports Tout_P[4]]
set_property IOSTANDARD LVDS [get_ports Tout_N[4]]
set_property IOSTANDARD LVDS [get_ports Tout_P[5]]
set_property IOSTANDARD LVDS [get_ports Tout_N[5]]
set_property IOSTANDARD LVDS [get_ports Tout_P[6]]
set_property IOSTANDARD LVDS [get_ports Tout_N[6]]
set_property IOSTANDARD LVDS [get_ports Tout_P[7]]
set_property IOSTANDARD LVDS [get_ports Tout_N[7]]

set_property PACKAGE_PIN AK34 [get_ports USER_CLOCK_P]
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_P]
set_property PACKAGE_PIN AL34 [get_ports USER_CLOCK_N]
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_N]

set_property PACKAGE_PIN AM3 [get_ports SFP_TX_P]
set_property IOSTANDARD LVDS [get_ports SFP_TX_P]
set_property PACKAGE_PIN AM4 [get_ports SFP_TX_N]
set_property IOSTANDARD LVDS [get_ports SFP_TX_N]

set_property PACKAGE_PIN AL6 [get_ports SFP_RX_P]
set_property IOSTANDARD LVDS [get_ports SFP_RX_P]
set_property PACKAGE_PIN AL5 [get_ports SFP_RX_N]
set_property IOSTANDARD LVDS [get_ports SFP_RX_N]


set_property PACKAGE_PIN AD8 [get_ports GTREFCLK0_1_P_IN]
set_property IOSTANDARD LVDS [get_ports GTREFCLK0_1_P_IN]
set_property PACKAGE_PIN AD7 [get_ports GTREFCLK0_1_N_IN]
set_property IOSTANDARD LVDS [get_ports GTREFCLK0_1_N_IN]

set_property PACKAGE_PIN AJ32 [get_ports USER_SMA_CLOCK_P]
set_property IOSTANDARD LVDS [get_ports USER_SMA_CLOCK_P]
set_property PACKAGE_PIN AK32 [get_ports USER_SMA_CLOCK_N]
set_property IOSTANDARD LVDS [get_ports USER_SMA_CLOCK_N]


set_property PACKAGE_PIN AT35 [get_ports iic_clk]
set_property IOSTANDARD LVCMOS18 [get_ports iic_clk]
set_property PACKAGE_PIN AU32 [get_ports iic_data]
set_property IOSTANDARD LVCMOS18 [get_ports iic_data]
set_property PACKAGE_PIN AN31 [get_ports USER_SMA_GPIO_P]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_P]
set_property PACKAGE_PIN AP31 [get_ports USER_SMA_GPIO_N]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_N]

set_property PACKAGE_PIN AY42 [get_ports iic_mux_reset]
set_property IOSTANDARD LVCMOS18 [get_ports iic_mux_reset]
set_property PACKAGE_PIN AT36 [get_ports si5324_nreset]
set_property IOSTANDARD LVCMOS18 [get_ports si5324_nreset]

set_max_delay -datapath_only -from [get_clocks -of_objects [get_pins clock_80_40_inist/inst/mmcm_adv_inst/CLKOUT3]] -to [get_clocks the_TTC709_trans_top/u1/U0/TTC709_transceiver_init_i/TTC709_transceiver_i/gt0_TTC709_transceiver_i/gtxe2_i/TXOUTCLK] 6.25

#write_cfgmem -format mcs -interface bpix16 -size 128 -loadbit "up 0x0 D:/TTCvi/firmware/Projects/TTCvi/TTCvi.runs/impl_1/TTCvi_top.bit" -file D:/TTCvi/firmware/Projects/TTCvi/TTCvi.runs/impl_1/TTCvi_top.mcs -force
#BPI Flash part name mt28gu01gaax1e-bpi-x16
