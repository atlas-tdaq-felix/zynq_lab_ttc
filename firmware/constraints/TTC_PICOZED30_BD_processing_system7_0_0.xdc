############################################################################
##
##  Xilinx, Inc. 2006            www.xilinx.com
############################################################################
##  File name :       ps7_constraints.xdc
##
##  Details :     Constraints file
##                    FPGA family:       zynq
##                    FPGA:              xc7z030sbg485-1
##                    Device Size:        xc7z030
##                    Package:            sbg485
##                    Speedgrade:         -1
##
##
############################################################################
############################################################################
############################################################################
# Clock constraints                                                        #
############################################################################
set_input_jitter clk_fpga_0 0.300
#The clocks are asynchronous, user should constrain them appropriately.#


############################################################################
# I/O STANDARDS and Location Constraints                                   #
############################################################################

#  UART 0 / tx / MIO[11]
#  UART 0 / rx / MIO[10]
#  Quad SPI Flash / qspi_fbclk / MIO[8]
#  Quad SPI Flash / qspi0_sclk / MIO[6]
#  Quad SPI Flash / qspi0_io[3]/HOLD_B / MIO[5]
#  Quad SPI Flash / qspi0_io[2] / MIO[4]
#  Quad SPI Flash / qspi0_io[1] / MIO[3]
#  Quad SPI Flash / qspi0_io[0] / MIO[2]
#  Quad SPI Flash / qspi0_ss_b / MIO[1]


