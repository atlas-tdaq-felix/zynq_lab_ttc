-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (win64) Build 2188600 Wed Apr  4 18:40:38 MDT 2018
-- Date        : Thu Aug  6 14:34:20 2020
-- Host        : Mesfin running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               C:/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_edge_det_0_0/TTC_PICOZED30_BD_edge_det_0_0_sim_netlist.vhdl
-- Design      : TTC_PICOZED30_BD_edge_det_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TTC_PICOZED30_BD_edge_det_0_0_edge_det is
  port (
    ECR_r : out STD_LOGIC;
    triger_r : out STD_LOGIC;
    BCR_r : out STD_LOGIC;
    clk_160M : in STD_LOGIC;
    ECR_push : in STD_LOGIC;
    triger_push : in STD_LOGIC;
    BCR_push : in STD_LOGIC;
    gpio_in : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of TTC_PICOZED30_BD_edge_det_0_0_edge_det : entity is "edge_det";
end TTC_PICOZED30_BD_edge_det_0_0_edge_det;

architecture STRUCTURE of TTC_PICOZED30_BD_edge_det_0_0_edge_det is
  signal BCR_in : STD_LOGIC;
  signal BCR_in_d : STD_LOGIC;
  signal BCR_push_debounce : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal BCR_push_t : STD_LOGIC;
  signal BCR_push_t_i_1_n_0 : STD_LOGIC;
  signal BCR_push_t_i_2_n_0 : STD_LOGIC;
  signal BCR_push_t_i_3_n_0 : STD_LOGIC;
  signal BCR_push_t_i_4_n_0 : STD_LOGIC;
  signal BCR_push_t_i_5_n_0 : STD_LOGIC;
  signal BCR_push_t_i_6_n_0 : STD_LOGIC;
  signal BCR_push_t_i_7_n_0 : STD_LOGIC;
  signal BCR_push_t_i_8_n_0 : STD_LOGIC;
  signal BCR_r0 : STD_LOGIC;
  signal ECR_in : STD_LOGIC;
  signal ECR_in_d : STD_LOGIC;
  signal ECR_push_debounce : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ECR_push_t : STD_LOGIC;
  signal ECR_push_t_i_1_n_0 : STD_LOGIC;
  signal ECR_push_t_i_2_n_0 : STD_LOGIC;
  signal ECR_push_t_i_3_n_0 : STD_LOGIC;
  signal ECR_push_t_i_4_n_0 : STD_LOGIC;
  signal ECR_push_t_i_5_n_0 : STD_LOGIC;
  signal ECR_push_t_i_6_n_0 : STD_LOGIC;
  signal ECR_push_t_i_7_n_0 : STD_LOGIC;
  signal ECR_push_t_i_8_n_0 : STD_LOGIC;
  signal ECR_r0 : STD_LOGIC;
  signal triger_in : STD_LOGIC;
  signal triger_in_d : STD_LOGIC;
  signal triger_push_debounce : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal triger_push_t : STD_LOGIC;
  signal triger_push_t_i_1_n_0 : STD_LOGIC;
  signal triger_push_t_i_2_n_0 : STD_LOGIC;
  signal triger_push_t_i_3_n_0 : STD_LOGIC;
  signal triger_push_t_i_4_n_0 : STD_LOGIC;
  signal triger_push_t_i_5_n_0 : STD_LOGIC;
  signal triger_push_t_i_6_n_0 : STD_LOGIC;
  signal triger_push_t_i_7_n_0 : STD_LOGIC;
  signal triger_push_t_i_8_n_0 : STD_LOGIC;
  signal triger_r0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of BCR_in_d_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of BCR_r_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of ECR_in_d_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of ECR_r_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of triger_in_d_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of triger_r_i_1 : label is "soft_lutpair1";
begin
BCR_in_d_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => gpio_in(2),
      I1 => gpio_in(5),
      I2 => BCR_push_t,
      O => BCR_in
    );
BCR_in_d_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_in,
      Q => BCR_in_d,
      R => '0'
    );
\BCR_push_debounce_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push,
      Q => BCR_push_debounce(0),
      R => '0'
    );
\BCR_push_debounce_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(9),
      Q => BCR_push_debounce(10),
      R => '0'
    );
\BCR_push_debounce_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(10),
      Q => BCR_push_debounce(11),
      R => '0'
    );
\BCR_push_debounce_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(11),
      Q => BCR_push_debounce(12),
      R => '0'
    );
\BCR_push_debounce_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(12),
      Q => BCR_push_debounce(13),
      R => '0'
    );
\BCR_push_debounce_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(13),
      Q => BCR_push_debounce(14),
      R => '0'
    );
\BCR_push_debounce_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(14),
      Q => BCR_push_debounce(15),
      R => '0'
    );
\BCR_push_debounce_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(0),
      Q => BCR_push_debounce(1),
      R => '0'
    );
\BCR_push_debounce_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(1),
      Q => BCR_push_debounce(2),
      R => '0'
    );
\BCR_push_debounce_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(2),
      Q => BCR_push_debounce(3),
      R => '0'
    );
\BCR_push_debounce_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(3),
      Q => BCR_push_debounce(4),
      R => '0'
    );
\BCR_push_debounce_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(4),
      Q => BCR_push_debounce(5),
      R => '0'
    );
\BCR_push_debounce_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(5),
      Q => BCR_push_debounce(6),
      R => '0'
    );
\BCR_push_debounce_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(6),
      Q => BCR_push_debounce(7),
      R => '0'
    );
\BCR_push_debounce_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(7),
      Q => BCR_push_debounce(8),
      R => '0'
    );
\BCR_push_debounce_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_debounce(8),
      Q => BCR_push_debounce(9),
      R => '0'
    );
BCR_push_t_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEA"
    )
        port map (
      I0 => BCR_push_t_i_2_n_0,
      I1 => BCR_push_debounce(0),
      I2 => BCR_push_t_i_3_n_0,
      I3 => BCR_push_t_i_4_n_0,
      I4 => BCR_push_t_i_5_n_0,
      O => BCR_push_t_i_1_n_0
    );
BCR_push_t_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => BCR_push_debounce(1),
      I1 => BCR_push_debounce(4),
      I2 => BCR_push_debounce(5),
      I3 => BCR_push_debounce(3),
      I4 => BCR_push_t_i_3_n_0,
      I5 => BCR_push_debounce(2),
      O => BCR_push_t_i_2_n_0
    );
BCR_push_t_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => BCR_push_t_i_6_n_0,
      I1 => BCR_push_t_i_7_n_0,
      I2 => BCR_push_t_i_8_n_0,
      I3 => BCR_push_t,
      O => BCR_push_t_i_3_n_0
    );
BCR_push_t_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => BCR_push_debounce(11),
      I1 => BCR_push_debounce(14),
      I2 => BCR_push_debounce(15),
      I3 => BCR_push_debounce(13),
      I4 => BCR_push_t_i_3_n_0,
      I5 => BCR_push_debounce(12),
      O => BCR_push_t_i_4_n_0
    );
BCR_push_t_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => BCR_push_debounce(6),
      I1 => BCR_push_debounce(9),
      I2 => BCR_push_debounce(10),
      I3 => BCR_push_debounce(8),
      I4 => BCR_push_t_i_3_n_0,
      I5 => BCR_push_debounce(7),
      O => BCR_push_t_i_5_n_0
    );
BCR_push_t_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => BCR_push_debounce(6),
      I1 => BCR_push_debounce(7),
      I2 => BCR_push_debounce(4),
      I3 => BCR_push_debounce(5),
      I4 => BCR_push_debounce(9),
      I5 => BCR_push_debounce(8),
      O => BCR_push_t_i_6_n_0
    );
BCR_push_t_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => BCR_push_debounce(1),
      I1 => BCR_push_debounce(0),
      I2 => BCR_push_debounce(3),
      I3 => BCR_push_debounce(2),
      O => BCR_push_t_i_7_n_0
    );
BCR_push_t_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => BCR_push_debounce(12),
      I1 => BCR_push_debounce(13),
      I2 => BCR_push_debounce(10),
      I3 => BCR_push_debounce(11),
      I4 => BCR_push_debounce(15),
      I5 => BCR_push_debounce(14),
      O => BCR_push_t_i_8_n_0
    );
BCR_push_t_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => BCR_push_t_i_1_n_0,
      Q => BCR_push_t,
      R => '0'
    );
BCR_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => BCR_push_t,
      I1 => gpio_in(5),
      I2 => gpio_in(2),
      I3 => BCR_in_d,
      O => BCR_r0
    );
BCR_r_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_160M,
      CE => '1',
      D => BCR_r0,
      Q => BCR_r,
      R => '0'
    );
ECR_in_d_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => gpio_in(1),
      I1 => gpio_in(4),
      I2 => ECR_push_t,
      O => ECR_in
    );
ECR_in_d_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_in,
      Q => ECR_in_d,
      R => '0'
    );
\ECR_push_debounce_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push,
      Q => ECR_push_debounce(0),
      R => '0'
    );
\ECR_push_debounce_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(9),
      Q => ECR_push_debounce(10),
      R => '0'
    );
\ECR_push_debounce_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(10),
      Q => ECR_push_debounce(11),
      R => '0'
    );
\ECR_push_debounce_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(11),
      Q => ECR_push_debounce(12),
      R => '0'
    );
\ECR_push_debounce_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(12),
      Q => ECR_push_debounce(13),
      R => '0'
    );
\ECR_push_debounce_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(13),
      Q => ECR_push_debounce(14),
      R => '0'
    );
\ECR_push_debounce_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(14),
      Q => ECR_push_debounce(15),
      R => '0'
    );
\ECR_push_debounce_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(0),
      Q => ECR_push_debounce(1),
      R => '0'
    );
\ECR_push_debounce_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(1),
      Q => ECR_push_debounce(2),
      R => '0'
    );
\ECR_push_debounce_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(2),
      Q => ECR_push_debounce(3),
      R => '0'
    );
\ECR_push_debounce_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(3),
      Q => ECR_push_debounce(4),
      R => '0'
    );
\ECR_push_debounce_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(4),
      Q => ECR_push_debounce(5),
      R => '0'
    );
\ECR_push_debounce_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(5),
      Q => ECR_push_debounce(6),
      R => '0'
    );
\ECR_push_debounce_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(6),
      Q => ECR_push_debounce(7),
      R => '0'
    );
\ECR_push_debounce_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(7),
      Q => ECR_push_debounce(8),
      R => '0'
    );
\ECR_push_debounce_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_debounce(8),
      Q => ECR_push_debounce(9),
      R => '0'
    );
ECR_push_t_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEA"
    )
        port map (
      I0 => ECR_push_t_i_2_n_0,
      I1 => ECR_push_debounce(0),
      I2 => ECR_push_t_i_3_n_0,
      I3 => ECR_push_t_i_4_n_0,
      I4 => ECR_push_t_i_5_n_0,
      O => ECR_push_t_i_1_n_0
    );
ECR_push_t_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => ECR_push_debounce(1),
      I1 => ECR_push_debounce(4),
      I2 => ECR_push_debounce(5),
      I3 => ECR_push_debounce(3),
      I4 => ECR_push_t_i_3_n_0,
      I5 => ECR_push_debounce(2),
      O => ECR_push_t_i_2_n_0
    );
ECR_push_t_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => ECR_push_t_i_6_n_0,
      I1 => ECR_push_t_i_7_n_0,
      I2 => ECR_push_t_i_8_n_0,
      I3 => ECR_push_t,
      O => ECR_push_t_i_3_n_0
    );
ECR_push_t_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => ECR_push_debounce(11),
      I1 => ECR_push_debounce(14),
      I2 => ECR_push_debounce(15),
      I3 => ECR_push_debounce(13),
      I4 => ECR_push_t_i_3_n_0,
      I5 => ECR_push_debounce(12),
      O => ECR_push_t_i_4_n_0
    );
ECR_push_t_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => ECR_push_debounce(6),
      I1 => ECR_push_debounce(9),
      I2 => ECR_push_debounce(10),
      I3 => ECR_push_debounce(8),
      I4 => ECR_push_t_i_3_n_0,
      I5 => ECR_push_debounce(7),
      O => ECR_push_t_i_5_n_0
    );
ECR_push_t_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => ECR_push_debounce(6),
      I1 => ECR_push_debounce(7),
      I2 => ECR_push_debounce(4),
      I3 => ECR_push_debounce(5),
      I4 => ECR_push_debounce(9),
      I5 => ECR_push_debounce(8),
      O => ECR_push_t_i_6_n_0
    );
ECR_push_t_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => ECR_push_debounce(1),
      I1 => ECR_push_debounce(0),
      I2 => ECR_push_debounce(3),
      I3 => ECR_push_debounce(2),
      O => ECR_push_t_i_7_n_0
    );
ECR_push_t_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => ECR_push_debounce(12),
      I1 => ECR_push_debounce(13),
      I2 => ECR_push_debounce(10),
      I3 => ECR_push_debounce(11),
      I4 => ECR_push_debounce(15),
      I5 => ECR_push_debounce(14),
      O => ECR_push_t_i_8_n_0
    );
ECR_push_t_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => ECR_push_t_i_1_n_0,
      Q => ECR_push_t,
      R => '0'
    );
ECR_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => ECR_push_t,
      I1 => gpio_in(4),
      I2 => gpio_in(1),
      I3 => ECR_in_d,
      O => ECR_r0
    );
ECR_r_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_160M,
      CE => '1',
      D => ECR_r0,
      Q => ECR_r,
      R => '0'
    );
triger_in_d_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => gpio_in(0),
      I1 => gpio_in(3),
      I2 => triger_push_t,
      O => triger_in
    );
triger_in_d_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_in,
      Q => triger_in_d,
      R => '0'
    );
\triger_push_debounce_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push,
      Q => triger_push_debounce(0),
      R => '0'
    );
\triger_push_debounce_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(9),
      Q => triger_push_debounce(10),
      R => '0'
    );
\triger_push_debounce_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(10),
      Q => triger_push_debounce(11),
      R => '0'
    );
\triger_push_debounce_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(11),
      Q => triger_push_debounce(12),
      R => '0'
    );
\triger_push_debounce_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(12),
      Q => triger_push_debounce(13),
      R => '0'
    );
\triger_push_debounce_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(13),
      Q => triger_push_debounce(14),
      R => '0'
    );
\triger_push_debounce_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(14),
      Q => triger_push_debounce(15),
      R => '0'
    );
\triger_push_debounce_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(0),
      Q => triger_push_debounce(1),
      R => '0'
    );
\triger_push_debounce_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(1),
      Q => triger_push_debounce(2),
      R => '0'
    );
\triger_push_debounce_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(2),
      Q => triger_push_debounce(3),
      R => '0'
    );
\triger_push_debounce_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(3),
      Q => triger_push_debounce(4),
      R => '0'
    );
\triger_push_debounce_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(4),
      Q => triger_push_debounce(5),
      R => '0'
    );
\triger_push_debounce_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(5),
      Q => triger_push_debounce(6),
      R => '0'
    );
\triger_push_debounce_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(6),
      Q => triger_push_debounce(7),
      R => '0'
    );
\triger_push_debounce_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(7),
      Q => triger_push_debounce(8),
      R => '0'
    );
\triger_push_debounce_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_debounce(8),
      Q => triger_push_debounce(9),
      R => '0'
    );
triger_push_t_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEA"
    )
        port map (
      I0 => triger_push_t_i_2_n_0,
      I1 => triger_push_debounce(0),
      I2 => triger_push_t_i_3_n_0,
      I3 => triger_push_t_i_4_n_0,
      I4 => triger_push_t_i_5_n_0,
      O => triger_push_t_i_1_n_0
    );
triger_push_t_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => triger_push_debounce(1),
      I1 => triger_push_debounce(4),
      I2 => triger_push_debounce(5),
      I3 => triger_push_debounce(3),
      I4 => triger_push_t_i_3_n_0,
      I5 => triger_push_debounce(2),
      O => triger_push_t_i_2_n_0
    );
triger_push_t_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => triger_push_t_i_6_n_0,
      I1 => triger_push_t_i_7_n_0,
      I2 => triger_push_t_i_8_n_0,
      I3 => triger_push_t,
      O => triger_push_t_i_3_n_0
    );
triger_push_t_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => triger_push_debounce(11),
      I1 => triger_push_debounce(14),
      I2 => triger_push_debounce(15),
      I3 => triger_push_debounce(13),
      I4 => triger_push_t_i_3_n_0,
      I5 => triger_push_debounce(12),
      O => triger_push_t_i_4_n_0
    );
triger_push_t_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => triger_push_debounce(6),
      I1 => triger_push_debounce(9),
      I2 => triger_push_debounce(10),
      I3 => triger_push_debounce(8),
      I4 => triger_push_t_i_3_n_0,
      I5 => triger_push_debounce(7),
      O => triger_push_t_i_5_n_0
    );
triger_push_t_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => triger_push_debounce(6),
      I1 => triger_push_debounce(7),
      I2 => triger_push_debounce(4),
      I3 => triger_push_debounce(5),
      I4 => triger_push_debounce(9),
      I5 => triger_push_debounce(8),
      O => triger_push_t_i_6_n_0
    );
triger_push_t_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => triger_push_debounce(1),
      I1 => triger_push_debounce(0),
      I2 => triger_push_debounce(3),
      I3 => triger_push_debounce(2),
      O => triger_push_t_i_7_n_0
    );
triger_push_t_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => triger_push_debounce(12),
      I1 => triger_push_debounce(13),
      I2 => triger_push_debounce(10),
      I3 => triger_push_debounce(11),
      I4 => triger_push_debounce(15),
      I5 => triger_push_debounce(14),
      O => triger_push_t_i_8_n_0
    );
triger_push_t_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_160M,
      CE => '1',
      D => triger_push_t_i_1_n_0,
      Q => triger_push_t,
      R => '0'
    );
triger_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => triger_push_t,
      I1 => gpio_in(3),
      I2 => gpio_in(0),
      I3 => triger_in_d,
      O => triger_r0
    );
triger_r_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_160M,
      CE => '1',
      D => triger_r0,
      Q => triger_r,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TTC_PICOZED30_BD_edge_det_0_0 is
  port (
    clk_160M : in STD_LOGIC;
    gpio_in : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ECR_r : out STD_LOGIC;
    triger_r : out STD_LOGIC;
    ECR_push : in STD_LOGIC;
    BCR_push : in STD_LOGIC;
    triger_push : in STD_LOGIC;
    BCR_r : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of TTC_PICOZED30_BD_edge_det_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of TTC_PICOZED30_BD_edge_det_0_0 : entity is "TTC_PICOZED30_BD_edge_det_0_0,edge_det,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of TTC_PICOZED30_BD_edge_det_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of TTC_PICOZED30_BD_edge_det_0_0 : entity is "edge_det,Vivado 2018.1";
end TTC_PICOZED30_BD_edge_det_0_0;

architecture STRUCTURE of TTC_PICOZED30_BD_edge_det_0_0 is
begin
U0: entity work.TTC_PICOZED30_BD_edge_det_0_0_edge_det
     port map (
      BCR_push => BCR_push,
      BCR_r => BCR_r,
      ECR_push => ECR_push,
      ECR_r => ECR_r,
      clk_160M => clk_160M,
      gpio_in(5 downto 0) => gpio_in(5 downto 0),
      triger_push => triger_push,
      triger_r => triger_r
    );
end STRUCTURE;
