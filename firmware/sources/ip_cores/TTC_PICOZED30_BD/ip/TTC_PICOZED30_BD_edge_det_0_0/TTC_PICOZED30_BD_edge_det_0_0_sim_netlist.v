// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (win64) Build 2188600 Wed Apr  4 18:40:38 MDT 2018
// Date        : Thu Aug  6 14:34:20 2020
// Host        : Mesfin running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_edge_det_0_0/TTC_PICOZED30_BD_edge_det_0_0_sim_netlist.v
// Design      : TTC_PICOZED30_BD_edge_det_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "TTC_PICOZED30_BD_edge_det_0_0,edge_det,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "edge_det,Vivado 2018.1" *) 
(* NotValidForBitStream *)
module TTC_PICOZED30_BD_edge_det_0_0
   (clk_160M,
    gpio_in,
    ECR_r,
    triger_r,
    ECR_push,
    BCR_push,
    triger_push,
    BCR_r);
  input clk_160M;
  input [5:0]gpio_in;
  output ECR_r;
  output triger_r;
  input ECR_push;
  input BCR_push;
  input triger_push;
  output BCR_r;

  wire BCR_push;
  wire BCR_r;
  wire ECR_push;
  wire ECR_r;
  wire clk_160M;
  wire [5:0]gpio_in;
  wire triger_push;
  wire triger_r;

  TTC_PICOZED30_BD_edge_det_0_0_edge_det U0
       (.BCR_push(BCR_push),
        .BCR_r(BCR_r),
        .ECR_push(ECR_push),
        .ECR_r(ECR_r),
        .clk_160M(clk_160M),
        .gpio_in(gpio_in),
        .triger_push(triger_push),
        .triger_r(triger_r));
endmodule

(* ORIG_REF_NAME = "edge_det" *) 
module TTC_PICOZED30_BD_edge_det_0_0_edge_det
   (ECR_r,
    triger_r,
    BCR_r,
    clk_160M,
    ECR_push,
    triger_push,
    BCR_push,
    gpio_in);
  output ECR_r;
  output triger_r;
  output BCR_r;
  input clk_160M;
  input ECR_push;
  input triger_push;
  input BCR_push;
  input [5:0]gpio_in;

  wire BCR_in;
  wire BCR_in_d;
  wire BCR_push;
  wire [15:0]BCR_push_debounce;
  wire BCR_push_t;
  wire BCR_push_t_i_1_n_0;
  wire BCR_push_t_i_2_n_0;
  wire BCR_push_t_i_3_n_0;
  wire BCR_push_t_i_4_n_0;
  wire BCR_push_t_i_5_n_0;
  wire BCR_push_t_i_6_n_0;
  wire BCR_push_t_i_7_n_0;
  wire BCR_push_t_i_8_n_0;
  wire BCR_r;
  wire BCR_r0;
  wire ECR_in;
  wire ECR_in_d;
  wire ECR_push;
  wire [15:0]ECR_push_debounce;
  wire ECR_push_t;
  wire ECR_push_t_i_1_n_0;
  wire ECR_push_t_i_2_n_0;
  wire ECR_push_t_i_3_n_0;
  wire ECR_push_t_i_4_n_0;
  wire ECR_push_t_i_5_n_0;
  wire ECR_push_t_i_6_n_0;
  wire ECR_push_t_i_7_n_0;
  wire ECR_push_t_i_8_n_0;
  wire ECR_r;
  wire ECR_r0;
  wire clk_160M;
  wire [5:0]gpio_in;
  wire triger_in;
  wire triger_in_d;
  wire triger_push;
  wire [15:0]triger_push_debounce;
  wire triger_push_t;
  wire triger_push_t_i_1_n_0;
  wire triger_push_t_i_2_n_0;
  wire triger_push_t_i_3_n_0;
  wire triger_push_t_i_4_n_0;
  wire triger_push_t_i_5_n_0;
  wire triger_push_t_i_6_n_0;
  wire triger_push_t_i_7_n_0;
  wire triger_push_t_i_8_n_0;
  wire triger_r;
  wire triger_r0;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    BCR_in_d_i_1
       (.I0(gpio_in[2]),
        .I1(gpio_in[5]),
        .I2(BCR_push_t),
        .O(BCR_in));
  FDRE #(
    .INIT(1'b0)) 
    BCR_in_d_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_in),
        .Q(BCR_in_d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[0] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push),
        .Q(BCR_push_debounce[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[10] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[9]),
        .Q(BCR_push_debounce[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[11] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[10]),
        .Q(BCR_push_debounce[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[12] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[11]),
        .Q(BCR_push_debounce[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[13] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[12]),
        .Q(BCR_push_debounce[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[14] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[13]),
        .Q(BCR_push_debounce[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[15] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[14]),
        .Q(BCR_push_debounce[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[1] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[0]),
        .Q(BCR_push_debounce[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[2] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[1]),
        .Q(BCR_push_debounce[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[3] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[2]),
        .Q(BCR_push_debounce[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[4] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[3]),
        .Q(BCR_push_debounce[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[5] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[4]),
        .Q(BCR_push_debounce[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[6] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[5]),
        .Q(BCR_push_debounce[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[7] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[6]),
        .Q(BCR_push_debounce[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[8] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[7]),
        .Q(BCR_push_debounce[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \BCR_push_debounce_reg[9] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_debounce[8]),
        .Q(BCR_push_debounce[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    BCR_push_t_i_1
       (.I0(BCR_push_t_i_2_n_0),
        .I1(BCR_push_debounce[0]),
        .I2(BCR_push_t_i_3_n_0),
        .I3(BCR_push_t_i_4_n_0),
        .I4(BCR_push_t_i_5_n_0),
        .O(BCR_push_t_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    BCR_push_t_i_2
       (.I0(BCR_push_debounce[1]),
        .I1(BCR_push_debounce[4]),
        .I2(BCR_push_debounce[5]),
        .I3(BCR_push_debounce[3]),
        .I4(BCR_push_t_i_3_n_0),
        .I5(BCR_push_debounce[2]),
        .O(BCR_push_t_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    BCR_push_t_i_3
       (.I0(BCR_push_t_i_6_n_0),
        .I1(BCR_push_t_i_7_n_0),
        .I2(BCR_push_t_i_8_n_0),
        .I3(BCR_push_t),
        .O(BCR_push_t_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    BCR_push_t_i_4
       (.I0(BCR_push_debounce[11]),
        .I1(BCR_push_debounce[14]),
        .I2(BCR_push_debounce[15]),
        .I3(BCR_push_debounce[13]),
        .I4(BCR_push_t_i_3_n_0),
        .I5(BCR_push_debounce[12]),
        .O(BCR_push_t_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    BCR_push_t_i_5
       (.I0(BCR_push_debounce[6]),
        .I1(BCR_push_debounce[9]),
        .I2(BCR_push_debounce[10]),
        .I3(BCR_push_debounce[8]),
        .I4(BCR_push_t_i_3_n_0),
        .I5(BCR_push_debounce[7]),
        .O(BCR_push_t_i_5_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    BCR_push_t_i_6
       (.I0(BCR_push_debounce[6]),
        .I1(BCR_push_debounce[7]),
        .I2(BCR_push_debounce[4]),
        .I3(BCR_push_debounce[5]),
        .I4(BCR_push_debounce[9]),
        .I5(BCR_push_debounce[8]),
        .O(BCR_push_t_i_6_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    BCR_push_t_i_7
       (.I0(BCR_push_debounce[1]),
        .I1(BCR_push_debounce[0]),
        .I2(BCR_push_debounce[3]),
        .I3(BCR_push_debounce[2]),
        .O(BCR_push_t_i_7_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    BCR_push_t_i_8
       (.I0(BCR_push_debounce[12]),
        .I1(BCR_push_debounce[13]),
        .I2(BCR_push_debounce[10]),
        .I3(BCR_push_debounce[11]),
        .I4(BCR_push_debounce[15]),
        .I5(BCR_push_debounce[14]),
        .O(BCR_push_t_i_8_n_0));
  FDRE #(
    .INIT(1'b0)) 
    BCR_push_t_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_push_t_i_1_n_0),
        .Q(BCR_push_t),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    BCR_r_i_1
       (.I0(BCR_push_t),
        .I1(gpio_in[5]),
        .I2(gpio_in[2]),
        .I3(BCR_in_d),
        .O(BCR_r0));
  FDRE BCR_r_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(BCR_r0),
        .Q(BCR_r),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    ECR_in_d_i_1
       (.I0(gpio_in[1]),
        .I1(gpio_in[4]),
        .I2(ECR_push_t),
        .O(ECR_in));
  FDRE #(
    .INIT(1'b0)) 
    ECR_in_d_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_in),
        .Q(ECR_in_d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[0] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push),
        .Q(ECR_push_debounce[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[10] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[9]),
        .Q(ECR_push_debounce[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[11] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[10]),
        .Q(ECR_push_debounce[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[12] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[11]),
        .Q(ECR_push_debounce[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[13] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[12]),
        .Q(ECR_push_debounce[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[14] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[13]),
        .Q(ECR_push_debounce[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[15] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[14]),
        .Q(ECR_push_debounce[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[1] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[0]),
        .Q(ECR_push_debounce[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[2] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[1]),
        .Q(ECR_push_debounce[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[3] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[2]),
        .Q(ECR_push_debounce[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[4] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[3]),
        .Q(ECR_push_debounce[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[5] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[4]),
        .Q(ECR_push_debounce[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[6] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[5]),
        .Q(ECR_push_debounce[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[7] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[6]),
        .Q(ECR_push_debounce[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[8] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[7]),
        .Q(ECR_push_debounce[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ECR_push_debounce_reg[9] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_debounce[8]),
        .Q(ECR_push_debounce[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    ECR_push_t_i_1
       (.I0(ECR_push_t_i_2_n_0),
        .I1(ECR_push_debounce[0]),
        .I2(ECR_push_t_i_3_n_0),
        .I3(ECR_push_t_i_4_n_0),
        .I4(ECR_push_t_i_5_n_0),
        .O(ECR_push_t_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    ECR_push_t_i_2
       (.I0(ECR_push_debounce[1]),
        .I1(ECR_push_debounce[4]),
        .I2(ECR_push_debounce[5]),
        .I3(ECR_push_debounce[3]),
        .I4(ECR_push_t_i_3_n_0),
        .I5(ECR_push_debounce[2]),
        .O(ECR_push_t_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    ECR_push_t_i_3
       (.I0(ECR_push_t_i_6_n_0),
        .I1(ECR_push_t_i_7_n_0),
        .I2(ECR_push_t_i_8_n_0),
        .I3(ECR_push_t),
        .O(ECR_push_t_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    ECR_push_t_i_4
       (.I0(ECR_push_debounce[11]),
        .I1(ECR_push_debounce[14]),
        .I2(ECR_push_debounce[15]),
        .I3(ECR_push_debounce[13]),
        .I4(ECR_push_t_i_3_n_0),
        .I5(ECR_push_debounce[12]),
        .O(ECR_push_t_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    ECR_push_t_i_5
       (.I0(ECR_push_debounce[6]),
        .I1(ECR_push_debounce[9]),
        .I2(ECR_push_debounce[10]),
        .I3(ECR_push_debounce[8]),
        .I4(ECR_push_t_i_3_n_0),
        .I5(ECR_push_debounce[7]),
        .O(ECR_push_t_i_5_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ECR_push_t_i_6
       (.I0(ECR_push_debounce[6]),
        .I1(ECR_push_debounce[7]),
        .I2(ECR_push_debounce[4]),
        .I3(ECR_push_debounce[5]),
        .I4(ECR_push_debounce[9]),
        .I5(ECR_push_debounce[8]),
        .O(ECR_push_t_i_6_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    ECR_push_t_i_7
       (.I0(ECR_push_debounce[1]),
        .I1(ECR_push_debounce[0]),
        .I2(ECR_push_debounce[3]),
        .I3(ECR_push_debounce[2]),
        .O(ECR_push_t_i_7_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ECR_push_t_i_8
       (.I0(ECR_push_debounce[12]),
        .I1(ECR_push_debounce[13]),
        .I2(ECR_push_debounce[10]),
        .I3(ECR_push_debounce[11]),
        .I4(ECR_push_debounce[15]),
        .I5(ECR_push_debounce[14]),
        .O(ECR_push_t_i_8_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ECR_push_t_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_push_t_i_1_n_0),
        .Q(ECR_push_t),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    ECR_r_i_1
       (.I0(ECR_push_t),
        .I1(gpio_in[4]),
        .I2(gpio_in[1]),
        .I3(ECR_in_d),
        .O(ECR_r0));
  FDRE ECR_r_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(ECR_r0),
        .Q(ECR_r),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    triger_in_d_i_1
       (.I0(gpio_in[0]),
        .I1(gpio_in[3]),
        .I2(triger_push_t),
        .O(triger_in));
  FDRE #(
    .INIT(1'b0)) 
    triger_in_d_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_in),
        .Q(triger_in_d),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[0] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push),
        .Q(triger_push_debounce[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[10] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[9]),
        .Q(triger_push_debounce[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[11] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[10]),
        .Q(triger_push_debounce[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[12] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[11]),
        .Q(triger_push_debounce[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[13] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[12]),
        .Q(triger_push_debounce[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[14] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[13]),
        .Q(triger_push_debounce[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[15] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[14]),
        .Q(triger_push_debounce[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[1] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[0]),
        .Q(triger_push_debounce[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[2] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[1]),
        .Q(triger_push_debounce[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[3] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[2]),
        .Q(triger_push_debounce[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[4] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[3]),
        .Q(triger_push_debounce[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[5] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[4]),
        .Q(triger_push_debounce[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[6] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[5]),
        .Q(triger_push_debounce[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[7] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[6]),
        .Q(triger_push_debounce[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[8] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[7]),
        .Q(triger_push_debounce[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \triger_push_debounce_reg[9] 
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_debounce[8]),
        .Q(triger_push_debounce[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    triger_push_t_i_1
       (.I0(triger_push_t_i_2_n_0),
        .I1(triger_push_debounce[0]),
        .I2(triger_push_t_i_3_n_0),
        .I3(triger_push_t_i_4_n_0),
        .I4(triger_push_t_i_5_n_0),
        .O(triger_push_t_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    triger_push_t_i_2
       (.I0(triger_push_debounce[1]),
        .I1(triger_push_debounce[4]),
        .I2(triger_push_debounce[5]),
        .I3(triger_push_debounce[3]),
        .I4(triger_push_t_i_3_n_0),
        .I5(triger_push_debounce[2]),
        .O(triger_push_t_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    triger_push_t_i_3
       (.I0(triger_push_t_i_6_n_0),
        .I1(triger_push_t_i_7_n_0),
        .I2(triger_push_t_i_8_n_0),
        .I3(triger_push_t),
        .O(triger_push_t_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    triger_push_t_i_4
       (.I0(triger_push_debounce[11]),
        .I1(triger_push_debounce[14]),
        .I2(triger_push_debounce[15]),
        .I3(triger_push_debounce[13]),
        .I4(triger_push_t_i_3_n_0),
        .I5(triger_push_debounce[12]),
        .O(triger_push_t_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    triger_push_t_i_5
       (.I0(triger_push_debounce[6]),
        .I1(triger_push_debounce[9]),
        .I2(triger_push_debounce[10]),
        .I3(triger_push_debounce[8]),
        .I4(triger_push_t_i_3_n_0),
        .I5(triger_push_debounce[7]),
        .O(triger_push_t_i_5_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    triger_push_t_i_6
       (.I0(triger_push_debounce[6]),
        .I1(triger_push_debounce[7]),
        .I2(triger_push_debounce[4]),
        .I3(triger_push_debounce[5]),
        .I4(triger_push_debounce[9]),
        .I5(triger_push_debounce[8]),
        .O(triger_push_t_i_6_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    triger_push_t_i_7
       (.I0(triger_push_debounce[1]),
        .I1(triger_push_debounce[0]),
        .I2(triger_push_debounce[3]),
        .I3(triger_push_debounce[2]),
        .O(triger_push_t_i_7_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    triger_push_t_i_8
       (.I0(triger_push_debounce[12]),
        .I1(triger_push_debounce[13]),
        .I2(triger_push_debounce[10]),
        .I3(triger_push_debounce[11]),
        .I4(triger_push_debounce[15]),
        .I5(triger_push_debounce[14]),
        .O(triger_push_t_i_8_n_0));
  FDRE #(
    .INIT(1'b0)) 
    triger_push_t_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_push_t_i_1_n_0),
        .Q(triger_push_t),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    triger_r_i_1
       (.I0(triger_push_t),
        .I1(gpio_in[3]),
        .I2(gpio_in[0]),
        .I3(triger_in_d),
        .O(triger_r0));
  FDRE triger_r_reg
       (.C(clk_160M),
        .CE(1'b1),
        .D(triger_r0),
        .Q(triger_r),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
