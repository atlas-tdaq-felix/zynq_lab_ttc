onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+TTC_BRAM_L1A -L xilinx_vip -L xil_defaultlib -L xpm -L blk_mem_gen_v8_4_1 -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.TTC_BRAM_L1A xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {TTC_BRAM_L1A.udo}

run -all

endsim

quit -force
