-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (win64) Build 2188600 Wed Apr  4 18:40:38 MDT 2018
-- Date        : Thu Aug  6 14:34:20 2020
-- Host        : Mesfin running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_edge_det_0_0/TTC_PICOZED30_BD_edge_det_0_0_stub.vhdl
-- Design      : TTC_PICOZED30_BD_edge_det_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TTC_PICOZED30_BD_edge_det_0_0 is
  Port ( 
    clk_160M : in STD_LOGIC;
    gpio_in : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ECR_r : out STD_LOGIC;
    triger_r : out STD_LOGIC;
    ECR_push : in STD_LOGIC;
    BCR_push : in STD_LOGIC;
    triger_push : in STD_LOGIC;
    BCR_r : out STD_LOGIC
  );

end TTC_PICOZED30_BD_edge_det_0_0;

architecture stub of TTC_PICOZED30_BD_edge_det_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_160M,gpio_in[5:0],ECR_r,triger_r,ECR_push,BCR_push,triger_push,BCR_r";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "edge_det,Vivado 2018.1";
begin
end;
