vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xilinx_vip  -sv2k12 "+incdir+../../../../TTC_PICOZED30.srcs/sources_1/ip/ila_master_rd_wr/hdl/verilog" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../TTC_PICOZED30.srcs/sources_1/ip/ila_master_rd_wr/hdl/verilog" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../../TTC_PICOZED30.srcs/sources_1/ip/ila_master_rd_wr/hdl/verilog" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -93 \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/ila_master_rd_wr/sim/ila_master_rd_wr.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

