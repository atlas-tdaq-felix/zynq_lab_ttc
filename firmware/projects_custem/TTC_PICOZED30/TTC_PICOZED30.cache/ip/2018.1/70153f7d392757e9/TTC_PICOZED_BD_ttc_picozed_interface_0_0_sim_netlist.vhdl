-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Thu Jul 14 14:27:35 2022
-- Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ TTC_PICOZED_BD_ttc_picozed_interface_0_0_sim_netlist.vhdl
-- Design      : TTC_PICOZED_BD_ttc_picozed_interface_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    L1A_out : out STD_LOGIC;
    BCR_out : out STD_LOGIC;
    ECR_out : out STD_LOGIC;
    BCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    BUSY_in : in STD_LOGIC;
    OCR_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI is
  signal \^bcr_period_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \BCR_PERIOD_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_PERIOD_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_PERIOD_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_PERIOD_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^bcr_out\ : STD_LOGIC;
  signal BCR_reg : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \BCR_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \BCR_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \^ecr_out\ : STD_LOGIC;
  signal ECR_reg : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \ECR_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \ECR_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \ECR_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \ECR_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \^l1a_count_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \L1A_COUNT_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_COUNT_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_COUNT_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_COUNT_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^l1a_period_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \L1A_PERIOD_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_PERIOD_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_PERIOD_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \L1A_PERIOD_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^l1a_out\ : STD_LOGIC;
  signal L1A_reg : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \L1A_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal LUT_CONT_EN : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \LUT_CONT_EN_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \LUT_CONT_EN_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \LUT_CONT_EN_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \LUT_CONT_EN_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^ocr_period_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \OCR_PERIOD_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \OCR_PERIOD_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \OCR_PERIOD_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \OCR_PERIOD_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal \^ttc_config_out\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \TTC_CONFIG_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \TTC_CONFIG_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \TTC_CONFIG_reg[31]_i_1_n_0\ : STD_LOGIC;
  signal \TTC_CONFIG_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_4_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \reg_data_out__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
begin
  BCR_PERIOD_out(31 downto 0) <= \^bcr_period_out\(31 downto 0);
  BCR_out <= \^bcr_out\;
  ECR_out <= \^ecr_out\;
  L1A_COUNT_out(31 downto 0) <= \^l1a_count_out\(31 downto 0);
  L1A_PERIOD_out(31 downto 0) <= \^l1a_period_out\(31 downto 0);
  L1A_out <= \^l1a_out\;
  OCR_PERIOD_out(31 downto 0) <= \^ocr_period_out\(31 downto 0);
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  TTC_CONFIG_out(31 downto 0) <= \^ttc_config_out\(31 downto 0);
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
\BCR_PERIOD_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \BCR_PERIOD_reg[15]_i_1_n_0\
    );
\BCR_PERIOD_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \BCR_PERIOD_reg[23]_i_1_n_0\
    );
\BCR_PERIOD_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \BCR_PERIOD_reg[31]_i_1_n_0\
    );
\BCR_PERIOD_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \BCR_PERIOD_reg[7]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^bcr_period_out\(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^bcr_period_out\(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^bcr_period_out\(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^bcr_period_out\(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^bcr_period_out\(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^bcr_period_out\(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^bcr_period_out\(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^bcr_period_out\(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^bcr_period_out\(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^bcr_period_out\(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^bcr_period_out\(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^bcr_period_out\(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^bcr_period_out\(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^bcr_period_out\(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^bcr_period_out\(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^bcr_period_out\(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^bcr_period_out\(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^bcr_period_out\(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^bcr_period_out\(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^bcr_period_out\(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^bcr_period_out\(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^bcr_period_out\(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^bcr_period_out\(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^bcr_period_out\(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^bcr_period_out\(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^bcr_period_out\(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^bcr_period_out\(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^bcr_period_out\(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^bcr_period_out\(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^bcr_period_out\(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^bcr_period_out\(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_PERIOD_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^bcr_period_out\(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \BCR_reg[0]_i_1_n_0\
    );
\BCR_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \BCR_reg[15]_i_1_n_0\
    );
\BCR_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \BCR_reg[23]_i_1_n_0\
    );
\BCR_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => \BCR_reg[31]_i_1_n_0\
    );
\BCR_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^bcr_out\,
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => BCR_reg(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => BCR_reg(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => BCR_reg(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => BCR_reg(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => BCR_reg(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => BCR_reg(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => BCR_reg(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => BCR_reg(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => BCR_reg(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => BCR_reg(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => BCR_reg(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => BCR_reg(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => BCR_reg(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => BCR_reg(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => BCR_reg(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => BCR_reg(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => BCR_reg(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => BCR_reg(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => BCR_reg(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => BCR_reg(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => BCR_reg(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => BCR_reg(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => BCR_reg(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => BCR_reg(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => BCR_reg(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => BCR_reg(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => BCR_reg(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => BCR_reg(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => BCR_reg(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => BCR_reg(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\BCR_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \BCR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => BCR_reg(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \ECR_reg[0]_i_1_n_0\
    );
\ECR_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \ECR_reg[15]_i_1_n_0\
    );
\ECR_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \ECR_reg[23]_i_1_n_0\
    );
\ECR_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \ECR_reg[31]_i_1_n_0\
    );
\ECR_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^ecr_out\,
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => ECR_reg(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => ECR_reg(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => ECR_reg(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => ECR_reg(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => ECR_reg(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => ECR_reg(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => ECR_reg(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => ECR_reg(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => ECR_reg(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => ECR_reg(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => ECR_reg(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => ECR_reg(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => ECR_reg(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => ECR_reg(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => ECR_reg(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => ECR_reg(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => ECR_reg(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => ECR_reg(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => ECR_reg(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => ECR_reg(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => ECR_reg(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => ECR_reg(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => ECR_reg(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => ECR_reg(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => ECR_reg(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => ECR_reg(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => ECR_reg(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => ECR_reg(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[0]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => ECR_reg(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => ECR_reg(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\ECR_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \ECR_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => ECR_reg(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \L1A_COUNT_reg[15]_i_1_n_0\
    );
\L1A_COUNT_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \L1A_COUNT_reg[23]_i_1_n_0\
    );
\L1A_COUNT_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \L1A_COUNT_reg[31]_i_1_n_0\
    );
\L1A_COUNT_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(3),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \L1A_COUNT_reg[7]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^l1a_count_out\(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^l1a_count_out\(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^l1a_count_out\(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^l1a_count_out\(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^l1a_count_out\(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^l1a_count_out\(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^l1a_count_out\(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^l1a_count_out\(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^l1a_count_out\(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^l1a_count_out\(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^l1a_count_out\(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^l1a_count_out\(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^l1a_count_out\(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^l1a_count_out\(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^l1a_count_out\(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^l1a_count_out\(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^l1a_count_out\(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^l1a_count_out\(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^l1a_count_out\(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^l1a_count_out\(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^l1a_count_out\(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^l1a_count_out\(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^l1a_count_out\(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^l1a_count_out\(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^l1a_count_out\(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^l1a_count_out\(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^l1a_count_out\(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^l1a_count_out\(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^l1a_count_out\(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^l1a_count_out\(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^l1a_count_out\(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_COUNT_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_COUNT_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^l1a_count_out\(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \L1A_PERIOD_reg[15]_i_1_n_0\
    );
\L1A_PERIOD_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \L1A_PERIOD_reg[23]_i_1_n_0\
    );
\L1A_PERIOD_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \L1A_PERIOD_reg[31]_i_1_n_0\
    );
\L1A_PERIOD_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \L1A_PERIOD_reg[7]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^l1a_period_out\(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^l1a_period_out\(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^l1a_period_out\(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^l1a_period_out\(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^l1a_period_out\(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^l1a_period_out\(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^l1a_period_out\(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^l1a_period_out\(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^l1a_period_out\(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^l1a_period_out\(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^l1a_period_out\(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^l1a_period_out\(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^l1a_period_out\(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^l1a_period_out\(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^l1a_period_out\(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^l1a_period_out\(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^l1a_period_out\(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^l1a_period_out\(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^l1a_period_out\(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^l1a_period_out\(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^l1a_period_out\(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^l1a_period_out\(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^l1a_period_out\(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^l1a_period_out\(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^l1a_period_out\(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^l1a_period_out\(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^l1a_period_out\(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^l1a_period_out\(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^l1a_period_out\(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^l1a_period_out\(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^l1a_period_out\(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_PERIOD_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \L1A_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^l1a_period_out\(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(0),
      O => p_1_in(0)
    );
\L1A_reg[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\L1A_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\L1A_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\L1A_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\L1A_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(0),
      Q => \^l1a_out\,
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => L1A_reg(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => L1A_reg(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => L1A_reg(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => L1A_reg(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => L1A_reg(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => L1A_reg(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => L1A_reg(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => L1A_reg(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => L1A_reg(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => L1A_reg(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(1),
      Q => L1A_reg(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => L1A_reg(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => L1A_reg(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => L1A_reg(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => L1A_reg(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => L1A_reg(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => L1A_reg(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => L1A_reg(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => L1A_reg(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => L1A_reg(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => L1A_reg(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(2),
      Q => L1A_reg(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => L1A_reg(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => L1A_reg(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(3),
      Q => L1A_reg(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(4),
      Q => L1A_reg(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(5),
      Q => L1A_reg(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(6),
      Q => L1A_reg(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(0),
      D => s00_axi_wdata(7),
      Q => L1A_reg(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => L1A_reg(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\L1A_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => L1A_reg(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(2),
      O => \LUT_CONT_EN_reg[15]_i_1_n_0\
    );
\LUT_CONT_EN_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(2),
      O => \LUT_CONT_EN_reg[23]_i_1_n_0\
    );
\LUT_CONT_EN_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(2),
      O => \LUT_CONT_EN_reg[31]_i_1_n_0\
    );
\LUT_CONT_EN_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(2),
      O => \LUT_CONT_EN_reg[7]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => LUT_CONT_EN(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => LUT_CONT_EN(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => LUT_CONT_EN(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => LUT_CONT_EN(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => LUT_CONT_EN(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => LUT_CONT_EN(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => LUT_CONT_EN(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => LUT_CONT_EN(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => LUT_CONT_EN(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => LUT_CONT_EN(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => LUT_CONT_EN(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => LUT_CONT_EN(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => LUT_CONT_EN(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => LUT_CONT_EN(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => LUT_CONT_EN(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => LUT_CONT_EN(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => LUT_CONT_EN(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => LUT_CONT_EN(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => LUT_CONT_EN(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => LUT_CONT_EN(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => LUT_CONT_EN(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => LUT_CONT_EN(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => LUT_CONT_EN(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => LUT_CONT_EN(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => LUT_CONT_EN(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => LUT_CONT_EN(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => LUT_CONT_EN(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => LUT_CONT_EN(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => LUT_CONT_EN(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => LUT_CONT_EN(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => LUT_CONT_EN(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\LUT_CONT_EN_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \LUT_CONT_EN_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => LUT_CONT_EN(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \OCR_PERIOD_reg[15]_i_1_n_0\
    );
\OCR_PERIOD_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \OCR_PERIOD_reg[23]_i_1_n_0\
    );
\OCR_PERIOD_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \OCR_PERIOD_reg[31]_i_1_n_0\
    );
\OCR_PERIOD_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \OCR_PERIOD_reg[7]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^ocr_period_out\(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^ocr_period_out\(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^ocr_period_out\(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^ocr_period_out\(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^ocr_period_out\(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^ocr_period_out\(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^ocr_period_out\(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^ocr_period_out\(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^ocr_period_out\(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^ocr_period_out\(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^ocr_period_out\(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^ocr_period_out\(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^ocr_period_out\(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^ocr_period_out\(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^ocr_period_out\(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^ocr_period_out\(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^ocr_period_out\(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^ocr_period_out\(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^ocr_period_out\(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^ocr_period_out\(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^ocr_period_out\(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^ocr_period_out\(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^ocr_period_out\(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^ocr_period_out\(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^ocr_period_out\(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^ocr_period_out\(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^ocr_period_out\(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^ocr_period_out\(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^ocr_period_out\(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^ocr_period_out\(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^ocr_period_out\(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\OCR_PERIOD_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \OCR_PERIOD_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^ocr_period_out\(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \TTC_CONFIG_reg[15]_i_1_n_0\
    );
\TTC_CONFIG_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \TTC_CONFIG_reg[23]_i_1_n_0\
    );
\TTC_CONFIG_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \TTC_CONFIG_reg[31]_i_1_n_0\
    );
\TTC_CONFIG_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \TTC_CONFIG_reg[7]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^ttc_config_out\(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^ttc_config_out\(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^ttc_config_out\(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^ttc_config_out\(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^ttc_config_out\(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^ttc_config_out\(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^ttc_config_out\(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^ttc_config_out\(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^ttc_config_out\(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^ttc_config_out\(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^ttc_config_out\(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^ttc_config_out\(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^ttc_config_out\(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^ttc_config_out\(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^ttc_config_out\(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^ttc_config_out\(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^ttc_config_out\(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^ttc_config_out\(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^ttc_config_out\(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^ttc_config_out\(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^ttc_config_out\(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^ttc_config_out\(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^ttc_config_out\(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^ttc_config_out\(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^ttc_config_out\(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^ttc_config_out\(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^ttc_config_out\(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^ttc_config_out\(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^ttc_config_out\(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^ttc_config_out\(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^ttc_config_out\(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\TTC_CONFIG_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \TTC_CONFIG_reg[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^ttc_config_out\(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      S => \L1A_reg[0]_i_1_n_0\
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      S => \L1A_reg[0]_i_1_n_0\
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      S => \L1A_reg[0]_i_1_n_0\
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(3),
      Q => sel0(3),
      S => \L1A_reg[0]_i_1_n_0\
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
axi_awready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => \L1A_reg[0]_i_1_n_0\
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[0]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[0]_i_4_n_0\,
      O => \reg_data_out__0\(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(0),
      I1 => \^l1a_count_out\(0),
      I2 => sel0(1),
      I3 => OCR_COUNT(0),
      I4 => sel0(0),
      I5 => L1A_COUNT(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => BUSY_in,
      I1 => \^ttc_config_out\(0),
      I2 => sel0(1),
      I3 => \^l1a_period_out\(0),
      I4 => sel0(0),
      I5 => \^ocr_period_out\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(0),
      I1 => \^ecr_out\,
      I2 => sel0(1),
      I3 => \^bcr_out\,
      I4 => sel0(0),
      I5 => \^l1a_out\,
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[10]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[10]_i_4_n_0\,
      O => \reg_data_out__0\(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(10),
      I1 => \^l1a_count_out\(10),
      I2 => sel0(1),
      I3 => OCR_COUNT(10),
      I4 => sel0(0),
      I5 => L1A_COUNT(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(10),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(10),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(10),
      I1 => ECR_reg(10),
      I2 => sel0(1),
      I3 => BCR_reg(10),
      I4 => sel0(0),
      I5 => L1A_reg(10),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[11]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[11]_i_4_n_0\,
      O => \reg_data_out__0\(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(11),
      I1 => \^l1a_count_out\(11),
      I2 => sel0(1),
      I3 => OCR_COUNT(11),
      I4 => sel0(0),
      I5 => L1A_COUNT(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(11),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(11),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(11),
      I1 => ECR_reg(11),
      I2 => sel0(1),
      I3 => BCR_reg(11),
      I4 => sel0(0),
      I5 => L1A_reg(11),
      O => \axi_rdata[11]_i_4_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[12]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[12]_i_4_n_0\,
      O => \reg_data_out__0\(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(12),
      I1 => \^l1a_count_out\(12),
      I2 => sel0(1),
      I3 => OCR_COUNT(12),
      I4 => sel0(0),
      I5 => L1A_COUNT(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(12),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(12),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(12),
      I1 => ECR_reg(12),
      I2 => sel0(1),
      I3 => BCR_reg(12),
      I4 => sel0(0),
      I5 => L1A_reg(12),
      O => \axi_rdata[12]_i_4_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[13]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[13]_i_4_n_0\,
      O => \reg_data_out__0\(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(13),
      I1 => \^l1a_count_out\(13),
      I2 => sel0(1),
      I3 => OCR_COUNT(13),
      I4 => sel0(0),
      I5 => L1A_COUNT(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(13),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(13),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(13),
      I1 => ECR_reg(13),
      I2 => sel0(1),
      I3 => BCR_reg(13),
      I4 => sel0(0),
      I5 => L1A_reg(13),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[14]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[14]_i_4_n_0\,
      O => \reg_data_out__0\(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(14),
      I1 => \^l1a_count_out\(14),
      I2 => sel0(1),
      I3 => OCR_COUNT(14),
      I4 => sel0(0),
      I5 => L1A_COUNT(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(14),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(14),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(14),
      I1 => ECR_reg(14),
      I2 => sel0(1),
      I3 => BCR_reg(14),
      I4 => sel0(0),
      I5 => L1A_reg(14),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[15]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[15]_i_4_n_0\,
      O => \reg_data_out__0\(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(15),
      I1 => \^l1a_count_out\(15),
      I2 => sel0(1),
      I3 => OCR_COUNT(15),
      I4 => sel0(0),
      I5 => L1A_COUNT(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(15),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(15),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(15),
      I1 => ECR_reg(15),
      I2 => sel0(1),
      I3 => BCR_reg(15),
      I4 => sel0(0),
      I5 => L1A_reg(15),
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[16]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[16]_i_4_n_0\,
      O => \reg_data_out__0\(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(16),
      I1 => \^l1a_count_out\(16),
      I2 => sel0(1),
      I3 => OCR_COUNT(16),
      I4 => sel0(0),
      I5 => L1A_COUNT(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(16),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(16),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(16),
      I1 => ECR_reg(16),
      I2 => sel0(1),
      I3 => BCR_reg(16),
      I4 => sel0(0),
      I5 => L1A_reg(16),
      O => \axi_rdata[16]_i_4_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[17]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[17]_i_4_n_0\,
      O => \reg_data_out__0\(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(17),
      I1 => \^l1a_count_out\(17),
      I2 => sel0(1),
      I3 => OCR_COUNT(17),
      I4 => sel0(0),
      I5 => L1A_COUNT(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(17),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(17),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(17),
      I1 => ECR_reg(17),
      I2 => sel0(1),
      I3 => BCR_reg(17),
      I4 => sel0(0),
      I5 => L1A_reg(17),
      O => \axi_rdata[17]_i_4_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[18]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[18]_i_4_n_0\,
      O => \reg_data_out__0\(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(18),
      I1 => \^l1a_count_out\(18),
      I2 => sel0(1),
      I3 => OCR_COUNT(18),
      I4 => sel0(0),
      I5 => L1A_COUNT(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(18),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(18),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(18),
      I1 => ECR_reg(18),
      I2 => sel0(1),
      I3 => BCR_reg(18),
      I4 => sel0(0),
      I5 => L1A_reg(18),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[19]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[19]_i_4_n_0\,
      O => \reg_data_out__0\(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(19),
      I1 => \^l1a_count_out\(19),
      I2 => sel0(1),
      I3 => OCR_COUNT(19),
      I4 => sel0(0),
      I5 => L1A_COUNT(19),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(19),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(19),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(19),
      I1 => ECR_reg(19),
      I2 => sel0(1),
      I3 => BCR_reg(19),
      I4 => sel0(0),
      I5 => L1A_reg(19),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[1]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[1]_i_4_n_0\,
      O => \reg_data_out__0\(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(1),
      I1 => \^l1a_count_out\(1),
      I2 => sel0(1),
      I3 => OCR_COUNT(1),
      I4 => sel0(0),
      I5 => L1A_COUNT(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(1),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(1),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(1),
      I1 => ECR_reg(1),
      I2 => sel0(1),
      I3 => BCR_reg(1),
      I4 => sel0(0),
      I5 => L1A_reg(1),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[20]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[20]_i_4_n_0\,
      O => \reg_data_out__0\(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(20),
      I1 => \^l1a_count_out\(20),
      I2 => sel0(1),
      I3 => OCR_COUNT(20),
      I4 => sel0(0),
      I5 => L1A_COUNT(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(20),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(20),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(20),
      I1 => ECR_reg(20),
      I2 => sel0(1),
      I3 => BCR_reg(20),
      I4 => sel0(0),
      I5 => L1A_reg(20),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[21]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[21]_i_4_n_0\,
      O => \reg_data_out__0\(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(21),
      I1 => \^l1a_count_out\(21),
      I2 => sel0(1),
      I3 => OCR_COUNT(21),
      I4 => sel0(0),
      I5 => L1A_COUNT(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(21),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(21),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(21),
      I1 => ECR_reg(21),
      I2 => sel0(1),
      I3 => BCR_reg(21),
      I4 => sel0(0),
      I5 => L1A_reg(21),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[22]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[22]_i_4_n_0\,
      O => \reg_data_out__0\(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(22),
      I1 => \^l1a_count_out\(22),
      I2 => sel0(1),
      I3 => OCR_COUNT(22),
      I4 => sel0(0),
      I5 => L1A_COUNT(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(22),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(22),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(22),
      I1 => ECR_reg(22),
      I2 => sel0(1),
      I3 => BCR_reg(22),
      I4 => sel0(0),
      I5 => L1A_reg(22),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[23]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[23]_i_4_n_0\,
      O => \reg_data_out__0\(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(23),
      I1 => \^l1a_count_out\(23),
      I2 => sel0(1),
      I3 => OCR_COUNT(23),
      I4 => sel0(0),
      I5 => L1A_COUNT(23),
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(23),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(23),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(23),
      I1 => ECR_reg(23),
      I2 => sel0(1),
      I3 => BCR_reg(23),
      I4 => sel0(0),
      I5 => L1A_reg(23),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[24]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[24]_i_4_n_0\,
      O => \reg_data_out__0\(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(24),
      I1 => \^l1a_count_out\(24),
      I2 => sel0(1),
      I3 => OCR_COUNT(24),
      I4 => sel0(0),
      I5 => L1A_COUNT(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(24),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(24),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[24]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(24),
      I1 => ECR_reg(24),
      I2 => sel0(1),
      I3 => BCR_reg(24),
      I4 => sel0(0),
      I5 => L1A_reg(24),
      O => \axi_rdata[24]_i_4_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[25]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[25]_i_4_n_0\,
      O => \reg_data_out__0\(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(25),
      I1 => \^l1a_count_out\(25),
      I2 => sel0(1),
      I3 => OCR_COUNT(25),
      I4 => sel0(0),
      I5 => L1A_COUNT(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(25),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(25),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[25]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(25),
      I1 => ECR_reg(25),
      I2 => sel0(1),
      I3 => BCR_reg(25),
      I4 => sel0(0),
      I5 => L1A_reg(25),
      O => \axi_rdata[25]_i_4_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[26]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[26]_i_4_n_0\,
      O => \reg_data_out__0\(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(26),
      I1 => \^l1a_count_out\(26),
      I2 => sel0(1),
      I3 => OCR_COUNT(26),
      I4 => sel0(0),
      I5 => L1A_COUNT(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(26),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(26),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[26]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(26),
      I1 => ECR_reg(26),
      I2 => sel0(1),
      I3 => BCR_reg(26),
      I4 => sel0(0),
      I5 => L1A_reg(26),
      O => \axi_rdata[26]_i_4_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[27]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[27]_i_4_n_0\,
      O => \reg_data_out__0\(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(27),
      I1 => \^l1a_count_out\(27),
      I2 => sel0(1),
      I3 => OCR_COUNT(27),
      I4 => sel0(0),
      I5 => L1A_COUNT(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(27),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(27),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[27]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(27),
      I1 => ECR_reg(27),
      I2 => sel0(1),
      I3 => BCR_reg(27),
      I4 => sel0(0),
      I5 => L1A_reg(27),
      O => \axi_rdata[27]_i_4_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[28]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[28]_i_4_n_0\,
      O => \reg_data_out__0\(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(28),
      I1 => \^l1a_count_out\(28),
      I2 => sel0(1),
      I3 => OCR_COUNT(28),
      I4 => sel0(0),
      I5 => L1A_COUNT(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(28),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(28),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[28]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(28),
      I1 => ECR_reg(28),
      I2 => sel0(1),
      I3 => BCR_reg(28),
      I4 => sel0(0),
      I5 => L1A_reg(28),
      O => \axi_rdata[28]_i_4_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[29]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[29]_i_4_n_0\,
      O => \reg_data_out__0\(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(29),
      I1 => \^l1a_count_out\(29),
      I2 => sel0(1),
      I3 => OCR_COUNT(29),
      I4 => sel0(0),
      I5 => L1A_COUNT(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(29),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(29),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(29),
      I1 => ECR_reg(29),
      I2 => sel0(1),
      I3 => BCR_reg(29),
      I4 => sel0(0),
      I5 => L1A_reg(29),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[2]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[2]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[2]_i_4_n_0\,
      O => \reg_data_out__0\(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(2),
      I1 => \^l1a_count_out\(2),
      I2 => sel0(1),
      I3 => OCR_COUNT(2),
      I4 => sel0(0),
      I5 => L1A_COUNT(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(2),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(2),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(2),
      I1 => ECR_reg(2),
      I2 => sel0(1),
      I3 => BCR_reg(2),
      I4 => sel0(0),
      I5 => L1A_reg(2),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[30]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[30]_i_4_n_0\,
      O => \reg_data_out__0\(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(30),
      I1 => \^l1a_count_out\(30),
      I2 => sel0(1),
      I3 => OCR_COUNT(30),
      I4 => sel0(0),
      I5 => L1A_COUNT(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(30),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(30),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(30),
      I1 => ECR_reg(30),
      I2 => sel0(1),
      I3 => BCR_reg(30),
      I4 => sel0(0),
      I5 => L1A_reg(30),
      O => \axi_rdata[30]_i_4_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[31]_i_3_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[31]_i_4_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[31]_i_5_n_0\,
      O => \reg_data_out__0\(31)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(31),
      I1 => \^l1a_count_out\(31),
      I2 => sel0(1),
      I3 => OCR_COUNT(31),
      I4 => sel0(0),
      I5 => L1A_COUNT(31),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(31),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(31),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(31),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(31),
      I1 => ECR_reg(31),
      I2 => sel0(1),
      I3 => BCR_reg(31),
      I4 => sel0(0),
      I5 => L1A_reg(31),
      O => \axi_rdata[31]_i_5_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[3]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[3]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[3]_i_4_n_0\,
      O => \reg_data_out__0\(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(3),
      I1 => \^l1a_count_out\(3),
      I2 => sel0(1),
      I3 => OCR_COUNT(3),
      I4 => sel0(0),
      I5 => L1A_COUNT(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(3),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(3),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(3),
      I1 => ECR_reg(3),
      I2 => sel0(1),
      I3 => BCR_reg(3),
      I4 => sel0(0),
      I5 => L1A_reg(3),
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[4]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[4]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[4]_i_4_n_0\,
      O => \reg_data_out__0\(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(4),
      I1 => \^l1a_count_out\(4),
      I2 => sel0(1),
      I3 => OCR_COUNT(4),
      I4 => sel0(0),
      I5 => L1A_COUNT(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(4),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(4),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(4),
      I1 => ECR_reg(4),
      I2 => sel0(1),
      I3 => BCR_reg(4),
      I4 => sel0(0),
      I5 => L1A_reg(4),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[5]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[5]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[5]_i_4_n_0\,
      O => \reg_data_out__0\(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(5),
      I1 => \^l1a_count_out\(5),
      I2 => sel0(1),
      I3 => OCR_COUNT(5),
      I4 => sel0(0),
      I5 => L1A_COUNT(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(5),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(5),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(5),
      I1 => ECR_reg(5),
      I2 => sel0(1),
      I3 => BCR_reg(5),
      I4 => sel0(0),
      I5 => L1A_reg(5),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[6]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[6]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[6]_i_4_n_0\,
      O => \reg_data_out__0\(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(6),
      I1 => \^l1a_count_out\(6),
      I2 => sel0(1),
      I3 => OCR_COUNT(6),
      I4 => sel0(0),
      I5 => L1A_COUNT(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(6),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(6),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(6),
      I1 => ECR_reg(6),
      I2 => sel0(1),
      I3 => BCR_reg(6),
      I4 => sel0(0),
      I5 => L1A_reg(6),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[7]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[7]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[7]_i_4_n_0\,
      O => \reg_data_out__0\(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(7),
      I1 => \^l1a_count_out\(7),
      I2 => sel0(1),
      I3 => OCR_COUNT(7),
      I4 => sel0(0),
      I5 => L1A_COUNT(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(7),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(7),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(7),
      I1 => ECR_reg(7),
      I2 => sel0(1),
      I3 => BCR_reg(7),
      I4 => sel0(0),
      I5 => L1A_reg(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[8]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[8]_i_4_n_0\,
      O => \reg_data_out__0\(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(8),
      I1 => \^l1a_count_out\(8),
      I2 => sel0(1),
      I3 => OCR_COUNT(8),
      I4 => sel0(0),
      I5 => L1A_COUNT(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(8),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(8),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(8),
      I1 => ECR_reg(8),
      I2 => sel0(1),
      I3 => BCR_reg(8),
      I4 => sel0(0),
      I5 => L1A_reg(8),
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[9]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[9]_i_4_n_0\,
      O => \reg_data_out__0\(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => LUT_CONT_EN(9),
      I1 => \^l1a_count_out\(9),
      I2 => sel0(1),
      I3 => OCR_COUNT(9),
      I4 => sel0(0),
      I5 => L1A_COUNT(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \^ttc_config_out\(9),
      I1 => sel0(1),
      I2 => \^l1a_period_out\(9),
      I3 => sel0(0),
      I4 => \^ocr_period_out\(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^bcr_period_out\(9),
      I1 => ECR_reg(9),
      I2 => sel0(1),
      I3 => BCR_reg(9),
      I4 => sel0(0),
      I5 => L1A_reg(9),
      O => \axi_rdata[9]_i_4_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(0),
      Q => s00_axi_rdata(0),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(10),
      Q => s00_axi_rdata(10),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(11),
      Q => s00_axi_rdata(11),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(12),
      Q => s00_axi_rdata(12),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(13),
      Q => s00_axi_rdata(13),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(14),
      Q => s00_axi_rdata(14),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(15),
      Q => s00_axi_rdata(15),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(16),
      Q => s00_axi_rdata(16),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(17),
      Q => s00_axi_rdata(17),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(18),
      Q => s00_axi_rdata(18),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(19),
      Q => s00_axi_rdata(19),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(1),
      Q => s00_axi_rdata(1),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(20),
      Q => s00_axi_rdata(20),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(21),
      Q => s00_axi_rdata(21),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(22),
      Q => s00_axi_rdata(22),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(23),
      Q => s00_axi_rdata(23),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(24),
      Q => s00_axi_rdata(24),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(25),
      Q => s00_axi_rdata(25),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(26),
      Q => s00_axi_rdata(26),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(27),
      Q => s00_axi_rdata(27),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(28),
      Q => s00_axi_rdata(28),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(29),
      Q => s00_axi_rdata(29),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(2),
      Q => s00_axi_rdata(2),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(30),
      Q => s00_axi_rdata(30),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(31),
      Q => s00_axi_rdata(31),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(3),
      Q => s00_axi_rdata(3),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(4),
      Q => s00_axi_rdata(4),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(5),
      Q => s00_axi_rdata(5),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(6),
      Q => s00_axi_rdata(6),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(7),
      Q => s00_axi_rdata(7),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(8),
      Q => s00_axi_rdata(8),
      R => \L1A_reg[0]_i_1_n_0\
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(9),
      Q => s00_axi_rdata(9),
      R => \L1A_reg[0]_i_1_n_0\
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => \L1A_reg[0]_i_1_n_0\
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => \L1A_reg[0]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    L1A_out : out STD_LOGIC;
    BCR_out : out STD_LOGIC;
    ECR_out : out STD_LOGIC;
    BCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    BUSY_in : in STD_LOGIC;
    OCR_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0 is
begin
ttc_picozed_interface_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI
     port map (
      BCR_PERIOD_out(31 downto 0) => BCR_PERIOD_out(31 downto 0),
      BCR_out => BCR_out,
      BUSY_in => BUSY_in,
      ECR_out => ECR_out,
      L1A_COUNT(31 downto 0) => L1A_COUNT(31 downto 0),
      L1A_COUNT_out(31 downto 0) => L1A_COUNT_out(31 downto 0),
      L1A_PERIOD_out(31 downto 0) => L1A_PERIOD_out(31 downto 0),
      L1A_out => L1A_out,
      OCR_COUNT(31 downto 0) => OCR_COUNT(31 downto 0),
      OCR_PERIOD_out(31 downto 0) => OCR_PERIOD_out(31 downto 0),
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      TTC_CONFIG_out(31 downto 0) => TTC_CONFIG_out(31 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(3 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(3 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    L1A_out : out STD_LOGIC;
    BCR_out : out STD_LOGIC;
    ECR_out : out STD_LOGIC;
    BCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BUSY_in : in STD_LOGIC;
    L1A_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "TTC_PICOZED_BD_ttc_picozed_interface_0_0,ttc_picozed_interface_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ttc_picozed_interface_v1_0,Vivado 2018.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN TTC_PICOZED_BD_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 16, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN TTC_PICOZED_BD_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0
     port map (
      BCR_PERIOD_out(31 downto 0) => BCR_PERIOD_out(31 downto 0),
      BCR_out => BCR_out,
      BUSY_in => BUSY_in,
      ECR_out => ECR_out,
      L1A_COUNT(31 downto 0) => L1A_COUNT(31 downto 0),
      L1A_COUNT_out(31 downto 0) => L1A_COUNT_out(31 downto 0),
      L1A_PERIOD_out(31 downto 0) => L1A_PERIOD_out(31 downto 0),
      L1A_out => L1A_out,
      OCR_COUNT(31 downto 0) => OCR_COUNT(31 downto 0),
      OCR_PERIOD_out(31 downto 0) => OCR_PERIOD_out(31 downto 0),
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      TTC_CONFIG_out(31 downto 0) => TTC_CONFIG_out(31 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(5 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(5 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
