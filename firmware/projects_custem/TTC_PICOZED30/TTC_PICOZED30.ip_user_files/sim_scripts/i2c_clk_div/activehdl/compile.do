vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xilinx_vip  -sv2k12 "+incdir+../../../ipstatic" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../ipstatic" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../ipstatic" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/mesfin/tools/xilinx/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" "+incdir+../../../ipstatic" "+incdir+/home/mesfin/tools/xilinx/Vivado/2018.1/data/xilinx_vip/include" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/i2c_clk_div/i2c_clk_div_clk_wiz.v" \
"../../../../TTC_PICOZED30.srcs/sources_1/ip/i2c_clk_div/i2c_clk_div.v" \

vlog -work xil_defaultlib \
"glbl.v"

