onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib TTC_GTX2ZYNQ7000_opt

do {wave.do}

view wave
view structure
view signals

do {TTC_GTX2ZYNQ7000.udo}

run -all

quit -force
