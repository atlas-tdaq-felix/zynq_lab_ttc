onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib TTC_BRAM_L1A_opt

do {wave.do}

view wave
view structure
view signals

do {TTC_BRAM_L1A.udo}

run -all

quit -force
