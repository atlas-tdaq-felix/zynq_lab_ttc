library ieee;
use ieee.std_logic_1164.all;

						
entity i2c_write_read is
	port (
		clk: in std_logic;
		reset : in std_logic;
		clk_pls_n: in std_logic;
		clk_pls_p: in std_logic;
		slow_clk : in std_logic;
		address : in std_logic_vector(6 downto 0);

		smb_data_in : in std_logic;
		smb_clk_out : out std_logic;
		smb_data_out : out std_logic;
		data_cont : out std_logic;
		
		data_out : out std_logic_vector(7 downto 0);
		send_cmd : in std_logic_vector(7 downto 0);
		send_data : in std_logic_vector(7 downto 0);

		write_read_done : out std_logic
	);
end i2c_write_read;

architecture rtl of i2c_write_read is
	signal counter : integer range 0 to 127;
	signal data_cont_i, data_cont_ii, data_cont_iii : std_logic;
	signal smb_data_int, smb_data_int2, smb_data_int3 : std_logic;
	signal clk_cont : std_logic;
	
	signal clk_mask_low : std_logic;
	signal slow_clk_delay : std_logic;
	
begin
	

	process(clk)begin
		if(clk'event and clk = '1')then
			slow_clk_delay <= slow_clk;
		end if;
	end process;

	process(clk, reset)begin
		if(reset = '0')then
			smb_clk_out <= '1';
		elsif(clk'event and clk = '1')then
			smb_clk_out <= ((not clk_cont) or slow_clk_delay) and clk_mask_low;
		end if;
	end process;


		
	process(clk, reset)begin
		if(reset = '0')then
			data_cont <= '0';
			smb_data_out <= '1';
			data_cont_ii <= '0';
			data_cont_iii <= '0';
			smb_data_int2 <= '0';
			smb_data_int3 <= '0';
		elsif(clk'event and clk = '1')then
			if(clk_pls_n = '1')then
				data_cont_ii <= data_cont_i;
				smb_data_int2 <= smb_data_int;
			end if;
			smb_data_int3 <= smb_data_int2;
			smb_data_out <= smb_data_int3;
			data_cont_iii <= data_cont_ii;
			data_cont <= data_cont_iii;
		end if;
	end process;


	process(clk, reset)begin
		if(reset = '0')then
			counter <= 0;
			write_read_done <= '0';
			data_cont_i <= '0';
			clk_cont <= '0';
			clk_mask_low <= '1';
		elsif(clk'event and clk = '1')then
			if(clk_pls_p = '1')then
				if(counter = 0)then
					smb_data_int <= '1';
					counter <= 1;
					data_cont_i <= '0';
					clk_cont <= '0';
				elsif(counter = 1)then
					data_cont_i <= '1'; -- send start bit
					smb_data_int <= '0'; -- start bit as '0'
					counter <= counter + 1;
				elsif(counter = 2)then
					clk_cont <= '1';
					smb_data_int <= address(6);
					counter <= counter + 1;
				elsif(counter = 3)then
					smb_data_int <= address(5);
					counter <= counter + 1;
				elsif(counter = 4)then
					smb_data_int <= address(4);
					counter <= counter + 1;
				elsif(counter = 5)then
					smb_data_int <= address(3);
					counter <= counter + 1;
				elsif(counter = 6)then
					smb_data_int <= address(2);
					counter <= counter + 1;
				elsif(counter = 7)then
					smb_data_int <= address(1);
					counter <= counter + 1;
				elsif(counter = 8)then
					smb_data_int <= address(0);
					counter <= counter + 1;
				elsif(counter = 9)then
					smb_data_int <= '0'; -- write sign as '0'
					counter <= counter + 1;
				elsif(counter = 10)then
					counter <= counter + 1;
					data_cont_i <= '0'; -- tri-state data output and wait for ACK
				elsif(counter = 11)then
					if(smb_data_in = '0')then -- wait for ACK
						counter <= counter + 1;
						data_cont_i <= '1'; -- start sending command data / register address
						smb_data_int <= send_cmd(7);
					else
--						counter <= counter;
						counter <= 0;
					end if;
				elsif(counter = 12)then
					smb_data_int <= send_cmd(6);
					counter <= counter + 1;
				elsif(counter = 13)then
					smb_data_int <= send_cmd(5);
					counter <= counter + 1;
				elsif(counter = 14)then
					smb_data_int <= send_cmd(4);
					counter <= counter + 1;
				elsif(counter = 15)then
					smb_data_int <= send_cmd(3);
					counter <= counter + 1;
				elsif(counter = 16)then
					smb_data_int <= send_cmd(2);
					counter <= counter + 1;
				elsif(counter = 17)then
					smb_data_int <= send_cmd(1);
					counter <= counter + 1;
				elsif(counter = 18)then
					smb_data_int <= send_cmd(0);
					counter <= counter + 1;
				elsif(counter = 19)then
					data_cont_i <= '0'; -- tri-state data output and wait for ACK
					counter <= counter + 1;
				elsif(counter = 20)then
					if(smb_data_in = '0')then -- wait for ACK
						clk_cont <= '1';
						counter <= counter + 1;
					else
--						counter <= counter;
						counter <= 0;
					end if;
				elsif(counter = 21)then
					clk_mask_low <= '0';
					clk_cont <= '0';
					counter <= counter + 1;

				elsif(counter = 22)then
					counter <= counter + 1;
				elsif(counter = 23)then
					counter <= counter + 1;
				elsif(counter = 24)then
					counter <= counter + 1;
				elsif(counter = 25)then
					counter <= counter + 1;
				elsif(counter = 26)then
					counter <= counter + 1;
				elsif(counter = 27)then
					clk_mask_low <= '1';
					counter <= 100;
					
-- from here is the read section					
				elsif(counter = 100)then
					smb_data_int <= '1';
					data_cont_i <= '0';
					clk_cont <= '0';
					counter <= counter + 1;
				elsif(counter = 101)then
					data_cont_i <= '1';
					smb_data_int <= '0';
					counter <= counter + 1;
				elsif(counter = 102)then
					clk_cont <= '1';
					smb_data_int <= address(6);
					counter <= counter + 1;
				elsif(counter = 103)then
					smb_data_int <= address(5);
					counter <= counter + 1;
				elsif(counter = 104)then
					smb_data_int <= address(4);
					counter <= counter + 1;
				elsif(counter = 105)then
					smb_data_int <= address(3);
					counter <= counter + 1;
				elsif(counter = 106)then
					smb_data_int <= address(2);
					counter <= counter + 1;
				elsif(counter = 107)then
					smb_data_int <= address(1);
					counter <= counter + 1;
				elsif(counter = 108)then
					smb_data_int <= address(0);
					counter <= counter + 1;
				elsif(counter = 109)then
					smb_data_int <= '1'; -- read sign is 1
					counter <= counter + 1;
				elsif(counter = 110)then
					counter <= counter + 1;
					data_cont_i <= '0';
				elsif(counter = 111)then
					if(smb_data_in = '0')then
						counter <= counter + 1;
					else
--						counter <= counter;
						counter <= 0;
					end if;
				elsif(counter = 112)then
					data_out(7) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 113)then
					data_out(6) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 114)then
					data_out(5) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 115)then
					data_out(4) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 116)then
					data_out(3) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 117)then
					data_out(2) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 118)then
					data_out(1) <= smb_data_in;
					counter <= counter + 1;
				elsif(counter = 119)then
					data_out(0) <= smb_data_in;
					counter <= counter + 1;
					smb_data_int <= '1'; -- NAC bit
					clk_cont <= '1';
					data_cont_i <= '1';
				elsif(counter = 120)then
					clk_cont <= '1';
					smb_data_int <= '0';
					data_cont_i <= '1';
					counter <= counter + 1;
				elsif(counter = 121)then
					clk_cont <= '0';
					smb_data_int <= '1';
					data_cont_i <= '0';
					counter <= counter + 1;
				elsif(counter = 122)then
					counter <= counter + 1;
				elsif(counter = 123)then
					write_read_done <= '1';				
					counter <= counter;
				else
					counter <= counter + 1;
				end if;
			end if;
		end if;
	end process;
end rtl;

