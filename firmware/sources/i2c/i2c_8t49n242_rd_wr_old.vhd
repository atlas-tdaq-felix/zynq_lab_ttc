--------------------------------------------------------------------------------
-- Object        : Entity work.si570_master
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------



library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use xil_defaultlib.TTC709_pkg.all;

entity i2c_8t49n242_rd_wr is
  port (
    clk          : in     std_logic;
    core_state   : in     std_logic_vector(5 downto 0);
    write_state: out     std_logic_vector(5 downto 0);
    free         : in     std_logic;
    iic_reset    : out    std_logic;
    mstr_din     : out    std_logic_vector(7 downto 0);
    mstr_dout    : in     std_logic_vector(7 downto 0);
    read         : out    std_logic;
    ready        : in     std_logic;
    rec_ack      : in     std_logic;
    send_ack     : out    std_logic;
    clk_8t49n242_control    : in     clk_8t49n242_reg_type;
    si570_reset  : out    std_logic;
    clk_8t49n242_status    : out     clk_8t49n242_reg_type;
    start        : out    std_logic;
    stop         : out    std_logic;
    rd_en_pb	 : in 	  std_logic;	
    wr_en_pb	 : in 	  std_logic;	
    write        : out    std_logic);
end entity i2c_8t49n242_rd_wr;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_master.rtl
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------


architecture rtl of i2c_8t49n242_rd_wr is
	signal reset : std_logic;
	signal sw_reset : std_logic;
--	constant dev_address : std_logic_vector(6 downto 0):= "1110000"; --virtex eval kit
--	constant dev_address : std_logic_vector(6 downto 0):= "1011101"; --kintex eval kit
--	constant dev_address : std_logic_vector(6 downto 0):= "1101000"; --si5324 1101A(2)A(1)A(0)eval kit
	constant dev_address : std_logic_vector(6 downto 0):= "1101100"; --t49n242
        signal clk_8t49n242_reg : clk_8t49n242_reg_type := clk_8t49n242_init;
	signal counter : integer range 0 to 127 := 100;
	signal time50ms : std_logic;
	signal wait50start : std_logic;
	
           
	signal reset_init_cnt : std_logic_vector(31 downto 0) := (others => '0');
	constant max_reset_init_cnt : std_logic_vector(31 downto 0) := X"000000FF"; -- (others => '1');

	signal reset_init: std_logic:= '1';

	signal read_reg_state : integer range 0 to 100 := 0; 
	signal read_en : std_logic;
	signal read_en_t : std_logic;
	signal read_mstr_din: std_logic_vector(7 downto 0);
	signal read_start : std_logic;
	signal read_stop : std_logic;
	signal read_send_ack : std_logic;
	signal read_write : std_logic;
	signal read_read : std_logic;
	signal read_busy : std_logic;
	signal New_freq_en : std_logic;
	signal read_powerup_run : std_logic:= '1';
			           

	signal write_reg_state : integer range 0 to 100 := 0; 
	signal write_en : std_logic;
	signal write_en_t : std_logic;
	signal write_mstr_dout: std_logic_vector(7 downto 0);
	signal write_start : std_logic;
	signal write_stop : std_logic;
--	signal write_send_ack : std_logic;
	signal write_write : std_logic;
	signal write_read : std_logic;
	signal write_busy : std_logic;
	signal write_powerup_run : std_logic:= '1';


	signal reg_num : integer range 0 to 42 := 0;	
	constant max_reset_count : std_logic_vector(15 downto 0) := X"FFFF"; --FFFFFFFFFFFF";
	signal reset_count : std_logic_vector(15 downto 0) := X"00FF"; --FFFFFFFFFFFF";
	signal write_busy_d : std_logic := '0';

	signal rd_en_probe: std_logic_vector(0 downto 0);
	signal wr_en_probe: std_logic_vector(0 downto 0);
    signal rd_8t49n242_reg    : clk_8t49n242_reg_type;
    signal wr_8t49n242_reg    : clk_8t49n242_reg_type := clk_8t49n242_init;
	signal data_out7 : std_logic_vector(7 downto 0);
	signal data_out8 : std_logic_vector(7 downto 0);
	signal data_out9 : std_logic_vector(7 downto 0);
	signal data_out10 : std_logic_vector(7 downto 0);
	signal data_out11 : std_logic_vector(7 downto 0);
	signal data_out12 : std_logic_vector(7 downto 0);
	signal twobyte_onebyte_rdaddr : std_logic := '1';
	signal twobyte_onebyte_wraddr : std_logic := '1';
	signal i_cnt, j_cnt : integer;
	signal i_wrcnt, j_wrcnt : integer;
	constant i_cnt_max : integer := 24; -- max array size
	constant j_cnt_max : integer := 32; -- max number of bytes
	constant i_wrcnt_max : integer := 3; -- max array size
	constant j_wrcnt_max : integer := 32; -- max number of bytes
	signal f : integer range 0 to 31;
	signal l : integer range 0 to 3;
	
	
--component ila_rd_8t49n242
--   PORT (
--     clk :    IN STD_LOGIC;
--     probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--     probe1 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--     probe2 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--     probe3 : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
--     probe4 : IN STD_LOGIC_VECTOR(255 DOWNTO 0)
--   );
-- end component;


  signal stm_write 						: std_logic_vector(5 downto 0):= "000000";
  signal ila_stm_write 					: std_logic_vector(5 downto 0):= "000000";
  constant write_idle 					: std_logic_vector(5 downto 0):= "00"& x"0";
  constant write_init					: std_logic_vector(5 downto 0):= "00"& x"1";
  constant write_start_clk1				: std_logic_vector(5 downto 0):= "00"& x"2";
  constant write_start_clk2 			: std_logic_vector(5 downto 0):= "00"& x"3";
  constant write_start_clk3 			: std_logic_vector(5 downto 0):= "00"& x"4";
  constant write_saddr1_clk1 			: std_logic_vector(5 downto 0):= "00"& x"5";
  constant write_saddr1_clk2 			: std_logic_vector(5 downto 0):= "00"& x"6";
  constant write_saddr1_clk3 			: std_logic_vector(5 downto 0):= "00"& x"7";
  constant write_offset1_clk1 			: std_logic_vector(5 downto 0):= "00"& x"8";
  constant write_offset1_clk2 			: std_logic_vector(5 downto 0):= "00"& x"9";
  constant write_offset1_clk3 			: std_logic_vector(5 downto 0):= "00"& x"A";
  constant write_offset2_clk1 			: std_logic_vector(5 downto 0):= "00"& x"B";
  constant write_offset2_clk2 			: std_logic_vector(5 downto 0):= "00"& x"C";
  constant write_offset2_clk3 			: std_logic_vector(5 downto 0):= "00"& x"D";
  constant write_reg_n_clk1 			: std_logic_vector(5 downto 0):= "00"& x"E";
  constant write_reg_n_clk2 			: std_logic_vector(5 downto 0):= "00"& x"F";
  constant write_reg_n_clk3 			: std_logic_vector(5 downto 0):= "01"& x"0";
  constant write_reg_all_clk1 			: std_logic_vector(5 downto 0):= "01"& x"1";
  constant write_reg_all_clk2 			: std_logic_vector(5 downto 0):= "01"& x"2";
  constant write_reg_all_clk3 			: std_logic_vector(5 downto 0):= "01"& x"3";
  constant write_reg_m_clk1 			: std_logic_vector(5 downto 0):= "01"& x"4";
  constant write_reg_m_clk2 			: std_logic_vector(5 downto 0):= "01"& x"5";
  constant write_reg_m_clk3 			: std_logic_vector(5 downto 0):= "01"& x"6";
  constant write_reg_m_clk4 			: std_logic_vector(5 downto 0):= "01"& x"7";
  constant write_stop_clk1 				: std_logic_vector(5 downto 0):= "01"& x"8";
  constant write_stop_clk2 				: std_logic_vector(5 downto 0):= "01"& x"9";
	       
begin

--	rd_8t49n242_reg(0)(47 downto 0) <= data_out7 & data_out8 & data_out9 & data_out10 & data_out11 & data_out12;

	read_en <= rd_en_pb; --rd_en_probe(0);
	write_en <= wr_en_pb; --wr_en_probe(0);
	sw_reset <= '0';
    
	mstr_din 	<= read_mstr_din 	OR	write_mstr_dout;
	start 		<= read_start 		OR 	write_start;
	stop 		<= read_stop 		OR	write_stop;
	send_ack 	<= read_send_ack;
	write 		<= read_write  		OR 	write_write;
	read 		<= read_read 		OR 	write_read;
	
	write_state <= stm_write;
	rest_init_proc:process(clk) 
	begin
		if(clk'event and clk = '1')then 
	      if(reset_init_cnt /= max_reset_init_cnt) then    
	        reset_init <= '1';
	        reset_init_cnt <= reset_init_cnt + 1;
	      else  	
	        reset_init <= '0';
	      end if;  
		end if;
	end process;
	
--######################################################################
--**********************write proc******
--######################################################################
    twobyte_onebyte_wraddr <= '1';

	write_proc:process(clk, reset_init) begin
		if(reset_init = '1')then

			write_start <= '0';
			write_stop <= '0';
			write_write <= '0';
			write_read <= '0';

			write_busy <= '0'; 
			write_en_t <= '0';  

	        i_wrcnt <= 0; J_wrcnt <= 8;

			write_powerup_run <= '1';

			reset_count <= (others => '0');
           	write_mstr_dout <= (others => '0');     

			stm_write <= (others => '0');
			
		elsif(clk'event and clk = '1')then 
			write_en_t <= write_en;
			case stm_write is			
			 when write_idle =>			-- 0
			    write_start <= '0';
			    write_stop <= '0';
				write_write <= '0';
				write_read <= '0';

				write_busy <= '0';
	            i_wrcnt <= 0; J_wrcnt <= 8;
           		
           		write_mstr_dout <= (others => '0');
			 	
			 	if(write_en = '1' and write_en_t = '0') or (write_powerup_run = '0') then
			      write_powerup_run <= '1';
			 	  stm_write <= write_init;
			 	end if;
			 when write_init =>			-- 1
				if(free = '1')then 
			 	  stm_write <= write_start_clk1;
				end if;
			 when write_start_clk1 => 		-- 2
				if(read_busy = '0') then
					write_start <= '1';
					write_busy <= '1';
		 	  		stm_write <= write_start_clk2;
		 	  	end if;	
			 when write_start_clk2 => 	-- 3
			 	if(Ready ='0' ) then
					write_start <= '0';
			 	  	stm_write 	<= write_start_clk3;
			 	end if;
			 when write_start_clk3 => 					-- 4
			 	if(Ready ='1' ) then
			 	  stm_write 	<= write_saddr1_clk1;
			 	end if;
-- ##########
			 when write_saddr1_clk1 => 					-- 5
           		write_mstr_dout <= dev_address & '0';     
				write_write <= '1';
		 	  	stm_write 		<= write_saddr1_clk2;
			 when write_saddr1_clk2 =>					-- 6
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	stm_write 	<= write_saddr1_clk3;
			 	end if;
			 when write_saddr1_clk3 =>					-- 7
			 	if(Ready ='1' ) then
					if(rec_ack = '1') then
						stm_write 	<= write_offset1_clk1;
					else
					    write_stop <= '1';
						stm_write 	<= write_idle;
					end if;	
			 	end if;
-- ##########
			 when write_offset1_clk1 => 					-- 8
				write_mstr_dout <= X"08"; --(others => '0'); 		   
				write_write <= '1';
		 	  	stm_write 		<= write_offset1_clk2;
			 when write_offset1_clk2 =>					-- 9
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	stm_write 	<= write_offset1_clk3;
			 	end if;
			 when write_offset1_clk3 =>					-- 10
			 	if(Ready ='1' ) then
					if(rec_ack = '1') then
						stm_write 	<= write_offset2_clk1;
					else
					    write_stop <= '1';
						stm_write 	<= write_idle;
					end if;	
			 	end if;
-- ##########
			 when write_offset2_clk1 => 					-- 11
				write_mstr_dout <= (others => '0'); 		   
				write_write <= '1';
		 	  	stm_write 		<= write_offset2_clk2;
			 when write_offset2_clk2 =>					-- 12
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	stm_write 	<= write_offset2_clk3;
			 	end if;
			 when write_offset2_clk3 =>
			 	if(Ready ='1' ) then
					if(rec_ack = '1') then
						stm_write 	<= write_reg_n_clk1;
					else
					    write_stop <= '1';
						stm_write 	<= write_idle;
					end if;	
			 	end if;
-- ##########
			 when write_reg_n_clk1 => 				--14
					write_mstr_dout <= wr_8t49n242_reg(i_wrcnt)( (31 - j_wrcnt) * 8 + 7 downto (31 - j_wrcnt) * 8);
					write_write <= '1';
			 	  	stm_write <= write_reg_n_clk2;
			 when write_reg_n_clk2 => 				-- 15
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	stm_write <= write_reg_n_clk3;
			 	end if;
			 when write_reg_n_clk3 =>			-- 16
			 	if(Ready ='1' ) then
					if(rec_ack = '1') then
						stm_write <= write_reg_all_clk1;
					else
					    write_stop <= '1';
						stm_write 	<= write_idle;
					end if;	
			 	end if;
-- ##########
			 when write_reg_all_clk1 => -- send data
			   if(j_wrcnt < j_wrcnt_max - 1) then
				    i_wrcnt <= i_wrcnt;
					j_wrcnt <= j_wrcnt + 1;
					stm_write <= write_reg_n_clk1;
			   else
					if(i_wrcnt < i_wrcnt_max - 1) then
					    i_wrcnt <= i_wrcnt + 1;
					    j_wrcnt <= 0;
					    stm_write <= write_reg_n_clk1;
					else
					    j_wrcnt <= 0;
					    i_wrcnt <= i_wrcnt + 1; -- only 4 reg are nedded
					    stm_write <= write_reg_m_clk1;
					end if;
			    end if;
			 when write_reg_m_clk1 => 
					write_mstr_dout <= wr_8t49n242_reg(i_wrcnt)( (31 - j_wrcnt) * 8 + 7 downto (31 - j_wrcnt) * 8);
   			        --write_mstr_dout <= wr_8t49n242_reg(i_wrcnt)( (j_wrcnt * 8) + 7 downto (j_wrcnt * 8)); -- For not inverted byte order		   
					write_write <= '1';
			 		stm_write <= write_reg_m_clk2;
			 when write_reg_m_clk2 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	stm_write <= write_reg_m_clk3;
			 	end if;
			 when write_reg_m_clk3 =>
			 	if(Ready ='1' ) then
					if(rec_ack = '1') then
						stm_write <= write_reg_m_clk4;
					else
					    write_stop <= '1';
						stm_write 	<= write_idle;
					end if;	
			 	end if;
			 when write_reg_m_clk4 =>
--				   if(j_wrcnt < j_cnt_max - 1 - 4) then
				   if(j_wrcnt < j_cnt_max - 1 - 5) then -- no factory reg 0x7B
				        i_wrcnt <= i_wrcnt;
						j_wrcnt <= j_wrcnt + 1;
						stm_write <= write_reg_m_clk1;
				   else
				    	j_wrcnt <= 0;
				    	i_wrcnt <= 0;	
						stm_write <= write_stop_clk1;
			       end if;
			 when write_stop_clk1 =>  
					stm_write <= write_stop_clk2;
					write_stop <= '1';
			 when write_stop_clk2 =>  
--				if(reset_count /= max_reset_count) then
--					reset_count <= reset_count + 1;
--					stm_write <= write_stop_clk2;
--				else
					stm_write <= write_idle;
--					reset_count <= (others => '0');
--				end if;
			when others =>  null;
          end case;
		end if;
	end process;


--######################################################################
--**********************read proc******
--######################################################################
	read_proc:process(clk, reset_init) begin
		if(reset_init = '1')then
			--counter <= 100;
			read_reg_state <= 0;
           	read_mstr_din <= (others => '0');    
			read_start <= '0';
			read_stop <= '0';
		 	read_send_ack <= '0';
			read_write <= '0';
			read_read <= '0';  
			read_busy <= '0';
			read_en_t <= '0';
	 	    read_powerup_run <= '1';
--		    twobyte_onebyte_rdaddr <= '1';
			i_cnt <= 0;
            j_cnt <= 0;	
		elsif(clk'event and clk = '1')then 
			read_en_t <= read_en;
			case read_reg_state is			
			 when 0 =>
				read_busy <= '0';
           		read_mstr_din <= (others => '0');     
				read_start <= '0';
				read_stop <= '0';
		 		read_send_ack <= '0';
				read_write <= '0';
				read_read <= '0';
			    twobyte_onebyte_rdaddr <= '1';  
				i_cnt <= 0;
	            j_cnt <= 0;	
			 	if(read_en = '1' and read_en_t = '0') or (read_powerup_run = '0') then
			 	  read_powerup_run <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 1 =>
			    read_busy <= '1';
				if(free = '1')then -- iic is free
			 	  read_reg_state <= read_reg_state + 1;
				end if;

			 when 2 =>
				read_start <= '1';	
			 	read_reg_state <= read_reg_state + 1;
			 when 3 =>
				if(Ready = '0') then
				    read_start <= '0';	
			 	    read_reg_state <= read_reg_state + 1;
				end if;
			 when 4 =>
				if(Ready = '1') then
			 	    read_reg_state <= read_reg_state + 1;
				end if;
			 when 5 => -- iic slave addr to write
           		read_mstr_din <= dev_address & '0';     
				read_write <= '1';
		 	  	read_reg_state <= read_reg_state + 1;
			 when 6 =>
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 7 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 8 => -- iic slave reg addr.
			 	if(Rec_ack ='1') then
				  read_write <= '1';
           		  read_mstr_din <= (others => '0'); 		   
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 9 => 
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 10 =>
			 	if(Ready ='1') then
				  if(twobyte_onebyte_rdaddr = '1') then
				    twobyte_onebyte_rdaddr <= '0';
				    read_reg_state <= 8;
				  else
			 	  	read_reg_state <= read_reg_state + 1;
				  end if;
			 	end if;
			 when 11 => 
			 	if(Rec_ack ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
-- Start reading the register value
-- ################################			
			 when 12 =>
			 	   	read_reg_state <= read_reg_state + 1;
			 when 13 =>
			 	  read_reg_state <= read_reg_state + 1;
			 when 14 => -- repeted iic start
				read_start <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 15 =>
			 	if(Ready ='0') then
				  read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 16 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 17 => -- iic slave addr to read
           		read_mstr_din <= dev_address & '1';     
				read_write <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 18 =>
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 19 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 20 =>
			 	if(Rec_ack ='1') then
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 21 =>
				read_read <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 22 => 
			 	if(Ready ='0') then
				  read_read <= '0'; 
--				  if(i_cnt = i_cnt_max -1 and j_cnt = j_cnt_max - 1) then
--				  	read_send_ack <= '0';
--				  else
				  	read_send_ack <= '1';
--				  end if;	
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 23 => 
			 	if(Ready ='1') then
			  	   rd_8t49n242_reg(i_cnt)( (j_cnt * 8) + 7 downto (j_cnt * 8)) <= mstr_dout;
				   if(j_cnt < j_cnt_max - 1) then
				        i_cnt <= i_cnt;
				    	j_cnt <= j_cnt + 1;
					    read_reg_state <= 21;
				   else
					if(i_cnt < i_cnt_max - 1) then
					    i_cnt <= i_cnt + 1;
					    j_cnt <= 0;
					    read_reg_state <= 21;
					else
					    j_cnt <= 0;
					    i_cnt <= i_cnt + 1; -- to read the last short register	
					    read_reg_state <= read_reg_state + 1;
					end if;
			       end if;
			 	end if;

			 when 24 =>
				read_read <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 25 => 
			 	if(Ready ='0') then
				  read_read <= '0'; 
				  if(i_cnt = i_cnt_max and j_cnt = j_cnt_max - 1 - 9) then
				  	read_send_ack <= '0';
				  else
				  	read_send_ack <= '1';
				  end if;	
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 26 => 
			 	if(Ready ='1') then
			  	   rd_8t49n242_reg(i_cnt)( (j_cnt * 8) + 7 downto (j_cnt * 8)) <= mstr_dout;
				   if(j_cnt < j_cnt_max - 1 - 9) then
				    i_cnt <= i_cnt;
					j_cnt <= j_cnt + 1;
					read_reg_state <= 24;
				   else
				   	j_cnt <= 0;
				   	i_cnt <= 0;	
				  	read_reg_state <= read_reg_state + 1;
			       end if;
			 	end if;

-- Send read stop condition
--========read 135=============
			 when 27 =>
				read_stop <= '1';
			  	read_reg_state <= read_reg_state + 1;
			 when 28 => 
			 	if(Ready ='0') then
				  read_stop <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 29 => 
				read_reg_state <= 0;
			 when others => NULL;
          		end case;
		end if;
	end process;

iic_reset <= reset_init;

end architecture rtl ; -- of 8t49n242_master
