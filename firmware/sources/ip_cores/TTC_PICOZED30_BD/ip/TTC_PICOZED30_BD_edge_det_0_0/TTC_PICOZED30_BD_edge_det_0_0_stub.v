// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (win64) Build 2188600 Wed Apr  4 18:40:38 MDT 2018
// Date        : Thu Aug  6 14:34:20 2020
// Host        : Mesfin running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_edge_det_0_0/TTC_PICOZED30_BD_edge_det_0_0_stub.v
// Design      : TTC_PICOZED30_BD_edge_det_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "edge_det,Vivado 2018.1" *)
module TTC_PICOZED30_BD_edge_det_0_0(clk_160M, gpio_in, ECR_r, triger_r, ECR_push, 
  BCR_push, triger_push, BCR_r)
/* synthesis syn_black_box black_box_pad_pin="clk_160M,gpio_in[5:0],ECR_r,triger_r,ECR_push,BCR_push,triger_push,BCR_r" */;
  input clk_160M;
  input [5:0]gpio_in;
  output ECR_r;
  output triger_r;
  input ECR_push;
  input BCR_push;
  input triger_push;
  output BCR_r;
endmodule
