set_property DCI_CASCADE {37 39} [get_iobanks 38]

set_property BITSTREAM.CONFIG.BPI_SYNC_MODE TYPE1 [current_design]
set_property BITSTREAM.CONFIG.M0PIN PULLUP [current_design]
set_property BITSTREAM.CONFIG.M1PIN PULLUP [current_design]
set_property BITSTREAM.CONFIG.M2PIN PULLUP [current_design]
set_property BITSTREAM.CONFIG.BPI_PAGE_SIZE 1 [current_design]

set_property BITSTREAM.GENERAL.COMPRESS FALSE [current_design]

set_property BITSTREAM.CONFIG.CONFIGRATE 40 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DIV-2 [current_design]

set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property CFGBVS GND [current_design]
set_property CONFIG_MODE BPI16 [current_design]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
#generate mcs file
#write_cfgmem -format mcs -interface bpix16 -size 128 -loadbit "up 0x0 D:/Users/mgebyehu/VirgoPC_vhdl_rev10/syn/Vivado_synth/Vivado_synth.runs/impl_1/VirgoPC_vhdl_top.bit" -file D:/Users/mgebyehu/VirgoPC_vhdl_rev10/syn/Vivado_synth/Vivado_synth.runs/impl_1/VirgoPC_vhdl_top.mcs -force
#BPI Flash part name mt28gu01gaax1e-bpi-x16
