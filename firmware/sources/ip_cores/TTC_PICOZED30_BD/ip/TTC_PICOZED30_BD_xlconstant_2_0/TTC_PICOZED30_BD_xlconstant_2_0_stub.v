// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Fri Jan 17 15:51:52 2020
// Host        : mesfin-Precision-T3600 running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /mnt/ad/project/ET/Atlas/Atlas_Felix/mgebyehu/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_xlconstant_2_0/TTC_PICOZED30_BD_xlconstant_2_0_stub.v
// Design      : TTC_PICOZED30_BD_xlconstant_2_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlconstant_v1_1_4_xlconstant,Vivado 2018.1" *)
module TTC_PICOZED30_BD_xlconstant_2_0(dout)
/* synthesis syn_black_box black_box_pad_pin="dout[0:0]" */;
  output [0:0]dout;
endmodule
