-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Tue Mar 22 12:37:46 2022
-- Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/mesfin/felix/mgebyehu/TTC_ZYNQ/firmware/projects_custem/TTC_PICOZED30/TTC_PICOZED30.srcs/sources_1/ip/ila_rd_8t49n242_1/ila_rd_8t49n242_stub.vhdl
-- Design      : ila_rd_8t49n242
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ila_rd_8t49n242 is
  Port ( 
    clk : in STD_LOGIC;
    probe0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe2 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    probe3 : in STD_LOGIC_VECTOR ( 255 downto 0 );
    probe4 : in STD_LOGIC_VECTOR ( 255 downto 0 );
    probe5 : in STD_LOGIC_VECTOR ( 255 downto 0 );
    probe6 : in STD_LOGIC_VECTOR ( 255 downto 0 );
    probe7 : in STD_LOGIC_VECTOR ( 255 downto 0 )
  );

end ila_rd_8t49n242;

architecture stub of ila_rd_8t49n242 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe0[0:0],probe1[0:0],probe2[5:0],probe3[255:0],probe4[255:0],probe5[255:0],probe6[255:0],probe7[255:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "ila,Vivado 2018.1";
begin
end;
