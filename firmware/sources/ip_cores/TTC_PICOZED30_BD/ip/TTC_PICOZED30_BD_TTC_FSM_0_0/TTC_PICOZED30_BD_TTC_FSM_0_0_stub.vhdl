-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Fri Jan 17 11:12:44 2020
-- Host        : mesfin-Precision-T3600 running 64-bit Ubuntu 18.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /mnt/ad/project/ET/Atlas/Atlas_Felix/mgebyehu/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_TTC_FSM_0_0/TTC_PICOZED30_BD_TTC_FSM_0_0_stub.vhdl
-- Design      : TTC_PICOZED30_BD_TTC_FSM_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TTC_PICOZED30_BD_TTC_FSM_0_0 is
  Port ( 
    clk_160M : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    ECR_r : in STD_LOGIC;
    triger_r : in STD_LOGIC;
    BCR_r : in STD_LOGIC;
    TTC_out : out STD_LOGIC
  );

end TTC_PICOZED30_BD_TTC_FSM_0_0;

architecture stub of TTC_PICOZED30_BD_TTC_FSM_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_160M,sys_rst_n,ECR_r,triger_r,BCR_r,TTC_out";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "TTC_FSM,Vivado 2018.1";
begin
end;
