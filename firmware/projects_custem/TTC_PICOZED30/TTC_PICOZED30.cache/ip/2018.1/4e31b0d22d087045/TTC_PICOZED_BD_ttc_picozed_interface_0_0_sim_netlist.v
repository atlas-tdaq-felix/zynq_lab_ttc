// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Thu Jul 14 13:53:25 2022
// Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ TTC_PICOZED_BD_ttc_picozed_interface_0_0_sim_netlist.v
// Design      : TTC_PICOZED_BD_ttc_picozed_interface_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "TTC_PICOZED_BD_ttc_picozed_interface_0_0,ttc_picozed_interface_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ttc_picozed_interface_v1_0,Vivado 2018.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (L1A_out,
    BCR_out,
    ECR_out,
    BCR_PERIOD_out,
    OCR_PERIOD_out,
    L1A_PERIOD_out,
    TTC_CONFIG_out,
    L1A_COUNT_out,
    BUSY_in,
    L1A_COUNT,
    OCR_COUNT,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  output L1A_out;
  output BCR_out;
  output ECR_out;
  output [31:0]BCR_PERIOD_out;
  output [31:0]OCR_PERIOD_out;
  output [31:0]L1A_PERIOD_out;
  output [31:0]TTC_CONFIG_out;
  output [31:0]L1A_COUNT_out;
  input BUSY_in;
  input [31:0]L1A_COUNT;
  input [31:0]OCR_COUNT;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 16, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 5e+07, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN TTC_PICOZED_BD_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 5e+07, PHASE 0.000, CLK_DOMAIN TTC_PICOZED_BD_processing_system7_0_0_FCLK_CLK0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [31:0]BCR_PERIOD_out;
  wire BCR_out;
  wire BUSY_in;
  wire ECR_out;
  wire [31:0]L1A_COUNT;
  wire [31:0]L1A_COUNT_out;
  wire [31:0]L1A_PERIOD_out;
  wire L1A_out;
  wire [31:0]OCR_COUNT;
  wire [31:0]OCR_PERIOD_out;
  wire [31:0]TTC_CONFIG_out;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0 U0
       (.BCR_PERIOD_out(BCR_PERIOD_out),
        .BCR_out(BCR_out),
        .BUSY_in(BUSY_in),
        .ECR_out(ECR_out),
        .L1A_COUNT(L1A_COUNT),
        .L1A_COUNT_out(L1A_COUNT_out),
        .L1A_PERIOD_out(L1A_PERIOD_out),
        .L1A_out(L1A_out),
        .OCR_COUNT(OCR_COUNT),
        .OCR_PERIOD_out(OCR_PERIOD_out),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .TTC_CONFIG_out(TTC_CONFIG_out),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[5:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[5:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    L1A_out,
    BCR_out,
    ECR_out,
    BCR_PERIOD_out,
    OCR_PERIOD_out,
    L1A_PERIOD_out,
    L1A_COUNT_out,
    TTC_CONFIG_out,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    BUSY_in,
    OCR_COUNT,
    L1A_COUNT,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output L1A_out;
  output BCR_out;
  output ECR_out;
  output [31:0]BCR_PERIOD_out;
  output [31:0]OCR_PERIOD_out;
  output [31:0]L1A_PERIOD_out;
  output [31:0]L1A_COUNT_out;
  output [31:0]TTC_CONFIG_out;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input BUSY_in;
  input [31:0]OCR_COUNT;
  input [31:0]L1A_COUNT;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [31:0]BCR_PERIOD_out;
  wire BCR_out;
  wire BUSY_in;
  wire ECR_out;
  wire [31:0]L1A_COUNT;
  wire [31:0]L1A_COUNT_out;
  wire [31:0]L1A_PERIOD_out;
  wire L1A_out;
  wire [31:0]OCR_COUNT;
  wire [31:0]OCR_PERIOD_out;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [31:0]TTC_CONFIG_out;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI ttc_picozed_interface_v1_0_S00_AXI_inst
       (.BCR_PERIOD_out(BCR_PERIOD_out),
        .BCR_out(BCR_out),
        .BUSY_in(BUSY_in),
        .ECR_out(ECR_out),
        .L1A_COUNT(L1A_COUNT),
        .L1A_COUNT_out(L1A_COUNT_out),
        .L1A_PERIOD_out(L1A_PERIOD_out),
        .L1A_out(L1A_out),
        .OCR_COUNT(OCR_COUNT),
        .OCR_PERIOD_out(OCR_PERIOD_out),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .TTC_CONFIG_out(TTC_CONFIG_out),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ttc_picozed_interface_v1_0_S00_AXI
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    L1A_out,
    BCR_out,
    ECR_out,
    BCR_PERIOD_out,
    OCR_PERIOD_out,
    L1A_PERIOD_out,
    L1A_COUNT_out,
    TTC_CONFIG_out,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    BUSY_in,
    OCR_COUNT,
    L1A_COUNT,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output L1A_out;
  output BCR_out;
  output ECR_out;
  output [31:0]BCR_PERIOD_out;
  output [31:0]OCR_PERIOD_out;
  output [31:0]L1A_PERIOD_out;
  output [31:0]L1A_COUNT_out;
  output [31:0]TTC_CONFIG_out;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input BUSY_in;
  input [31:0]OCR_COUNT;
  input [31:0]L1A_COUNT;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [31:0]BCR_PERIOD_out;
  wire \BCR_PERIOD_reg[15]_i_1_n_0 ;
  wire \BCR_PERIOD_reg[23]_i_1_n_0 ;
  wire \BCR_PERIOD_reg[31]_i_1_n_0 ;
  wire \BCR_PERIOD_reg[7]_i_1_n_0 ;
  wire BCR_out;
  wire [31:1]BCR_reg;
  wire \BCR_reg[0]_i_1_n_0 ;
  wire \BCR_reg[15]_i_1_n_0 ;
  wire \BCR_reg[23]_i_1_n_0 ;
  wire \BCR_reg[31]_i_1_n_0 ;
  wire BUSY_in;
  wire ECR_out;
  wire [31:1]ECR_reg;
  wire \ECR_reg[0]_i_1_n_0 ;
  wire \ECR_reg[15]_i_1_n_0 ;
  wire \ECR_reg[23]_i_1_n_0 ;
  wire \ECR_reg[31]_i_1_n_0 ;
  wire [31:0]L1A_COUNT;
  wire [31:0]L1A_COUNT_out;
  wire \L1A_COUNT_reg[15]_i_1_n_0 ;
  wire \L1A_COUNT_reg[23]_i_1_n_0 ;
  wire \L1A_COUNT_reg[31]_i_1_n_0 ;
  wire \L1A_COUNT_reg[7]_i_1_n_0 ;
  wire [31:0]L1A_PERIOD_out;
  wire \L1A_PERIOD_reg[15]_i_1_n_0 ;
  wire \L1A_PERIOD_reg[23]_i_1_n_0 ;
  wire \L1A_PERIOD_reg[31]_i_1_n_0 ;
  wire \L1A_PERIOD_reg[7]_i_1_n_0 ;
  wire L1A_out;
  wire [31:1]L1A_reg;
  wire \L1A_reg[0]_i_1_n_0 ;
  wire [31:0]LUT_CONT_EN;
  wire \LUT_CONT_EN_reg[15]_i_1_n_0 ;
  wire \LUT_CONT_EN_reg[23]_i_1_n_0 ;
  wire \LUT_CONT_EN_reg[31]_i_1_n_0 ;
  wire \LUT_CONT_EN_reg[7]_i_1_n_0 ;
  wire [31:0]OCR_COUNT;
  wire [31:0]OCR_PERIOD_out;
  wire \OCR_PERIOD_reg[15]_i_1_n_0 ;
  wire \OCR_PERIOD_reg[23]_i_1_n_0 ;
  wire \OCR_PERIOD_reg[31]_i_1_n_0 ;
  wire \OCR_PERIOD_reg[7]_i_1_n_0 ;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [31:0]TTC_CONFIG_out;
  wire \TTC_CONFIG_reg[15]_i_1_n_0 ;
  wire \TTC_CONFIG_reg[23]_i_1_n_0 ;
  wire \TTC_CONFIG_reg[31]_i_1_n_0 ;
  wire \TTC_CONFIG_reg[7]_i_1_n_0 ;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[31]_i_5_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [3:0]p_0_in;
  wire [31:0]p_1_in;
  wire [31:0]reg_data_out__0;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]sel0;
  wire slv_reg_rden;
  wire slv_reg_wren__2;

  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \BCR_PERIOD_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\BCR_PERIOD_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \BCR_PERIOD_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\BCR_PERIOD_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \BCR_PERIOD_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\BCR_PERIOD_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \BCR_PERIOD_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\BCR_PERIOD_reg[7]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(BCR_PERIOD_out[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(BCR_PERIOD_out[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(BCR_PERIOD_out[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(BCR_PERIOD_out[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(BCR_PERIOD_out[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(BCR_PERIOD_out[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(BCR_PERIOD_out[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(BCR_PERIOD_out[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(BCR_PERIOD_out[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(BCR_PERIOD_out[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(BCR_PERIOD_out[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(BCR_PERIOD_out[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(BCR_PERIOD_out[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(BCR_PERIOD_out[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(BCR_PERIOD_out[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(BCR_PERIOD_out[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(BCR_PERIOD_out[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(BCR_PERIOD_out[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(BCR_PERIOD_out[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(BCR_PERIOD_out[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(BCR_PERIOD_out[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(BCR_PERIOD_out[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(BCR_PERIOD_out[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(BCR_PERIOD_out[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(BCR_PERIOD_out[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(BCR_PERIOD_out[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(BCR_PERIOD_out[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(BCR_PERIOD_out[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(BCR_PERIOD_out[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(BCR_PERIOD_out[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(BCR_PERIOD_out[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_PERIOD_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\BCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(BCR_PERIOD_out[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \BCR_reg[0]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\BCR_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \BCR_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\BCR_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \BCR_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\BCR_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \BCR_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(\BCR_reg[31]_i_1_n_0 ));
  FDRE \BCR_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(BCR_out),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(BCR_reg[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(BCR_reg[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(BCR_reg[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(BCR_reg[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(BCR_reg[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(BCR_reg[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(BCR_reg[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(BCR_reg[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(BCR_reg[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(BCR_reg[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(BCR_reg[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(BCR_reg[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(BCR_reg[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(BCR_reg[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(BCR_reg[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(BCR_reg[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(BCR_reg[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(BCR_reg[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(BCR_reg[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(BCR_reg[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(BCR_reg[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(BCR_reg[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(BCR_reg[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(BCR_reg[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(BCR_reg[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(BCR_reg[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(BCR_reg[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(BCR_reg[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(BCR_reg[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(BCR_reg[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \BCR_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\BCR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(BCR_reg[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \ECR_reg[0]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\ECR_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \ECR_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\ECR_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \ECR_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\ECR_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \ECR_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\ECR_reg[31]_i_1_n_0 ));
  FDRE \ECR_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(ECR_out),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(ECR_reg[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(ECR_reg[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(ECR_reg[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(ECR_reg[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(ECR_reg[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(ECR_reg[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(ECR_reg[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(ECR_reg[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(ECR_reg[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(ECR_reg[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ECR_reg[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(ECR_reg[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(ECR_reg[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(ECR_reg[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(ECR_reg[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(ECR_reg[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(ECR_reg[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(ECR_reg[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(ECR_reg[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(ECR_reg[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(ECR_reg[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(ECR_reg[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(ECR_reg[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(ECR_reg[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(ECR_reg[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(ECR_reg[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(ECR_reg[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(ECR_reg[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[0]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(ECR_reg[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(ECR_reg[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \ECR_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\ECR_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(ECR_reg[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_COUNT_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\L1A_COUNT_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_COUNT_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\L1A_COUNT_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_COUNT_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\L1A_COUNT_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_COUNT_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[3]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\L1A_COUNT_reg[7]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(L1A_COUNT_out[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(L1A_COUNT_out[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(L1A_COUNT_out[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(L1A_COUNT_out[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(L1A_COUNT_out[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(L1A_COUNT_out[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(L1A_COUNT_out[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(L1A_COUNT_out[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(L1A_COUNT_out[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(L1A_COUNT_out[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(L1A_COUNT_out[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(L1A_COUNT_out[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(L1A_COUNT_out[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(L1A_COUNT_out[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(L1A_COUNT_out[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(L1A_COUNT_out[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(L1A_COUNT_out[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(L1A_COUNT_out[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(L1A_COUNT_out[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(L1A_COUNT_out[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(L1A_COUNT_out[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(L1A_COUNT_out[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(L1A_COUNT_out[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(L1A_COUNT_out[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(L1A_COUNT_out[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(L1A_COUNT_out[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(L1A_COUNT_out[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(L1A_COUNT_out[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(L1A_COUNT_out[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(L1A_COUNT_out[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(L1A_COUNT_out[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_COUNT_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\L1A_COUNT_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(L1A_COUNT_out[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_PERIOD_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\L1A_PERIOD_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_PERIOD_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\L1A_PERIOD_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_PERIOD_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\L1A_PERIOD_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \L1A_PERIOD_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\L1A_PERIOD_reg[7]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(L1A_PERIOD_out[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(L1A_PERIOD_out[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(L1A_PERIOD_out[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(L1A_PERIOD_out[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(L1A_PERIOD_out[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(L1A_PERIOD_out[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(L1A_PERIOD_out[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(L1A_PERIOD_out[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(L1A_PERIOD_out[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(L1A_PERIOD_out[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(L1A_PERIOD_out[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(L1A_PERIOD_out[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(L1A_PERIOD_out[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(L1A_PERIOD_out[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(L1A_PERIOD_out[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(L1A_PERIOD_out[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(L1A_PERIOD_out[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(L1A_PERIOD_out[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(L1A_PERIOD_out[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(L1A_PERIOD_out[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(L1A_PERIOD_out[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(L1A_PERIOD_out[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(L1A_PERIOD_out[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(L1A_PERIOD_out[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(L1A_PERIOD_out[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(L1A_PERIOD_out[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(L1A_PERIOD_out[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(L1A_PERIOD_out[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(L1A_PERIOD_out[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(L1A_PERIOD_out[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(L1A_PERIOD_out[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_PERIOD_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\L1A_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(L1A_PERIOD_out[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \L1A_reg[0]_i_1 
       (.I0(s00_axi_aresetn),
        .O(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \L1A_reg[0]_i_2 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[0]),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'h8000)) 
    \L1A_reg[0]_i_3 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \L1A_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \L1A_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \L1A_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  FDRE \L1A_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[0]),
        .Q(L1A_out),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(L1A_reg[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(L1A_reg[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(L1A_reg[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(L1A_reg[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(L1A_reg[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(L1A_reg[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(L1A_reg[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(L1A_reg[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(L1A_reg[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(L1A_reg[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[1]),
        .Q(L1A_reg[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(L1A_reg[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(L1A_reg[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(L1A_reg[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(L1A_reg[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(L1A_reg[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(L1A_reg[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(L1A_reg[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(L1A_reg[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(L1A_reg[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(L1A_reg[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[2]),
        .Q(L1A_reg[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(L1A_reg[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(L1A_reg[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[3]),
        .Q(L1A_reg[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[4]),
        .Q(L1A_reg[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[5]),
        .Q(L1A_reg[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[6]),
        .Q(L1A_reg[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[0]),
        .D(s00_axi_wdata[7]),
        .Q(L1A_reg[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(L1A_reg[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \L1A_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(L1A_reg[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \LUT_CONT_EN_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\LUT_CONT_EN_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \LUT_CONT_EN_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\LUT_CONT_EN_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \LUT_CONT_EN_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\LUT_CONT_EN_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \LUT_CONT_EN_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[2]),
        .O(\LUT_CONT_EN_reg[7]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(LUT_CONT_EN[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(LUT_CONT_EN[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(LUT_CONT_EN[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(LUT_CONT_EN[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(LUT_CONT_EN[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(LUT_CONT_EN[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(LUT_CONT_EN[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(LUT_CONT_EN[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(LUT_CONT_EN[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(LUT_CONT_EN[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(LUT_CONT_EN[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(LUT_CONT_EN[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(LUT_CONT_EN[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(LUT_CONT_EN[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(LUT_CONT_EN[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(LUT_CONT_EN[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(LUT_CONT_EN[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(LUT_CONT_EN[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(LUT_CONT_EN[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(LUT_CONT_EN[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(LUT_CONT_EN[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(LUT_CONT_EN[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(LUT_CONT_EN[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(LUT_CONT_EN[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(LUT_CONT_EN[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(LUT_CONT_EN[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(LUT_CONT_EN[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(LUT_CONT_EN[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(LUT_CONT_EN[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(LUT_CONT_EN[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(LUT_CONT_EN[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \LUT_CONT_EN_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\LUT_CONT_EN_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(LUT_CONT_EN[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \OCR_PERIOD_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\OCR_PERIOD_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \OCR_PERIOD_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\OCR_PERIOD_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \OCR_PERIOD_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\OCR_PERIOD_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \OCR_PERIOD_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\OCR_PERIOD_reg[7]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(OCR_PERIOD_out[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(OCR_PERIOD_out[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(OCR_PERIOD_out[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(OCR_PERIOD_out[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(OCR_PERIOD_out[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(OCR_PERIOD_out[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(OCR_PERIOD_out[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(OCR_PERIOD_out[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(OCR_PERIOD_out[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(OCR_PERIOD_out[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(OCR_PERIOD_out[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(OCR_PERIOD_out[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(OCR_PERIOD_out[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(OCR_PERIOD_out[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(OCR_PERIOD_out[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(OCR_PERIOD_out[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(OCR_PERIOD_out[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(OCR_PERIOD_out[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(OCR_PERIOD_out[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(OCR_PERIOD_out[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(OCR_PERIOD_out[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(OCR_PERIOD_out[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(OCR_PERIOD_out[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(OCR_PERIOD_out[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(OCR_PERIOD_out[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(OCR_PERIOD_out[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(OCR_PERIOD_out[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(OCR_PERIOD_out[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(OCR_PERIOD_out[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(OCR_PERIOD_out[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(OCR_PERIOD_out[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \OCR_PERIOD_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\OCR_PERIOD_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(OCR_PERIOD_out[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \TTC_CONFIG_reg[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\TTC_CONFIG_reg[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \TTC_CONFIG_reg[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\TTC_CONFIG_reg[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \TTC_CONFIG_reg[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\TTC_CONFIG_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \TTC_CONFIG_reg[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\TTC_CONFIG_reg[7]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(TTC_CONFIG_out[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(TTC_CONFIG_out[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(TTC_CONFIG_out[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(TTC_CONFIG_out[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(TTC_CONFIG_out[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(TTC_CONFIG_out[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(TTC_CONFIG_out[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(TTC_CONFIG_out[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(TTC_CONFIG_out[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(TTC_CONFIG_out[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(TTC_CONFIG_out[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(TTC_CONFIG_out[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(TTC_CONFIG_out[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(TTC_CONFIG_out[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(TTC_CONFIG_out[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(TTC_CONFIG_out[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(TTC_CONFIG_out[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(TTC_CONFIG_out[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(TTC_CONFIG_out[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(TTC_CONFIG_out[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(TTC_CONFIG_out[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(TTC_CONFIG_out[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(TTC_CONFIG_out[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(TTC_CONFIG_out[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(TTC_CONFIG_out[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(TTC_CONFIG_out[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(TTC_CONFIG_out[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(TTC_CONFIG_out[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(TTC_CONFIG_out[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(TTC_CONFIG_out[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(TTC_CONFIG_out[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \TTC_CONFIG_reg_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\TTC_CONFIG_reg[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(TTC_CONFIG_out[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(\L1A_reg[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(\L1A_reg[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(\L1A_reg[0]_i_1_n_0 ));
  FDSE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .S(\L1A_reg[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[0]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[0]_i_4_n_0 ),
        .O(reg_data_out__0[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(LUT_CONT_EN[0]),
        .I1(L1A_COUNT_out[0]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[0]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(BUSY_in),
        .I1(TTC_CONFIG_out[0]),
        .I2(sel0[1]),
        .I3(L1A_PERIOD_out[0]),
        .I4(sel0[0]),
        .I5(OCR_PERIOD_out[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_4 
       (.I0(BCR_PERIOD_out[0]),
        .I1(ECR_out),
        .I2(sel0[1]),
        .I3(BCR_out),
        .I4(sel0[0]),
        .I5(L1A_out),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[10]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[10]_i_4_n_0 ),
        .O(reg_data_out__0[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(LUT_CONT_EN[10]),
        .I1(L1A_COUNT_out[10]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[10]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_3 
       (.I0(TTC_CONFIG_out[10]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[10]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_4 
       (.I0(BCR_PERIOD_out[10]),
        .I1(ECR_reg[10]),
        .I2(sel0[1]),
        .I3(BCR_reg[10]),
        .I4(sel0[0]),
        .I5(L1A_reg[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[11]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[11]_i_4_n_0 ),
        .O(reg_data_out__0[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(LUT_CONT_EN[11]),
        .I1(L1A_COUNT_out[11]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[11]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_3 
       (.I0(TTC_CONFIG_out[11]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[11]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_4 
       (.I0(BCR_PERIOD_out[11]),
        .I1(ECR_reg[11]),
        .I2(sel0[1]),
        .I3(BCR_reg[11]),
        .I4(sel0[0]),
        .I5(L1A_reg[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[12]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[12]_i_4_n_0 ),
        .O(reg_data_out__0[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(LUT_CONT_EN[12]),
        .I1(L1A_COUNT_out[12]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[12]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[12]_i_3 
       (.I0(TTC_CONFIG_out[12]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[12]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_4 
       (.I0(BCR_PERIOD_out[12]),
        .I1(ECR_reg[12]),
        .I2(sel0[1]),
        .I3(BCR_reg[12]),
        .I4(sel0[0]),
        .I5(L1A_reg[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[13]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[13]_i_4_n_0 ),
        .O(reg_data_out__0[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(LUT_CONT_EN[13]),
        .I1(L1A_COUNT_out[13]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[13]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[13]_i_3 
       (.I0(TTC_CONFIG_out[13]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[13]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_4 
       (.I0(BCR_PERIOD_out[13]),
        .I1(ECR_reg[13]),
        .I2(sel0[1]),
        .I3(BCR_reg[13]),
        .I4(sel0[0]),
        .I5(L1A_reg[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[14]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[14]_i_4_n_0 ),
        .O(reg_data_out__0[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(LUT_CONT_EN[14]),
        .I1(L1A_COUNT_out[14]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[14]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[14]_i_3 
       (.I0(TTC_CONFIG_out[14]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[14]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_4 
       (.I0(BCR_PERIOD_out[14]),
        .I1(ECR_reg[14]),
        .I2(sel0[1]),
        .I3(BCR_reg[14]),
        .I4(sel0[0]),
        .I5(L1A_reg[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[15]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[15]_i_4_n_0 ),
        .O(reg_data_out__0[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(LUT_CONT_EN[15]),
        .I1(L1A_COUNT_out[15]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[15]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[15]_i_3 
       (.I0(TTC_CONFIG_out[15]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[15]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_4 
       (.I0(BCR_PERIOD_out[15]),
        .I1(ECR_reg[15]),
        .I2(sel0[1]),
        .I3(BCR_reg[15]),
        .I4(sel0[0]),
        .I5(L1A_reg[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[16]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[16]_i_4_n_0 ),
        .O(reg_data_out__0[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(LUT_CONT_EN[16]),
        .I1(L1A_COUNT_out[16]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[16]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[16]_i_3 
       (.I0(TTC_CONFIG_out[16]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[16]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_4 
       (.I0(BCR_PERIOD_out[16]),
        .I1(ECR_reg[16]),
        .I2(sel0[1]),
        .I3(BCR_reg[16]),
        .I4(sel0[0]),
        .I5(L1A_reg[16]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[17]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[17]_i_4_n_0 ),
        .O(reg_data_out__0[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(LUT_CONT_EN[17]),
        .I1(L1A_COUNT_out[17]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[17]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[17]_i_3 
       (.I0(TTC_CONFIG_out[17]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[17]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_4 
       (.I0(BCR_PERIOD_out[17]),
        .I1(ECR_reg[17]),
        .I2(sel0[1]),
        .I3(BCR_reg[17]),
        .I4(sel0[0]),
        .I5(L1A_reg[17]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[18]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[18]_i_4_n_0 ),
        .O(reg_data_out__0[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(LUT_CONT_EN[18]),
        .I1(L1A_COUNT_out[18]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[18]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[18]_i_3 
       (.I0(TTC_CONFIG_out[18]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[18]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(BCR_PERIOD_out[18]),
        .I1(ECR_reg[18]),
        .I2(sel0[1]),
        .I3(BCR_reg[18]),
        .I4(sel0[0]),
        .I5(L1A_reg[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[19]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[19]_i_4_n_0 ),
        .O(reg_data_out__0[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(LUT_CONT_EN[19]),
        .I1(L1A_COUNT_out[19]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[19]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[19]_i_3 
       (.I0(TTC_CONFIG_out[19]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[19]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(BCR_PERIOD_out[19]),
        .I1(ECR_reg[19]),
        .I2(sel0[1]),
        .I3(BCR_reg[19]),
        .I4(sel0[0]),
        .I5(L1A_reg[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[1]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[1]_i_4_n_0 ),
        .O(reg_data_out__0[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(LUT_CONT_EN[1]),
        .I1(L1A_COUNT_out[1]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[1]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_3 
       (.I0(TTC_CONFIG_out[1]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[1]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(BCR_PERIOD_out[1]),
        .I1(ECR_reg[1]),
        .I2(sel0[1]),
        .I3(BCR_reg[1]),
        .I4(sel0[0]),
        .I5(L1A_reg[1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[20]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[20]_i_4_n_0 ),
        .O(reg_data_out__0[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(LUT_CONT_EN[20]),
        .I1(L1A_COUNT_out[20]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[20]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[20]_i_3 
       (.I0(TTC_CONFIG_out[20]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[20]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(BCR_PERIOD_out[20]),
        .I1(ECR_reg[20]),
        .I2(sel0[1]),
        .I3(BCR_reg[20]),
        .I4(sel0[0]),
        .I5(L1A_reg[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[21]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .O(reg_data_out__0[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(LUT_CONT_EN[21]),
        .I1(L1A_COUNT_out[21]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[21]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[21]_i_3 
       (.I0(TTC_CONFIG_out[21]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[21]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_4 
       (.I0(BCR_PERIOD_out[21]),
        .I1(ECR_reg[21]),
        .I2(sel0[1]),
        .I3(BCR_reg[21]),
        .I4(sel0[0]),
        .I5(L1A_reg[21]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[22]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[22]_i_4_n_0 ),
        .O(reg_data_out__0[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(LUT_CONT_EN[22]),
        .I1(L1A_COUNT_out[22]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[22]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[22]_i_3 
       (.I0(TTC_CONFIG_out[22]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[22]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(BCR_PERIOD_out[22]),
        .I1(ECR_reg[22]),
        .I2(sel0[1]),
        .I3(BCR_reg[22]),
        .I4(sel0[0]),
        .I5(L1A_reg[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[23]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[23]_i_4_n_0 ),
        .O(reg_data_out__0[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(LUT_CONT_EN[23]),
        .I1(L1A_COUNT_out[23]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[23]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[23]_i_3 
       (.I0(TTC_CONFIG_out[23]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[23]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(BCR_PERIOD_out[23]),
        .I1(ECR_reg[23]),
        .I2(sel0[1]),
        .I3(BCR_reg[23]),
        .I4(sel0[0]),
        .I5(L1A_reg[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[24]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[24]_i_4_n_0 ),
        .O(reg_data_out__0[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(LUT_CONT_EN[24]),
        .I1(L1A_COUNT_out[24]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[24]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[24]_i_3 
       (.I0(TTC_CONFIG_out[24]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[24]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_4 
       (.I0(BCR_PERIOD_out[24]),
        .I1(ECR_reg[24]),
        .I2(sel0[1]),
        .I3(BCR_reg[24]),
        .I4(sel0[0]),
        .I5(L1A_reg[24]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[25]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[25]_i_4_n_0 ),
        .O(reg_data_out__0[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(LUT_CONT_EN[25]),
        .I1(L1A_COUNT_out[25]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[25]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[25]_i_3 
       (.I0(TTC_CONFIG_out[25]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[25]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_4 
       (.I0(BCR_PERIOD_out[25]),
        .I1(ECR_reg[25]),
        .I2(sel0[1]),
        .I3(BCR_reg[25]),
        .I4(sel0[0]),
        .I5(L1A_reg[25]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[26]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[26]_i_4_n_0 ),
        .O(reg_data_out__0[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(LUT_CONT_EN[26]),
        .I1(L1A_COUNT_out[26]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[26]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[26]_i_3 
       (.I0(TTC_CONFIG_out[26]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[26]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_4 
       (.I0(BCR_PERIOD_out[26]),
        .I1(ECR_reg[26]),
        .I2(sel0[1]),
        .I3(BCR_reg[26]),
        .I4(sel0[0]),
        .I5(L1A_reg[26]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[27]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[27]_i_4_n_0 ),
        .O(reg_data_out__0[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(LUT_CONT_EN[27]),
        .I1(L1A_COUNT_out[27]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[27]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[27]_i_3 
       (.I0(TTC_CONFIG_out[27]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[27]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_4 
       (.I0(BCR_PERIOD_out[27]),
        .I1(ECR_reg[27]),
        .I2(sel0[1]),
        .I3(BCR_reg[27]),
        .I4(sel0[0]),
        .I5(L1A_reg[27]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[28]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[28]_i_4_n_0 ),
        .O(reg_data_out__0[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(LUT_CONT_EN[28]),
        .I1(L1A_COUNT_out[28]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[28]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[28]_i_3 
       (.I0(TTC_CONFIG_out[28]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[28]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_4 
       (.I0(BCR_PERIOD_out[28]),
        .I1(ECR_reg[28]),
        .I2(sel0[1]),
        .I3(BCR_reg[28]),
        .I4(sel0[0]),
        .I5(L1A_reg[28]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[29]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[29]_i_4_n_0 ),
        .O(reg_data_out__0[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(LUT_CONT_EN[29]),
        .I1(L1A_COUNT_out[29]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[29]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[29]_i_3 
       (.I0(TTC_CONFIG_out[29]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[29]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_4 
       (.I0(BCR_PERIOD_out[29]),
        .I1(ECR_reg[29]),
        .I2(sel0[1]),
        .I3(BCR_reg[29]),
        .I4(sel0[0]),
        .I5(L1A_reg[29]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[2]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[2]_i_4_n_0 ),
        .O(reg_data_out__0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(LUT_CONT_EN[2]),
        .I1(L1A_COUNT_out[2]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[2]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_3 
       (.I0(TTC_CONFIG_out[2]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[2]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(BCR_PERIOD_out[2]),
        .I1(ECR_reg[2]),
        .I2(sel0[1]),
        .I3(BCR_reg[2]),
        .I4(sel0[0]),
        .I5(L1A_reg[2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[30]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[30]_i_4_n_0 ),
        .O(reg_data_out__0[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(LUT_CONT_EN[30]),
        .I1(L1A_COUNT_out[30]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[30]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[30]_i_3 
       (.I0(TTC_CONFIG_out[30]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[30]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_4 
       (.I0(BCR_PERIOD_out[30]),
        .I1(ECR_reg[30]),
        .I2(sel0[1]),
        .I3(BCR_reg[30]),
        .I4(sel0[0]),
        .I5(L1A_reg[30]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[31]_i_4_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[31]_i_5_n_0 ),
        .O(reg_data_out__0[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_3 
       (.I0(LUT_CONT_EN[31]),
        .I1(L1A_COUNT_out[31]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[31]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[31]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[31]_i_4 
       (.I0(TTC_CONFIG_out[31]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[31]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[31]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_5 
       (.I0(BCR_PERIOD_out[31]),
        .I1(ECR_reg[31]),
        .I2(sel0[1]),
        .I3(BCR_reg[31]),
        .I4(sel0[0]),
        .I5(L1A_reg[31]),
        .O(\axi_rdata[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[3]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[3]_i_4_n_0 ),
        .O(reg_data_out__0[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(LUT_CONT_EN[3]),
        .I1(L1A_COUNT_out[3]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[3]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_3 
       (.I0(TTC_CONFIG_out[3]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[3]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_4 
       (.I0(BCR_PERIOD_out[3]),
        .I1(ECR_reg[3]),
        .I2(sel0[1]),
        .I3(BCR_reg[3]),
        .I4(sel0[0]),
        .I5(L1A_reg[3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[4]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[4]_i_4_n_0 ),
        .O(reg_data_out__0[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(LUT_CONT_EN[4]),
        .I1(L1A_COUNT_out[4]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[4]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_3 
       (.I0(TTC_CONFIG_out[4]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[4]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_4 
       (.I0(BCR_PERIOD_out[4]),
        .I1(ECR_reg[4]),
        .I2(sel0[1]),
        .I3(BCR_reg[4]),
        .I4(sel0[0]),
        .I5(L1A_reg[4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[5]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[5]_i_4_n_0 ),
        .O(reg_data_out__0[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(LUT_CONT_EN[5]),
        .I1(L1A_COUNT_out[5]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[5]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_3 
       (.I0(TTC_CONFIG_out[5]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[5]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_4 
       (.I0(BCR_PERIOD_out[5]),
        .I1(ECR_reg[5]),
        .I2(sel0[1]),
        .I3(BCR_reg[5]),
        .I4(sel0[0]),
        .I5(L1A_reg[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[6]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[6]_i_4_n_0 ),
        .O(reg_data_out__0[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(LUT_CONT_EN[6]),
        .I1(L1A_COUNT_out[6]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[6]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_3 
       (.I0(TTC_CONFIG_out[6]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[6]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_4 
       (.I0(BCR_PERIOD_out[6]),
        .I1(ECR_reg[6]),
        .I2(sel0[1]),
        .I3(BCR_reg[6]),
        .I4(sel0[0]),
        .I5(L1A_reg[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[7]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[7]_i_4_n_0 ),
        .O(reg_data_out__0[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(LUT_CONT_EN[7]),
        .I1(L1A_COUNT_out[7]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[7]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_3 
       (.I0(TTC_CONFIG_out[7]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[7]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_4 
       (.I0(BCR_PERIOD_out[7]),
        .I1(ECR_reg[7]),
        .I2(sel0[1]),
        .I3(BCR_reg[7]),
        .I4(sel0[0]),
        .I5(L1A_reg[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[8]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[8]_i_4_n_0 ),
        .O(reg_data_out__0[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(LUT_CONT_EN[8]),
        .I1(L1A_COUNT_out[8]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[8]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_3 
       (.I0(TTC_CONFIG_out[8]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[8]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_4 
       (.I0(BCR_PERIOD_out[8]),
        .I1(ECR_reg[8]),
        .I2(sel0[1]),
        .I3(BCR_reg[8]),
        .I4(sel0[0]),
        .I5(L1A_reg[8]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[9]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[9]_i_4_n_0 ),
        .O(reg_data_out__0[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(LUT_CONT_EN[9]),
        .I1(L1A_COUNT_out[9]),
        .I2(sel0[1]),
        .I3(OCR_COUNT[9]),
        .I4(sel0[0]),
        .I5(L1A_COUNT[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_3 
       (.I0(TTC_CONFIG_out[9]),
        .I1(sel0[1]),
        .I2(L1A_PERIOD_out[9]),
        .I3(sel0[0]),
        .I4(OCR_PERIOD_out[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_4 
       (.I0(BCR_PERIOD_out[9]),
        .I1(ECR_reg[9]),
        .I2(sel0[1]),
        .I3(BCR_reg[9]),
        .I4(sel0[0]),
        .I5(L1A_reg[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[0]),
        .Q(s00_axi_rdata[0]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[10]),
        .Q(s00_axi_rdata[10]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[11]),
        .Q(s00_axi_rdata[11]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[12]),
        .Q(s00_axi_rdata[12]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[13]),
        .Q(s00_axi_rdata[13]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[14]),
        .Q(s00_axi_rdata[14]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[15]),
        .Q(s00_axi_rdata[15]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[16]),
        .Q(s00_axi_rdata[16]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[17]),
        .Q(s00_axi_rdata[17]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[18]),
        .Q(s00_axi_rdata[18]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[19]),
        .Q(s00_axi_rdata[19]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[1]),
        .Q(s00_axi_rdata[1]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[20]),
        .Q(s00_axi_rdata[20]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[21]),
        .Q(s00_axi_rdata[21]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[22]),
        .Q(s00_axi_rdata[22]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[23]),
        .Q(s00_axi_rdata[23]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[24]),
        .Q(s00_axi_rdata[24]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[25]),
        .Q(s00_axi_rdata[25]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[26]),
        .Q(s00_axi_rdata[26]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[27]),
        .Q(s00_axi_rdata[27]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[28]),
        .Q(s00_axi_rdata[28]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[29]),
        .Q(s00_axi_rdata[29]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[2]),
        .Q(s00_axi_rdata[2]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[30]),
        .Q(s00_axi_rdata[30]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[31]),
        .Q(s00_axi_rdata[31]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[3]),
        .Q(s00_axi_rdata[3]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[4]),
        .Q(s00_axi_rdata[4]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[5]),
        .Q(s00_axi_rdata[5]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[6]),
        .Q(s00_axi_rdata[6]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[7]),
        .Q(s00_axi_rdata[7]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[8]),
        .Q(s00_axi_rdata[8]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[9]),
        .Q(s00_axi_rdata[9]),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(\L1A_reg[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(\L1A_reg[0]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
