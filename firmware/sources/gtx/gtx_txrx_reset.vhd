-- EASE/HDL begin --------------------------------------------------------------
-- 
-- Architecture 'rtl' of entity 'gtx_txrx_reset'.
-- 
--------------------------------------------------------------------------------
-- 
-- Copy of the interface declaration:
-- 
library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
use ieee.std_logic_1164.all;

entity  gtx_txrx_reset is
   port (
     SOFT_RESET_RX_IN : out    std_logic;
     SOFT_RESET_TX_IN : out    std_logic;
     resetn_in         : in    std_logic;
     reset_out        : out    std_logic;
     wr_en_pb         : in     std_logic;
     clk              : in     std_logic);
end entity; 
-- EASE/HDL end ----------------------------------------------------------------

architecture rtl of gtx_txrx_reset is

signal   count_reset : std_logic_vector(8 downto 0) := (others => '0');
constant   max_count_reset : std_logic_vector(8 downto 0) := (others => '1');
signal wr_en_pb_t : std_logic := '0';
signal soft_reset : std_logic := '1';

begin

process(clk, wr_en_pb)
begin
 if(rising_edge(clk)) then
--    	wr_en_pb_t <= wr_en_pb;
		if(soft_reset = '1') then
--		if(wr_en_pb_t = '0' and wr_en_pb = '1') then
			count_reset <= (others => '0');
			reset_out <= '1';
			soft_reset <= '0'; 
		else              
			if(count_reset /= max_count_reset) then
				count_reset <= count_reset + 1;
			    reset_out <= '1'; 
			else
			    reset_out <= '0'; 
               count_reset <= count_reset;
			end if;   
		end if;
	end if;
end process; 

--reset_out <= '0'; 
SOFT_RESET_RX_IN <= '0'; --not resetn_in;
SOFT_RESET_TX_IN <= '0'; --not resetn_in;

end architecture rtl ; -- of gtx_txrx_reset

