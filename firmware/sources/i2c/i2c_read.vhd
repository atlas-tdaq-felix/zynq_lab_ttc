library ieee;
use ieee.std_logic_1164.all;

					
entity i2c_read is
	port (
		clk: in std_logic;
		reset : in std_logic;
		clk_pls_n: in std_logic;
		clk_pls_p: in std_logic;
		slow_clk : in std_logic;
		address : in std_logic_vector(6 downto 0);
		
		smb_data_in : in std_logic;
		smb_clk_out : out std_logic;
		smb_data_out : out std_logic;
		data_cont : out std_logic;
		
		data_out : out std_logic_vector(7 downto 0);
		data_vd : out std_logic;
		read_done : out std_logic
	);
end i2c_read;

architecture rtl of i2c_read is	
	signal counter : integer range 0 to 127;
--	signal data_cont_i, data_cont_d : std_logic;
	signal data_cont_i : std_logic;
	signal clk_cont : std_logic;
--	signal smb_data_int, smb_data_intd : std_logic;
	signal smb_data_int : std_logic;
	
	
begin
	
	smb_clk_out <= (not clk_cont) or slow_clk;
--	smb_data_out <= smb_data_intd;
--	data_cont <= data_cont_d;
	

	-- changing data timing to falling edge of smb_clk
	-- both data and data_controle needs to be sync with falling edge
	process(clk, reset)begin
		if(reset = '0')then
--			data_cont_d <= '0'; -- do not output any data and tri-sated
			data_cont <= '0'; -- changed
--			smb_data_intd <= '1'; -- no data period has to be Hi or tri-stated
			smb_data_out <= '1'; -- changed
		elsif(clk'event and clk = '1')then
			if(clk_pls_n = '1')then -- change to the falling edge of smb_clk
--				data_cont_d <= data_cont_i; -- data needs to be changed on falling edge of smb_clk
				data_cont <= data_cont_i; -- changed
--				smb_data_intd <= smb_data_int;
				smb_data_out <= smb_data_int; -- changed
			end if;
		end if;
	end process;


	
	process(clk, reset)begin
		if(reset = '0')then
			counter <= 0;
			data_cont_i <= '0';
			clk_cont <= '0';
			read_done <= '0';
			data_vd <= '0';
		elsif(clk'event and clk = '1')then
			if(clk_pls_p = '1')then -- this process is working in raising edge of smb_clk 
				case counter is
				when 0 =>
					smb_data_int <= '1';
					counter <= 1;
					data_cont_i <= '0';
					clk_cont <= '0';
					read_done <= '0';
					data_vd <= '1';
				when 1 =>
					data_cont_i <= '1';
					smb_data_int <= '0';
					counter <= counter + 1;
				when 2 =>
					clk_cont <= '1';
					smb_data_int <= address(6);
					counter <= counter + 1;
				when 3 =>
					smb_data_int <= address(5);
					counter <= counter + 1;
				when 4 =>
					smb_data_int <= address(4);
					counter <= counter + 1;
				when 5 =>
					smb_data_int <= address(3);
					counter <= counter + 1;
				when 6 =>
					smb_data_int <= address(2);
					counter <= counter + 1;
				when 7 =>
					smb_data_int <= address(1);
					counter <= counter + 1;
				when 8 =>
					smb_data_int <= address(0);
					counter <= counter + 1;
				when 9 =>
					smb_data_int <= '1'; -- read sign is 1
					counter <= counter + 1;
				when 10 =>					
					counter <= counter + 1;
					data_cont_i <= '0';
				when 11 =>					
					if(smb_data_in = '0')then
						counter <= counter + 1;
						data_vd <= '0';
					else
						counter <= counter;
					end if;
				when 12 =>
					data_out(7) <= smb_data_in;
					counter <= counter + 1;
				when 13 =>
					data_out(6) <= smb_data_in;
					counter <= counter + 1;
				when 14 =>
					data_out(5) <= smb_data_in;
					counter <= counter + 1;
				when 15 =>
					data_out(4) <= smb_data_in;
					counter <= counter + 1;
				when 16 =>
					data_out(3) <= smb_data_in;
					counter <= counter + 1;
				when 17 =>
					data_out(2) <= smb_data_in;
					counter <= counter + 1;
				when 18 =>
					data_out(1) <= smb_data_in;
					counter <= counter + 1;
				when 19 =>
					data_out(0) <= smb_data_in;
					counter <= counter + 1;
					smb_data_int <= '1'; -- stop bit
					clk_cont <= '0';
					data_cont_i <= '1';
				when 20 =>
					data_vd <= '1';
					data_cont_i <= '0';
					counter <= counter + 1;
				when 127 =>
					read_done <= '1';				
					counter <= counter;
				when others =>
					counter <= counter + 1;
				end case;
			end if;
		end if;
	end process;
end rtl;

