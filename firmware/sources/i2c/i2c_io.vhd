library ieee;
use ieee.std_logic_1164.all;

entity i2c_io is
	port (
		reset_n: in std_logic;
		clk : in std_logic;

		smb_clk: out std_logic;
		smb_data: inout std_logic;
		
		address : in std_logic_vector(6 downto 0); -- 7bits device address
		reg_add : in std_logic_vector(7 downto 0);
		write_read : in std_logic; -- 0: read   1:write
		process_done : out std_logic; -- 0: under process   1:process done
		
		data_in : in  std_logic_vector(7 downto 0);
		data_out : out std_logic_vector(7 downto 0)
	);
end i2c_io;


architecture rtl of i2c_io is

	component gen100k_or_less_pls
		port (
			clk: in std_logic;
			reset : in std_logic;
			clk_pls_n: out std_logic;
			clk_pls_p: out std_logic;
			slow_clk : out std_logic
		);
	end component;

	component i2c_cont_si570
		port (
			clk: in std_logic; -- system 50MHz clk
			reset : in std_logic; -- system reset/power on reset
			clk_pls_n: in std_logic; -- negative slow clk pulse, less than 100kHz, this project is 50kHz, 1 cycle width of system clk
			clk_pls_p: in std_logic; -- positive slow clk pulse, less than 100kHz, this project is 50kHz, 1 cycle width of system clk
			slow_clk : in std_logic; -- 50kHz clk to output as smb_clk
			
			smb_clk : out std_logic; -- I2C bus control clk
			smb_data : inout std_logic; -- bidirectional data signal
	
			address : in std_logic_vector(6 downto 0); -- 7bits device address
			reg_add : in std_logic_vector(7 downto 0);
			write_read : in std_logic; -- 0: read   1:write
			process_done : out std_logic; -- 0: under process   1:process done
			
			data_in : in  std_logic_vector(7 downto 0);
			data_out : out std_logic_vector(7 downto 0)
		);
	end component;	

	signal clk_pls_n, clk_pls_p, slow_clk : std_logic;
	
begin

	u1: gen100k_or_less_pls port map(clk, reset_n, clk_pls_n, clk_pls_p, slow_clk);
	
	u2: i2c_cont_si570 port map(clk, reset_n, clk_pls_n, clk_pls_p, slow_clk, 
								smb_clk, smb_data,
								address, reg_add, write_read, process_done,
								data_in, data_out
								);

end rtl;