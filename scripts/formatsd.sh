#!/bin/bash
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit
fi
if [ $# -eq 0 ]
  	then
    	echo "Usage: formatsd <device>"
	echo "e.g.:"
	echo "  formatsd /dev/sdd"
	echo "Make sure the SD card is not mounted, any existing partitions will be removed"
	exit
fi

echo "Creating bootable SD card"
dd if=/dev/zero of=$1 bs=1024 count=1
fdisk $1 <mksd.cmds
mkfs.vfat -F 32 -s 2 -n BOOT ${1}1
mkfs.ext4 -L rootfs ${1}2
