onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib TTC_PICOZED_BD_opt

do {wave.do}

view wave
view structure
view signals

do {TTC_PICOZED_BD.udo}

run -all

quit -force
