onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ila_test_rd_8t49n242_opt

do {wave.do}

view wave
view structure
view signals

do {ila_test_rd_8t49n242.udo}

run -all

quit -force
