library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use xil_defaultlib.TTC709_pkg.all;

entity clk_8t49n242_i2c is
  port (
    clk             		: in     std_logic;
    iic_mux_reset_n 		: out    std_logic;
    clk_8t49n242_control    : in     clk_8t49n242_reg_type;
    si570_reset     		: out    std_logic;
    rd_en_pb	 			: in 	  std_logic;	
    wr_en_pb     			: in       std_logic;    
	mstr_ctri_io 			: out       std_logic;
	mstr_cin_io 			: out       std_logic;
	mstr_cout_io 			: in       std_logic;
	mstr_dtri_io 			: out       std_logic;
	mstr_din_io 			: out       std_logic;
	mstr_dout_io 			: in       std_logic;
    clk_8t49n242_status    	: out    clk_8t49n242_reg_type);
end entity clk_8t49n242_i2c;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of clk_8t49n242_i2c is

  signal start      : std_logic;
  signal stop       : std_logic;
  signal read       : std_logic;
  signal write      : std_logic;
  signal send_ack   : std_logic;
  signal mstr_din   : std_logic_vector(7 downto 0);
  signal ready      : std_logic;
  signal mstr_dout  : std_logic_vector(7 downto 0);
  signal free       : std_logic;
  signal rec_ack    : std_logic;
  signal core_state : std_logic_vector(5 downto 0);
  signal iic_reset  : std_logic;
  signal write_state: std_logic_vector(5 downto 0);

  component i2c_master_rd_wr_ver0
    generic(
      CLK_FREQ : natural := 25000000;
      BAUD     : natural := 100000);
    port (
      sys_clk    		: in     std_logic;
      sys_rst    		: in     std_logic;
      start      		: in     std_logic;
      stop       		: in     std_logic;
      read       		: in     std_logic;
      write      		: in     std_logic;
      send_ack   		: in     std_logic;
      mstr_din   		: in     std_logic_vector(7 downto 0);
      free       		: out    std_logic;
      rec_ack    		: out    std_logic;
      ready      		: out    std_logic;
      core_state 		: out    std_logic_vector(5 downto 0);
      write_state 		: in     std_logic_vector(5 downto 0);
      mstr_ctri_io      : out    std_logic;
      mstr_cin_io      	: out    std_logic;
      mstr_cout_io      : in    std_logic;
      mstr_dtri_io      : out    std_logic;
      mstr_din_io      	: out    std_logic;
      mstr_dout_io      : in    std_logic;
      mstr_dout  		: out    std_logic_vector(7 downto 0));
  end component i2c_master_rd_wr_ver0;

  component i2c_8t49n242_rd_wr
    port (
    clk          : in     std_logic;
    core_state   : in     std_logic_vector(5 downto 0);
    write_state	 : out     std_logic_vector(5 downto 0);
    free         : in     std_logic;
    iic_reset    : out    std_logic;
    mstr_din     : out    std_logic_vector(7 downto 0);
    mstr_dout    : in     std_logic_vector(7 downto 0);
    read         : out    std_logic;
    ready        : in     std_logic;
    rec_ack      : in     std_logic;
    send_ack     : out    std_logic;
    clk_8t49n242_control   : in     clk_8t49n242_reg_type;
    si570_reset  : out    std_logic;
    clk_8t49n242_status : out    clk_8t49n242_reg_type;
    start        : out    std_logic;
    stop         : out    std_logic;
    rd_en_pb     : in     std_logic;
    wr_en_pb     : in     std_logic;
    write        : out    std_logic);
  end component i2c_8t49n242_rd_wr;

--  component ila_i2c
--    port (
--      clk    	 : in     std_logic;
--      probe0    : in     std_logic_vector(0 downto 0);
--      probe1    : in     std_logic_vector(0 downto 0);
--      probe2    : in     std_logic_vector(0 downto 0));
--  end component ila_i2c;

--  component si570_master
--    port (
--      clk          : in     std_logic;
--      core_state   : in     std_logic_vector(5 downto 0);
--      free         : in     std_logic;
--      iic_reset    : out    std_logic;
--      mstr_din     : out    std_logic_vector(7 downto 0);
--      mstr_dout    : in     std_logic_vector(7 downto 0);
--      read         : out    std_logic;
--      ready        : in     std_logic;
--      rec_ack      : in     std_logic;
--      reset_in     : in     std_logic;
--      send_ack     : out    std_logic;
--      si570_ctrl   : in     si570_ctrl_record;
--      si570_reset  : out    std_logic;
--      si570_status : out    si570_status_record;
--      start        : out    std_logic;
--      stop         : out    std_logic;
--      write        : out    std_logic);
--  end component si570_master;

--  component INV_wrapper
--    port (
--      O : out    std_ulogic;
--      I : in     std_ulogic);
--  end component INV_wrapper;

begin

  master_rd_wr: i2c_master_rd_wr_ver0
    generic map(
      CLK_FREQ => 50000000,
      BAUD     => 400000)
    port map(
      sys_clk    => clk,
      sys_rst    => iic_reset,
      start      => start,
      stop       => stop,
      read       => read,
      write      => write,
      send_ack   => send_ack,
      mstr_din   => mstr_din,
      free       => free,
      rec_ack    => rec_ack,
      ready      => ready,
      core_state => core_state,
      write_state => write_state,
      mstr_ctri_io => mstr_ctri_io,
      mstr_cin_io => mstr_cin_io,
      mstr_cout_io => mstr_cout_io,
      mstr_dtri_io => mstr_dtri_io,
      mstr_din_io => mstr_din_io,
      mstr_dout_io => mstr_dout_io,
      mstr_dout  => mstr_dout);



  master_8t49n242_wr_rd: i2c_8t49n242_rd_wr
    port map(
      clk          => clk,
      core_state   => core_state,
      write_state => write_state,
      free         => free,
      iic_reset    => iic_reset,
      mstr_din     => mstr_din,
      mstr_dout    => mstr_dout,
      read         => read,
      ready        => ready,
      rec_ack      => rec_ack,
      send_ack     => send_ack,
      clk_8t49n242_control   => clk_8t49n242_control,
      si570_reset  => si570_reset,
      clk_8t49n242_status => clk_8t49n242_status,
      start        => start,
      stop         => stop,
      rd_en_pb     => rd_en_pb,
      wr_en_pb     => wr_en_pb,
      write        => write);

--ila_i2c_inst: ila_i2c
--  PORT map(
--      clk => clk,
--      probe0(0) => si570_iic_data,
--      probe1(0) => si570_iic_clk,
--      probe2(0) => read
--  );

iic_mux_reset_n <= not iic_reset;
--  u2: INV_wrapper
--    port map(
--      O => iic_mux_reset_n,
--      I => iic_reset);
end architecture structure ; -- of 8t49n242_i2c
