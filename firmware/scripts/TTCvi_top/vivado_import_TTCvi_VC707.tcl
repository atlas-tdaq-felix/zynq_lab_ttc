#
#  File import script for the FMEmu hdl Vivado project
#  Board: BNL712
#

source ../helper/clear_filesets.tcl

set PROJECT_NAME TTCvi_VC707
set BOARD_TYPE VC707
set TOPLEVEL TTCvi_top_VC707

#Import blocks for different filesets
source ../filesets/fmemu_top_fileset.tcl
source ../filesets/fmemu_fileset.tcl
source ../filesets/centralRouter_fileset.tcl
source ../filesets/wupper_fileset.tcl
source ../filesets/gbt_core_fileset.tcl
source ../filesets/housekeeping_fileset.tcl
source ../filesets/gbt_fanout_fileset.tcl
source ../filesets/gbt_emulator_fileset.tcl
source ../filesets/ttc_decoder_fileset.tcl
source ../filesets/ttc_emulator_fileset.tcl


#Actually execute all the filesets
source ../helper/vivado_import_generic.tcl

#upgrade_ip  [get_ips] -log ip_upgrade.log
#
#close [ open $firmware_dir/constraints/felix_probes.xdc w ]
#read_xdc -verbose $firmware_dir/constraints/felix_probes.xdc
#set_property target_constrs_file $firmware_dir/constraints/felix_probes.xdc [current_fileset -constrset]
#
#set_property top FMEmu_top_bnl711 [current_fileset]
#set_property top FMEmu_top_bnl711_tb [get_filesets sim_1]

puts "INFO: Done!"
