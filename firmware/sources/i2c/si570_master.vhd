--------------------------------------------------------------------------------
-- Object        : Entity work.si570_master
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------



library ieee, work;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.TTCvi_pkg.all;

entity si570_master is
  port (
    clk          : in     std_logic;
    core_state   : in     std_logic_vector(5 downto 0);
    free         : in     std_logic;
    iic_reset    : out    std_logic;
    mstr_din     : out    std_logic_vector(7 downto 0);
    mstr_dout    : in     std_logic_vector(7 downto 0);
    read         : out    std_logic;
    ready        : in     std_logic;
    rec_ack      : in     std_logic;
    reset_in     : in     std_logic;
    send_ack     : out    std_logic;
    si570_ctrl   : in     si570_ctrl_record;
    si570_reset  : out    std_logic;
    si570_status : out    si570_status_record;
    start        : out    std_logic;
    stop         : out    std_logic;
    write        : out    std_logic);
end entity si570_master;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_master.rtl
-- Version       : 966 (Subversion)
-- Last modified : Tue Jul 01 12:42:48 2014.
--------------------------------------------------------------------------------


architecture rtl of si570_master is
	signal reset : std_logic;
	signal sw_reset : std_logic;
--	constant dev_address : std_logic_vector(6 downto 0):= "1110000"; --virtex eval kit
	constant dev_address : std_logic_vector(6 downto 0):= "1011101"; --kintex eval kit
	constant reg7_cnst : std_logic_vector(7 downto 0):= X"07";
	constant reg8_cnst : std_logic_vector(7 downto 0):= X"08";
	constant reg9_cnst : std_logic_vector(7 downto 0):= X"09";
	constant reg10_cnst : std_logic_vector(7 downto 0):= X"0A";
	constant reg11_cnst : std_logic_vector(7 downto 0):= X"0B";
	constant reg12_cnst : std_logic_vector(7 downto 0):= X"0C";
	constant reg135_cnst : std_logic_vector(7 downto 0):= X"87";
	constant reg137_cnst : std_logic_vector(7 downto 0):= X"89";

	signal counter : integer range 0 to 127 := 100;
	signal time50ms : std_logic;
	signal wait50start : std_logic;
	
	signal data_out7 : std_logic_vector(7 downto 0);
	signal data_out8 : std_logic_vector(7 downto 0);
	signal data_out9 : std_logic_vector(7 downto 0);
	signal data_out10 : std_logic_vector(7 downto 0);
	signal data_out11 : std_logic_vector(7 downto 0);
	signal data_out12 : std_logic_vector(7 downto 0);
	signal data_out135 : std_logic_vector(7 downto 0);
	signal data_out137 : std_logic_vector(7 downto 0);

	signal data_in7 : std_logic_vector(7 downto 0);
	signal data_in8 : std_logic_vector(7 downto 0);
	signal data_in9 : std_logic_vector(7 downto 0);
	signal data_in10 : std_logic_vector(7 downto 0);
	signal data_in11 : std_logic_vector(7 downto 0);
	signal data_in12 : std_logic_vector(7 downto 0);
	signal data_in135 : std_logic_vector(7 downto 0);
	signal data_in137 : std_logic_vector(7 downto 0);

	signal ila_data_in7 : std_logic_vector(7 downto 0);
	signal ila_data_in8 : std_logic_vector(7 downto 0);
	signal ila_data_in9 : std_logic_vector(7 downto 0);
	signal ila_data_in10 : std_logic_vector(7 downto 0);
	signal ila_data_in11 : std_logic_vector(7 downto 0);
	signal ila_data_in12 : std_logic_vector(7 downto 0);
	signal ila_data_in135 : std_logic_vector(7 downto 0);
	signal ila_data_in137 : std_logic_vector(7 downto 0);
	
	signal DCO_Freeze : std_logic_vector(7 downto 0);
	signal DCO_UnFreeze : std_logic_vector(7 downto 0);
	signal si570_NewFreq : std_logic_vector(7 downto 0);

	signal write_triger, reset_triger : std_logic;
	signal reg7_out, reg8_out, reg9_out, reg10_out, reg11_out, reg12_out : std_logic_vector(7 downto 0);
	
	
           
	signal reset_init_cnt : integer range 0 to 1000 := 0; 
	signal reset_init: std_logic:= '1';

	signal read_reg_state : integer range 0 to 100 := 0; 
	signal read_en : std_logic;
	signal read_en_t : std_logic;
	signal read_mstr_din: std_logic_vector(7 downto 0);
	signal read_start : std_logic;
	signal read_stop : std_logic;
	signal read_send_ack : std_logic;
	signal read_write : std_logic;
	signal read_read : std_logic;
	signal read_busy : std_logic;
	signal New_freq_en : std_logic;
	signal read_powerup_run : std_logic:= '1';
			           

	signal write_reg_state : integer range 0 to 100 := 0; 
	signal write_en : std_logic;
	signal write_en_t : std_logic;
	signal write_mstr_dout: std_logic_vector(7 downto 0);
	signal write_start : std_logic;
	signal write_stop : std_logic;
--	signal write_send_ack : std_logic;
	signal write_write : std_logic;
	signal write_read : std_logic;
	signal write_busy : std_logic;
	signal write_powerup_run : std_logic:= '0';


	signal mux_state : integer range 0 to 100 := 0; 
	signal mux_en : std_logic;
	signal mux_en_t : std_logic;
	signal mux_mstr_din: std_logic_vector(7 downto 0);
	signal mux_start : std_logic;
	signal mux_stop : std_logic;
	signal mux_send_ack : std_logic;
	signal mux_write : std_logic;
	signal mux_read : std_logic;
	signal mux_busy : std_logic;
	signal mux_powerup_run : std_logic:= '0';

	signal debug_read: std_logic_vector(7 downto 0);
	signal debug_write: std_logic_vector(7 downto 0);
	signal debug_mux: std_logic_vector(7 downto 0);
	signal debug_iic: std_logic_vector(7 downto 0);
	signal PROBE_OUT0: std_logic_vector(0 downto 0);
	signal PROBE_OUT1: std_logic_vector(0 downto 0);
	signal PROBE_OUT2: std_logic_vector(0 downto 0);
	signal rd_en_probe: std_logic_vector(0 downto 0);
	signal wr_en_probe: std_logic_vector(0 downto 0);
	signal mx_en_probe: std_logic_vector(0 downto 0);
	
component ila_reg
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
     probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe8 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe9 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe10 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe11 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe12 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe13 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe14 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe15 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
 end component;

component ila_debug_i2c
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
     probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
 end component;

component vio_0
   PORT (
     clk :    IN STD_LOGIC;
     PROBE_OUT0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0); 
     PROBE_OUT1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0); 
     PROBE_OUT2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0) 
   );
 end component;
	       
begin

	reset <= reset_in or sw_reset or reset_init; 

	si570_status.si570_reg7_out <= data_out7;
	si570_status.si570_reg8_out <= data_out8;
	si570_status.si570_reg9_out <= data_out9;
	si570_status.si570_reg10_out <= data_out10;
	si570_status.si570_reg11_out <= data_out11;
	si570_status.si570_reg12_out <= data_out12;
	si570_status.si570_reg135_out <= data_out135;
	si570_status.si570_reg137_out <= data_out137;

	data_in7 <= si570_ctrl.si570_reg7;
	data_in8 <= si570_ctrl.si570_reg8;
	data_in9 <= si570_ctrl.si570_reg9;
	data_in10 <= si570_ctrl.si570_reg10;
	data_in11 <= si570_ctrl.si570_reg11;
	data_in12 <= si570_ctrl.si570_reg12;
	data_in135 <= si570_ctrl.si570_reg135;
	data_in137 <= si570_ctrl.si570_reg137;

process(clk)
begin
 if rising_edge(clk) then	
	ila_data_in7 <=   data_in7;
	ila_data_in8 <=   data_in8;
	ila_data_in9 <=   data_in9;
	ila_data_in10 <=  data_in10;
	ila_data_in11 <=  data_in11;
	ila_data_in12 <=  data_in12;
	ila_data_in135 <= data_in135; 
	ila_data_in137 <= data_in137; 
end if;
end process;	
process(clk)
begin
 if rising_edge(clk) then	
	rd_en_probe <=   PROBE_OUT0;
	wr_en_probe <=   PROBE_OUT1;
	mx_en_probe <=   PROBE_OUT2;
end if;
end process;	

vio_inst: vio_0
  PORT map(
      clk => clk,
      PROBE_OUT0 => PROBE_OUT0,
      PROBE_OUT1 => PROBE_OUT1,
      PROBE_OUT2 => PROBE_OUT2
  );

ila_inst: ila_reg
  PORT map(
      clk => clk,
      probe0 => data_out7,
      probe1 => data_out8,
      probe2 => data_out9,
      probe3 => data_out10,
      probe4 => data_out11,
      probe5 => data_out12,
      probe6 => data_out135,
      probe7 => data_out137,
      probe8 =>  ila_data_in7,
      probe9 =>  ila_data_in8,
      probe10 => ila_data_in9,
      probe11 => ila_data_in10,
      probe12 => ila_data_in11,
      probe13 => ila_data_in12,
      probe14 => ila_data_in135, 
      probe15 => ila_data_in137
  );

ila_i2c_inst: ila_debug_i2c
  PORT map(
      clk => clk,
      probe0 => debug_read,
      probe1 => debug_write,
      probe2 => debug_mux,
      probe3 => debug_iic,
      probe4 => mstr_dout
  );
	

	DCO_Freeze <= data_out137 OR 	"00010000";
	DCO_UnFreeze <= data_out137 AND "11101111";
	si570_NewFreq <= data_out135 OR "01000000";

	read_en <= si570_ctrl.si570_read_en or rd_en_probe(0);
	write_en <= si570_ctrl.si570_write_en or wr_en_probe(0);
	mux_en <= si570_ctrl.si570_mux_en or mx_en_probe(0);
	sw_reset <= si570_ctrl.si570_iic_reset;
    iic_reset <= si570_ctrl.si570_iic_reset;
    New_freq_en <= si570_ctrl.si570_New_freq;
    
	si570_status.si570_debug_read <= debug_read;
	si570_status.si570_debug_write <= debug_write;
	si570_status.si570_debug_mux <= debug_mux;
	si570_status.si570_debug_iic <= debug_iic;
	       

	mstr_din 	<= read_mstr_din 	OR	mux_mstr_din 	OR 	write_mstr_dout;
	start 		<= read_start 		OR 	mux_start 		OR 	write_start;
	stop 		<= read_stop 		OR	mux_stop 		OR 	write_stop;
	send_ack 	<= read_send_ack 	OR	mux_send_ack;
	write 		<= read_write 		OR	mux_write 		OR 	write_write;
	read 		<= read_read 		OR	mux_read 		OR 	write_read;
	
	iic_status:process(clk) begin
		if(clk'event and clk = '1')then 
  			debug_read 	<=  CONV_STD_LOGIC_VECTOR(read_reg_state,8); 
  			debug_write <=  CONV_STD_LOGIC_VECTOR(write_reg_state,8); 
  			debug_mux 	<=  CONV_STD_LOGIC_VECTOR(mux_state,8); 
			debug_iic 	<=  "00"& core_state;
		end if;
	end process;

	rest_init_proc:process(clk) 
	begin
		if(clk'event and clk = '1')then 
	      if(reset_init_cnt /= 1000) then    
	        reset_init <= '1';
	        reset_init_cnt <= reset_init_cnt + 1;
	      else  	
	        reset_init <= '0';
	      end if;  
		end if;
	end process;

--**********************mux******
	mux:process(clk, reset) begin
		if(reset = '1')then
			mux_state <= 0;
           	mux_mstr_din <= (others => '0');
			mux_start <= '0';
			mux_stop <= '0';
		 	mux_send_ack <= '0';
			mux_write <= '0';
			mux_read <= '0';
			mux_busy <= '0';
			mux_en_t <= '0';
			mux_powerup_run <= '0';
		elsif(clk'event and clk = '1')then 
			mux_en_t <= mux_en;
			case mux_state is			
			 when 0 =>
				mux_busy <= '0';
				mux_start <= '0';
				mux_stop <= '0';
		 		mux_send_ack <= '0';
				mux_write <= '0';
				mux_read <= '0';
           		mux_mstr_din <= (others => '0');
			 	if(mux_en = '1' and mux_en_t = '0') or (mux_powerup_run = '0') then
			 	  mux_powerup_run <= '1';
			 	  mux_state <= mux_state + 1;
			 	end if;
			 when 1 =>
				if(Free = '1')then -- iic free to accept command
					mux_busy <= '1';
			 	  	mux_state <= mux_state + 1;
				end if;
			 when 2 =>
				mux_start <= '1';
		 	  	mux_state <= mux_state + 1;
			 when 3 =>
				if(ready = '0')then -- start iic
					mux_start <= '0';
			 	  	mux_state <= mux_state + 1;
				end if;
			 when 4 =>
				if(ready = '1')then 
			 	  	mux_state <= mux_state + 1;
				end if;
			 when 5 =>           
			 	mux_mstr_din <= "11101000";     -- write to  select PCA9548A ch0;
			    mux_write <= '1';
		 	  	mux_state <= mux_state + 1;
			 when 6 =>           
				if(Ready = '0')then 
			 	  	mux_state <= mux_state + 1;
			        mux_write <= '0';
				end if;
			 when 7 =>           
				if(Ready = '1')then 
			 	  	mux_state <= mux_state + 1;
				end if;
			 when 8 =>           
				if(Rec_ack = '1')then 
			 	  	mux_state <= mux_state + 1;
			        mux_write <= '1';
			 		mux_mstr_din <= x"01";
				end if;
			 when 9 =>           
				if(Ready = '0')then 
			 	  	mux_state <= mux_state + 1;
			        mux_write <= '0';
				end if;
			 when 10 =>           
				if(Ready = '1')then 
			 	  	mux_state <= mux_state + 1;
				end if;
			 when 11 =>           
				if(Rec_ack = '1')then -- Mux is set 
			 	  	mux_state <= mux_state + 1;
			        mux_stop <= '1';
				end if;
			 when 12 =>
				if(Ready = '0')then 
			 	  	mux_state <= mux_state + 1;
			        mux_stop <= '0';
				end if;
			 when 13 =>           
				if(Free = '1') then 
			 	  	mux_state <= 0;
				end if;
			when others =>  null;
          end case;
		end if;
	end process;
--*******************************
	read_proc:process(clk, reset) begin
		if(reset = '1')then
			--counter <= 100;
			read_reg_state <= 0;
           	read_mstr_din <= (others => '0');    
			read_start <= '0';
			read_stop <= '0';
		 	read_send_ack <= '0';
			read_write <= '0';
			read_read <= '0';  
			read_busy <= '0';
			read_en_t <= '0';
			data_out7 <= x"01";
			data_out8 <= x"C2";
			data_out9 <= x"BC";
			data_out10 <= x"01";
			data_out11 <= x"78";
			data_out12 <= x"28";
			data_out135 <= x"00";
			data_out137 <= x"08";
	 	    read_powerup_run <= '0';
		elsif(clk'event and clk = '1')then 
			read_en_t <= read_en;
			case read_reg_state is			
			 when 0 =>
				read_busy <= '0';
           		read_mstr_din <= (others => '0');     
				read_start <= '0';
				read_stop <= '0';
		 		read_send_ack <= '0';
				read_write <= '0';
				read_read <= '0';  
			 	if(read_en = '1' and read_en_t = '0') or (read_powerup_run = '0') then
			 	  read_powerup_run <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 1 =>
			    read_busy <= '1';
				if(free = '1')then -- iic is free
			 	  read_reg_state <= read_reg_state + 1;
				end if;
			 when 2 => 
				if(mux_busy = '0') then -- start iic
					read_start <= '1';
		 	  		read_reg_state <= read_reg_state + 1; 
		 	  	else
		 	  		read_reg_state <= 1;
		 	  	end if;	
			 when 3 => 
			 	if(Ready ='0' ) then
				  read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 4 => 
			 	if(Ready ='1' ) then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 5 => -- iic slave addr to write
           		read_mstr_din <= dev_address & '0';     
				read_write <= '1';
		 	  	read_reg_state <= read_reg_state + 1;
			 when 6 =>
			 	if(Ready ='0') then
					read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 7 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 8 => -- iic slave reg addr.
			 	if(Rec_ack ='1') then
				  	read_write <= '1';
           		  	read_mstr_din <= reg7_cnst; 		   
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 9 => 
			 	if(Ready ='0') then
					read_write <= '0';
			 	   	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 10 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 11 => 
			 	if(Rec_ack ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 12 =>
			 	   	read_reg_state <= read_reg_state + 1;
			 when 13 =>
			 	  read_reg_state <= read_reg_state + 1;
			 when 14 => -- repeted iic start
				read_start <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 15 =>
			 	if(Ready ='0') then
					read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 16 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 17 => -- iic slave addr to read
           		read_mstr_din <= dev_address & '1';     
				read_write <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 when 18 =>
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 19 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 20 =>
			 	if(Rec_ack ='1') then
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 21 => 
			 	if(Ready ='0') then
				  read_read <= '0'; 
				  read_send_ack <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 22 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out7 <= mstr_dout; -- READ REG7
			 	end if;
			 when 23 =>
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 24 => 
			 	if(Ready ='0') then
				  read_read <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 25 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out8 <= mstr_dout; -- READ REG8
			 	end if;
			 when 26 =>
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 27 => 
			 	if(Ready ='0') then
				  read_read <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 28 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out9 <= mstr_dout; -- READ REG9
			 	end if;
			 when 29 =>
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 30 => 
			 	if(Ready ='0') then
				  read_read <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 31 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out10 <= mstr_dout; -- READ REG10
			 	end if;
			 when 32 =>
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 33 => 
			 	if(Ready ='0') then
				  read_read <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 34 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out11 <= mstr_dout; -- READ REG11
			 	end if;
			 when 35 =>
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 36 => 
			 	if(Ready ='0') then
				  read_read <= '0';
				  read_send_ack <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 37 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out12 <= mstr_dout; -- READ REG12
			 	end if;
			--========read 135=============
			 when 38 =>
					read_stop <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 39 => 
			 	if(Ready ='0') then
				  read_stop <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 40 => 
			 	if(Free ='1') then
					read_start <= '1';
		 	  		read_reg_state <= read_reg_state + 1; 
			 	end if;
			 when 41 => -- iic start
			 	if(Ready ='0' ) then
				  read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 42 => 
			 	if(Ready ='1' ) then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 43 => -- iic slave addr
           		read_mstr_din <= dev_address & '0';     
				read_write <= '1';
		 	  	read_reg_state <= read_reg_state + 1;
			 when 44 =>
			 	if(Ready ='0') then
					read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 45 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 46 => -- iic reg addr
			 	if(Rec_ack ='1') then
				  	read_write <= '1';
           		  	read_mstr_din <= reg135_cnst; 		   
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 47 => 
			 	if(Ready ='0') then
					read_write <= '0';
			 	   	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 48 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 49 => 
			 	if(Rec_ack ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 50 =>
			 	   	read_reg_state <= read_reg_state + 1;
			 when 51 =>
			 	  read_reg_state <= read_reg_state + 1;
			 when 52 => -- repeted start
				read_start <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 53 =>
			 	if(Ready ='0') then
					read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 54 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 55 =>
           		read_mstr_din <= dev_address & '1';     
				read_write <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 when 56 =>
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 57 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 58 =>
			 	if(Rec_ack ='1') then
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 59 => 
			 	if(Ready ='0') then
				  read_read <= '0'; 
				  read_send_ack <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 60 => 
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out135 <= mstr_dout; -- READ REG135
			 	end if;

			 --============read reg 137========  
			 when 61 =>
					read_stop <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 when 62 => 
			 	if(Ready ='0') then
				  read_stop <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 63 => -- iic start
			 	if(Free ='1') then
					read_start <= '1';
		 	  		read_reg_state <= read_reg_state + 1; 
			 	end if;
			 when 64 => 
			 	if(Ready ='0' ) then
				  read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 65 => 
			 	if(Ready ='1' ) then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 66 => -- iic slave addr
           		read_mstr_din <= dev_address & '0';     
				read_write <= '1';
		 	  	read_reg_state <= read_reg_state + 1;
			 when 67 =>
			 	if(Ready ='0') then
					read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 68 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 69 => 
			 	if(Rec_ack ='1') then
				  	read_write <= '1';
           		  	read_mstr_din <= reg137_cnst; 		   
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 70 => -- iic reg addr
			 	if(Ready ='0') then
					read_write <= '0';
			 	   	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 71 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 72 => 
			 	if(Rec_ack ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 73 =>
			 	   	read_reg_state <= read_reg_state + 1;
			 when 74 =>
			 	  read_reg_state <= read_reg_state + 1;
			 when 75 => -- iic repeted start
				read_start <= '1';
			 	read_reg_state <= read_reg_state + 1;
			 when 76 =>
			 	if(Ready ='0') then
					read_start <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 77 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 78 =>
           		read_mstr_din <= dev_address & '1';     
				read_write <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 when 79 =>
			 	if(Ready ='0') then
				  read_write <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 80 =>
			 	if(Ready ='1') then
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 81 =>
			 	if(Rec_ack ='1') then
					read_read <= '1';
			 	  	read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 82 => 
			 	if(Ready ='0') then
				  read_read <= '0'; 
				  read_send_ack <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 83 => 
			 	if(Ready ='1') then
				  read_stop <= '1';
			 	  read_reg_state <= read_reg_state + 1;
			 	  data_out137 <= mstr_dout; -- READ REG137
			 	end if;
			 when 84 => 
			 	if(Ready ='0') then
				  read_stop <= '0';
			 	  read_reg_state <= read_reg_state + 1;
			 	end if;
			 when 85 => 
			 	if(Free ='1') then
			 	  read_reg_state <= 0;
			 	end if;
			when others =>  null;
          end case;
		end if;
	end process;

	write_proc:process(clk, reset) begin
		if(reset = '1')then
			write_reg_state <= 0;
           	write_mstr_dout <= (others => '0');     
			write_start <= '0';
			write_stop <= '0';
			write_write <= '0';
			write_read <= '0';
			write_busy <= '0'; 
			write_en_t <= '0';  
			si570_reset <= '1';
			write_powerup_run <= '0';
		elsif(clk'event and clk = '1')then 
			write_en_t <= write_en;
			case write_reg_state is			
			 when 0 =>
				write_busy <= '0';
			    write_start <= '0';
			    write_stop <= '0';
				write_write <= '0';
				write_read <= '0';
           		write_mstr_dout <= (others => '0');     
			 	if(write_en = '1' and write_en_t = '0') or (write_powerup_run = '0') then
			 	  write_powerup_run <= '1';
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 1 =>
				if(free = '1')then 
			 	  write_reg_state <= write_reg_state + 1;
				end if;
			 when 2 => -- iic start
				if(mux_busy = '0'and read_busy = '0') then
					write_start <= '1';
					write_busy <= '1';
		 	  		write_reg_state <= write_reg_state + 1;
		 	  	else
		 	  		write_reg_state <= 1;
		 	  	end if;	
			 when 3 => 
			 	if(Ready ='0' ) then
					write_start <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 4 => 
			 	if(Ready ='1' ) then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 5 => -- iic slave addr
           		write_mstr_dout <= dev_address & '0';     
				write_write <= '1';
		 	  	write_reg_state <= write_reg_state + 1;
			 when 6 =>
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 7 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 8 => -- iic reg addr
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= reg137_cnst; 		   
					write_write <= '1';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 9 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 10 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 11 => -- send freeze
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= DCO_Freeze; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 12 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 13 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 14 => -- stop iic
			 	if(Rec_ack ='1') then
					write_stop <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 15 => 
			 	if(Ready ='0') then
					write_stop <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 16 => -- iic start
			 	if(Free ='1') then
				  write_start <= '1';
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 17 => 
			 	if(Ready ='0') then
					write_start <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 18 => -- iic slave addr
			 	if(Ready ='1') then
           			write_mstr_dout <= dev_address & '0'; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 19 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 20 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 21 =>
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= reg7_cnst; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 22 => -- iic reg addr
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 23 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 24 => -- iic  write reg 7 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in7; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 25 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 26 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 27 => -- iic  write reg 8 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in8; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 28 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 29 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 30 => -- iic  write reg 9 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in9; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 31 =>
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 32 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 33 => -- iic  write reg 10 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in10; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 34 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 35 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 36 => -- iic  write reg 11 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in11; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 37 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 38 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 39 => -- iic  write reg 12 
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= data_in12; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 40 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 41 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;

			 when 42 =>
			 	if(Rec_ack ='1') then
					write_stop <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 43 => 
			 	if(Ready ='0') then
					write_stop <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 44 =>   -- iic start
			 	if(Free ='1' ) then
					write_start <= '1';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;       
			 when 45 => 
			 	if(Ready ='0' ) then
					write_start <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 46 => 
			 	if(Ready ='1' ) then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 47 => --iic slave addr
           		write_mstr_dout <= dev_address & '0';     
				write_write <= '1';
		 	  	write_reg_state <= write_reg_state + 1;
			 when 48 =>
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 49 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 50 => -- iic reg addr
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= reg137_cnst; 		   
					write_write <= '1';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 51 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 52 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 53 => -- iic Unfreeze
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= DCO_UnFreeze; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 54 => 
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 55 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 56 =>
			 	if(Rec_ack ='1') then
					write_stop <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 57 => 
			 	if(Ready ='0') then
					write_stop <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 58 =>
			 	if(Free ='1') then
					write_start <= '1';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;       
			 when 59 => -- iic start
			 	if(Ready ='0' ) then
					write_start <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 60 => -- first set the reg pointer to read in burst mode
			 	if(Ready ='1' ) then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 61 => -- send slave addr
           		write_mstr_dout <= dev_address & '0';     
				write_write <= '1';
		 	  	write_reg_state <= write_reg_state + 1;
			 when 62 =>
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 63 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 64 => -- rcv ack and send address 137 to freeze
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= reg135_cnst; 		   
					write_write <= '1';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 65 => -- send reg addr
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 66 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 67 =>
			 	if(Rec_ack ='1') then
           			write_mstr_dout <= si570_NewFreq; 		   
					write_write <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 68 => -- send reg addr
			 	if(Ready ='0') then
					write_write <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 69 =>
			 	if(Ready ='1') then
			 	  write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 70 =>
			 	if(Rec_ack ='1') then
					write_stop <= '1';
			 		write_reg_state <= write_reg_state + 1;
			 	end if;
			 when 71 => -- send reg addr
			 	if(Ready ='0') then
					write_stop <= '0';
			 	  	write_reg_state <= write_reg_state + 1;
			 	end if;

--===============================  
			 when 72 =>
			 	if(Free ='1') then
					si570_reset <= '0';
			 	  	write_reg_state <= 0;
			 	end if;       
			 	
			when others =>  null;
          end case;
		end if;
	end process;


end architecture rtl ; -- of si570_master
