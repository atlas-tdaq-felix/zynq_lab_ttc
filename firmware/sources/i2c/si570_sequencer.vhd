----------------------------------------------------------------------
------  Revision history  --------------------------------------------
----------------------------------------------------------------------
-- Date:  By:         Remark:
-- 230212 Wilco Vink  Add additional state after reset to configure
--                    the default frequency to 120 MHZ, after power up
----------------------------------------------------------------------


------ Getting Fxtal first ---------
------ default Fout is 1GHz --------

--RFREQ = 43.7923800684511661529541015625
--(1000000000 * 5 * 1) / 43.7923800684511661529541015625
--    =  114175114.30492200424323266379194
-- ~= 114175114Hz
-- ~= 114.175114 MHz

-------------------------------------------
------ Finding a HSDIV, N1, and RFREQ  ----	

--F_out : 425MHz  << this is the target Fout
--425 * 6(HSDIV) * 2(N1) = 5100
--RFREQ = 5100 / 114.175114 = 44.668227789113484046970165495083
--0.66822778911348404697016549508328 * 2^28 = 179376031.28254992589716179306815
-- ~= 179376031  => 0xAB10F9F
--44 -> 0x2C
--Final RFREQ is 0x02CAB10F9F


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

						
entity si570_sequencer is
	port (
		clk: in std_logic; -- system 50MHz clk
		reset_in : in std_logic; -- system reset/power on reset

		address : out std_logic_vector(6 downto 0); -- 7bits device address
		reg_add : out std_logic_vector(7 downto 0);
		write_read : out std_logic; -- 0: read   1:write
		process_start : out std_logic; -- 0:stop   1:start
		process_done : in std_logic; -- 0: under process   1:process done
		
		data_read : in  std_logic_vector(7 downto 0);    
		data_write : out std_logic_vector(7 downto 0)
		
	);
end si570_sequencer;

architecture rtl of si570_sequencer is
	signal reset : std_logic;
	constant dev_address : std_logic_vector(6 downto 0):= "1011101";
	signal counter : integer range 0 to 127 := 100;
	signal time50ms : std_logic;
	signal wait50start : std_logic;
	
	signal data_out7 : std_logic_vector(7 downto 0);
	signal data_out8 : std_logic_vector(7 downto 0);
	signal data_out9 : std_logic_vector(7 downto 0);
	signal data_out10 : std_logic_vector(7 downto 0);
	signal data_out11 : std_logic_vector(7 downto 0);
	signal data_out12 : std_logic_vector(7 downto 0);
	signal data_out135 : std_logic_vector(7 downto 0);
	signal data_out137 : std_logic_vector(7 downto 0);


	signal write_triger, reset_triger : std_logic;
	signal reg7_out, reg8_out, reg9_out, reg10_out, reg11_out, reg12_out : std_logic_vector(7 downto 0);
begin

	reset <= reset_in;
	
	

	process(clk, reset, write_triger, reset_triger)begin
		if(reset = '1')then
			--counter <= 100;
			counter <= 19;
			wait50start <= '0';
			process_start <= '0';
			reg_add <= x"00";
			data_write <= x"00";
            address <= dev_address;
			write_read <= '1';
--		elsif(write_triger = '0')then  -- section commented driven by virtual Jtag device(removed for porting to xilinx)
--			counter <= 0;
--			wait50start <= '0';
--			process_start <= '0';
--			reg_add <= x"00";
--			data_write <= x"00";
--			write_read <= '1';
--		elsif(reset_triger = '0')then
--			counter <= 96;
--			wait50start <= '0';
--			process_start <= '0';
--			reg_add <= x"00";
--			data_write <= x"00";
--			write_read <= '1';
		elsif(clk'event and clk = '1')then
			case counter is			
-- Set after reset to 120 MHz



-- Setting VCXO to be 425MHz			
			when 0 =>
				data_out7 <= (others => '0');
				data_out8 <= (others => '0');
				data_out9 <= (others => '0');
				data_out10 <= (others => '0');
				data_out11 <= (others => '0');
				data_out12  <= (others => '0');
				data_out135 <= (others => '0');
				data_out137 <= (others => '0');
			
				reg_add <= "10001001"; --reg137
				data_write <= "00010000"; --freeze!!
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 1 =>
				reg_add <= "00000111"; -- reg7
				data_write <= reg7_out; -- reg7
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 2 =>
				reg_add <= "00001000"; -- reg8
				data_write <= reg8_out; -- reg8
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 3 =>
				reg_add <= "00001001"; --reg9
				data_write <= reg9_out; --reg9
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 4 =>
				reg_add <= "00001010";  --reg10
				data_write <= reg10_out;  --reg10
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 5 =>
				reg_add <= "00001011"; --reg11
				data_write <= reg11_out; --reg11
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 6 =>
				reg_add <= "00001100"; --reg12
				data_write <= reg12_out; --reg12
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 7 =>
				reg_add <= "10001001"; --reg137
				data_write <= x"00";
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 8 =>
				reg_add <= "10000111"; --reg135
				data_write <= "01000000";
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= 100; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
----------------------------------------------------------
---- 23-2-12 Wilco Vink
---- This is to reset the Si570 to 120 MHz 
---- after power on reset 
----------------------------------------------------------

			when 19 =>  -- set PCA9548 mux on VC707 to first port Si570
				data_out7 <= (others => '0');
				data_out8 <= (others => '0');
				data_out9 <= (others => '0');
				data_out10 <= (others => '0');
				data_out11 <= (others => '0');
				data_out12  <= (others => '0');
				data_out135 <= (others => '0');
				data_out137 <= (others => '0');
			
                address <= "1110100";     -- select PCA9548A iic mux
				reg_add <= "00000001";    -- select PCA9548A iic mux port 1 use regaddres as first dat word
				data_write <= "00000001"; -- last data word always acts as first byte in PCA9548A
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 2; -- go to next state and set next data (skip freeze state)
					process_start <= '0'; -- stop process
				end if;
			when 20 =>
				data_out7 <= (others => '0');
				data_out8 <= (others => '0');
				data_out9 <= (others => '0');
				data_out10 <= (others => '0');
				data_out11 <= (others => '0');
				data_out12  <= (others => '0');
				data_out135 <= (others => '0');
				data_out137 <= (others => '0');
                address <= dev_address;
			
				reg_add <= "10001001"; --reg137
				data_write <= "00010000"; --freeze!!
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 21 =>
                address <= dev_address;
				reg_add <= "00000111"; -- reg7
				data_write <= x"E0"; -- reg7
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 22 =>
				reg_add <= "00001000"; -- reg8
				data_write <= x"C2"; -- reg8
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 23 =>
				reg_add <= "00001001"; --reg9
				data_write <= x"E3"; --reg9
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 24 =>
				reg_add <= "00001010";  --reg10
				data_write <= x"31";  --reg10
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 25 =>
				reg_add <= "00001011"; --reg11
				data_write <= x"5E"; --reg11
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 26 =>
				reg_add <= "00001100"; --reg12
				data_write <= X"87"; --reg12
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 27 =>
				reg_add <= "10001001"; --reg137
				data_write <= x"00";
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 28 =>
				reg_add <= "10000111"; --reg135
				data_write <= "01000000";
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= 100; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;

----------------------------------------------------------
---- This is to reset the Si570 in default condition -----
----------------------------------------------------------
			when 96 =>
				reg_add <= "10001001"; --reg137
				data_write <= "00010000"; --freeze!!
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 97 =>
				reg_add <= "10000111"; --reg135
				data_write <= "00000001"; -- recall all setting from NVM
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 98 =>
				reg_add <= "10001001"; --reg137
				data_write <= "00000000"; --Un-freeze
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;
			when 99 =>
				reg_add <= "10000111"; --reg135
				data_write <= "01000000"; -- set with the default value
				write_read <= '1'; -- write
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= 100; -- go to next state and set next data
					process_start <= '0'; -- stop process
				end if;


-----------------------------------------------
---------------- Read Section -----------------
-----------------------------------------------
			when 100 =>
				reg_add <= "00000111"; -- reg7
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out7 <= data_read;
					process_start <= '0'; -- stop process
				end if;
				
			when 101 =>
				reg_add <= "00001000"; -- reg8
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out8 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when 102 =>
				reg_add <= "00001001"; -- reg9
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out9 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when 103 =>
				reg_add <= "00001010"; -- reg10
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out10 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when 104 =>
				reg_add <= "00001011"; -- reg11
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out11 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when 105 =>
				reg_add <= "00001100"; -- reg12
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out12 <= data_read;
					process_start <= '0'; -- stop process
				end if;
				
			when 106 =>
				reg_add <= "10000111"; -- reg135
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= counter + 1; -- go to next state and set next data
					data_out135 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when 107 =>
				reg_add <= "10001001"; -- reg137
				data_write <= x"00"; --dummy data
				write_read <= '0'; -- read
				process_start <= '1'; -- start process;
				if(process_done = '1')then -- when process done,
					counter <= 100;
					data_out137 <= data_read;
					process_start <= '0'; -- stop process
				end if;

			when others =>
			end case;
		end if;
	end process;
end rtl;

