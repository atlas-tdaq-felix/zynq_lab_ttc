library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"


entity led_drv is
port
(
clk  		: in   std_logic;
led_out		: out std_logic_vector(1 downto 0);
pb  		: in   std_logic
);

end entity;
    
architecture RTL of led_drv is

signal clk_count : std_logic_vector(10 downto 0) := (others => '1');

begin

led_out(1) <= pb;
process(clk)
begin
	if rising_edge(clk) then
		clk_count <= clk_count + 1;
	  	if(clk_count(10) = '1') then
 		  led_out(0) <= '1';
		else
		  led_out(0) <= '0';
		end if;
	end if;
end process;
end RTL;

 
