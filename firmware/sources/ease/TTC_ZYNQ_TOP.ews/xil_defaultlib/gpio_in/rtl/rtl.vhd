-- EASE/HDL begin --------------------------------------------------------------
-- 
-- Architecture 'rtl' of entity 'gpio_in'.
-- 
--------------------------------------------------------------------------------
-- 
-- Copy of the interface declaration:
-- 
--   port (
--     triger      : out    std_logic;
--     ECR         : out    std_logic;
--     BCR         : out    std_logic;
--     BCR_mux     : out    std_logic;
--     BCR_push    : out    std_logic;
--     ECR_mux     : out    std_logic;
--     ECR_push    : out    std_logic;
--     triger_mux  : out    std_logic;
--     triger_push : out    std_logic;
--     gpio_in     : out    std_logic_vector(8 downto 0));
-- 
-- EASE/HDL end ----------------------------------------------------------------

architecture rtl of gpio_in is

begin

end architecture rtl ; -- of gpio_in

