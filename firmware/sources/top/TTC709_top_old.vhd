--!------------------------------------------------------------------------------
--!                                                             
--!           NIKHEF - National Institute for Subatomic Physics 
--!
--!                       Electronics Department                
--!                                                             
--!-----------------------------------------------------------------------------

library ieee, work, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use work.TTC709_pkg.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
use ieee.std_logic_1164.all;

entity TTC709_top is
  port (
    clk_ref_n            : in     std_logic;
    clk_ref_p            : in     std_logic;
	triger				 : in     std_logic;
	si5324_nreset		: out std_logic;
	ECR				 : in     std_logic;
	BCR				 : in     std_logic;
    sys_rst              : in     std_logic;
	BCR_mux				 : in     std_logic;
	BCR_push			 : in     std_logic;
	ECR_mux				 : in     std_logic;
	ECR_push			 : in     std_logic;
	triger_mux			 : in     std_logic;
	triger_push			 : in     std_logic;
    iic_clk        : inout  std_logic;
    iic_data       : inout  std_logic;
	USER_CLOCK_P		 : in std_logic;
	USER_CLOCK_N		 : in std_logic;
	SFP_TX_N			 : out std_logic;
	SFP_TX_P			 : out std_logic;
	SFP_RX_N			 : in std_logic;
	SFP_RX_P			 : in std_logic;
	iic_mux_reset		 : out std_logic;
	USER_SMA_CLOCK_P	 : out std_logic;
	USER_SMA_CLOCK_N	 : out std_logic;
	USER_SMA_GPIO_P	 	 : out std_logic;
	USER_SMA_GPIO_N	 	 : out std_logic;
	GTREFCLK0_1_P_IN	 : in std_logic;
	GTREFCLK0_1_N_IN	 : in std_logic;
	Tout_P			 	 : out    std_logic_vector(7 downto 0);
	Tout_n			 	 : out    std_logic_vector(7 downto 0)
  );
end entity TTC709_top;


architecture structure of TTC709_top is

-- ttc hamming encoding for broadcast (d8/h5)
	-- /* build Hamming bits */
	-- hmg[0] = d[0]^d[1]^d[2]^d[3];
	-- hmg[1] = d[0]^d[4]^d[5]^d[6];
	-- hmg[2] = d[1]^d[2]^d[4]^d[5]^d[7];
	-- hmg[3] = d[1]^d[3]^d[4]^d[6]^d[7];
	-- hmg[4] =hmg[0]^hmg[1]^hmg[2]^hmg[3]^d[0]^d[1]^d[2]^d[3]^d[4]^d[5]^d[6]^d[7]; --eric: d[0]^d[2]^d[3]^d[5]^d[6]^d[7];--
	-- /* build Hamming word */
	-- hamming = hmg[0] | (hmg[1]<<1) |(hmg[2]<<2) |(hmg[3]<<3) |(hmg[4]<<4);
signal HMG : std_logic_vector(4 downto 0) := (others => '0');
signal triger_r : std_logic := '0';
signal ECR_r : std_logic := '0';
signal BCR_r : std_logic := '0';
signal triger_is : std_logic := '0';
signal si570_ctrl : si570_ctrl_record := si570_ctrl_init;
signal si570_status : si570_status_record;
signal ECR_is : std_logic := '0';
signal BCR_is : std_logic := '0';
signal triger_rst : std_logic := '0';
signal ECR_rst : std_logic := '0';
signal BCR_rst : std_logic := '0';
constant zero_cnst : std_logic := '0';
signal clk100 : std_logic := '0';
signal clk_160M : std_logic := '0';
signal clk_80M : std_logic := '0';
signal clk_40M : std_logic := '0';
signal TTC_out : std_logic := '0';
signal SFP_t : std_logic;
signal rxoutclk : std_logic;
signal txoutclk : std_logic; 
signal data_valid : std_logic; 
signal txdata : std_logic_vector(15 downto 0);
signal rxdata : std_logic_vector(15 downto 0);
signal gtrefclk0 : std_logic:= '0'; 
signal gtrefclk1 : std_logic:= '0'; 
signal txoutclk_out : std_logic := '0'; 
signal USER_SMA_GPIO : std_logic := '0';
signal si570_reset_in : std_logic := '0';
signal USER_CLOCK_160M16 : std_logic := '0';
signal soft_hard_reset : std_logic := '0';
component TTCvi_trans_top
  port (
    data_valid_in : in     std_logic;
    gtrefclk0_in  : in     std_logic;
    gtrefclk1_in  : in     std_logic;
    gtxrxn_in     : in     std_logic;
    gtxrxp_in     : in     std_logic;
    gtxtxn_out    : out    std_logic;
    gtxtxp_out    : out    std_logic;
    reset_in      : in     std_logic;
    rxdata_out    : out    std_logic_vector(15 downto 0);
    rxoutclk_out  : out    std_logic;
    rxusrclk_in   : in     std_logic;
    sysclk_in     : in     std_logic;
    txdata_in     : in     std_logic_vector(15 downto 0);
    txoutclk_out  : out    std_logic;
    txusrclk_in   : in     std_logic);
end component;


component selectio_wiz_0
generic
 (-- width of the data for the system
  SYS_W       : integer := 1;
  -- width of the data for the device
  DEV_W       : integer := 1);
port
 (
  -- From the device out to the system
  data_out_from_device    : in    std_logic_vector(DEV_W-1 downto 0);
  data_out_to_pins_p      : out   std_logic_vector(SYS_W-1 downto 0);
  data_out_to_pins_n      : out   std_logic_vector(SYS_W-1 downto 0);

 
-- Clock and reset signals
  clk_in_p                : in    std_logic;                    -- Differential fast clock from IOB
  clk_in_n                : in    std_logic;
  clk_out                 : out   std_logic;
  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
end component;

component clk_wiz_200to80_40
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;
  clk_out2          : out    std_logic;
  clk_out3          : out    std_logic;
  clk_out4          : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1_p         : in     std_logic;
  clk_in1_n         : in     std_logic
 );
end component;


begin

clock_80_40_inist : clk_wiz_200to80_40
   port map ( 
   clk_out1 => clk_80M,
   clk_out2 => clk_40M,
   clk_out3 => clk_160M,
   clk_out4 => clk100,
   reset => zero_cnst,
   locked => open,
   clk_in1_p => clk_ref_p,
   clk_in1_n => clk_ref_n
 );


edge_det_inst : entity work.edge_det
   port map ( 
   clk_80M          => clk_80M,
   clk_160M          => txoutclk, --USER_CLOCK_160M16,
	triger				=>  triger,			
	ECR				=>  ECR,			
	BCR				=>  BCR,			
    sys_rst         =>  soft_hard_reset,     
	BCR_mux			=> 	BCR_mux,		
	BCR_push		=> 	BCR_push,	
	ECR_mux			=> 	ECR_mux,		
	ECR_push		=> 	ECR_push,	
	triger_mux		=> 	triger_mux,	
	triger_push		=> 	triger_push,
	ECR_r			=>  ECR_r,
	triger_r		=>  triger_r,
	BCR_r			=>  BCR_r
);	

TTC_FSM_inst : entity work.TTC_FSM
   port map ( 
   clk_160M          => txoutclk, --USER_CLOCK_160M16,
    sys_rst         =>  soft_hard_reset,     
	ECR_r			=>  ECR_r,
	triger_r		=>  triger_r,
	BCR_r			=>  BCR_r,
	TTC_out			=> TTC_out
);	

the_si5324: entity work.si5324_i2c
    port map(
      clk             => clk100,
      iic_mux_reset_n => open, --iic_mux_reset,
      reset_in        => soft_hard_reset,
      si570_ctrl      => si570_ctrl,
      iic_clk   => iic_clk,
      iic_data  => iic_data,
      si570_reset     => si570_reset_in,
      si570_status    => si570_status);
	  
the_si5324_reset: entity work.rest_init_test
    port map(
      clk100          => clk100,
      Rst_out 		  => soft_hard_reset,
      rst_in          => sys_rst,
      si570_reset     => si570_reset_in
);

iic_mux_reset <= '1';
si5324_nreset <= '1';
the_TTC709_trans_top: entity work.TTC709_transciever_top
    port map(
		data_valid_in =>  data_valid,
		SOFT_RESET_TX_IN => si570_reset_in,
		SOFT_RESET_RX_IN => si570_reset_in,	
		Q1_CLK0_GTREFCLK_PAD_P_IN  =>  GTREFCLK0_1_P_IN, 
		Q1_CLK0_GTREFCLK_PAD_N_IN  =>  GTREFCLK0_1_N_IN, 
		rxn_in        =>  SFP_RX_N, -- Ok
		rxp_in        =>  SFP_RX_P, -- Ok
        txn_out       =>  SFP_TX_N, -- Ok
        txp_out       =>  SFP_TX_P, -- Ok
	    reset_in      =>  sys_rst, -- Ok
	    rxdata_out    =>  rxdata,
	    rxoutclk_out  =>  rxoutclk,
	    sysclk_in     =>  clk100,
	    txdata_in     =>  txdata,
	    txoutclk_out  =>  txoutclk
	  );

--BUFG_inst0 : BUFG
--port map (
--I 			=> txoutclk, 
--O 		=> txoutclk_out
--);

--IBUFDS_GTE2_inst0 : IBUFDS_GTE2
--generic map (
--CLKCM_CFG 	=> true,
--CLKRCV_TRST	=> true,
--CLKSWING_CFG 	=> "11" -- Refer to Transceiver User Guide
--)
--port map (
--O 			=> gtrefclk1, -- 1-bit output: Refer to Transceiver User Guide
--ODIV2 		=> open, -- 1-bit output: Refer to Transceiver User Guide
--CEB 		=> '0', -- 1-bit input: Refer to Transceiver User Guide
--I 			=> GTREFCLK0_1_P_IN, -- 1-bit input: Refer to Transceiver User Guide
--IB 			=> GTREFCLK0_1_N_IN -- 1-bit input: Refer to Transceiver User Guide
--);
	  
process(txoutclk, soft_hard_reset)
begin
		if(rising_edge(txoutclk)) then
			if(soft_hard_reset = '1') then
				txdata <= (others => '1');
				data_valid <= '0';
			else
				data_valid <= '1';
				txdata <= (others => TTC_out);
			end if;
		end if;
end process;		

OBUFDS_inst0 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => TTC_out,
    O         		=>  Tout_P(0),     
	OB				=>  Tout_n(0)
);	

OBUFDS_inst00 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => TTC_out,
    O         		=>  USER_SMA_CLOCK_P,     
	OB				=>  USER_SMA_CLOCK_N
);	

OBUFDS_inst1 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => triger,
    O         		=>  Tout_P(1),     
	OB				=>  Tout_n(1)
);	

OBUFDS_inst2 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => triger_r,
    O         		=>  Tout_P(2),     
	OB				=>  Tout_n(2)
);	

OBUFDS_inst3 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => ECR_r,
    O         		=>  Tout_P(3),     
	OB				=>  Tout_n(3)
);	
OBUFDS_inst4 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => BCR_r,
    O         		=>  Tout_P(4),     
	OB				=>  Tout_n(4)
);	

OBUFDS_inst5 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               =>  clk_40M,
    O         		=>  Tout_P(5),     
	OB				=>  Tout_n(5)
);	


--your_instance_name : selectio_wiz_0
--   port map 
--   ( 
--   data_out_from_device => (OTHERS =>  TTC_out),
--   data_out_to_pins_p(0) => SFP_TX_P,
--   data_out_to_pins_n(0) => SFP_TX_N,
--   clk_in_p => USER_CLOCK_P,                          
--   clk_in_n => USER_CLOCK_P,
--   clk_out => SFP_t,
--   io_reset => '0'
--);

OBUFDS_inst7 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => USER_CLOCK_160M16,
    O         		=>  Tout_P(6),
	OB				=>  Tout_N(6) 
);	

IBUFDS_inst8 : IBUFDS
   port map ( 
    I               => USER_CLOCK_P,
	IB				=>  USER_CLOCK_N,
    O         		=>  USER_CLOCK_160M16 --SFP_t
);	

--IBUFDS_inst9 : IBUFDS
--   port map ( 
--    I               => USER_SMA_GPIO_P,
--	IB				=>  USER_SMA_GPIO_N,
--    O         		=>  USER_SMA_GPIO
--);	

--OBUFDS_inst8 : OBUFDS
--	generic map(IOSTANDARD => "LVDS_25")
--   port map ( 
--    I               => rxdata(0), --USER_SMA_GPIO,
--    O         		=>  USER_SMA_GPIO_P, --Tout_P(7),
--	OB				=>  USER_SMA_GPIO_N --Tout_N(7) 
--);	
USER_SMA_GPIO_P <= txoutclk;
USER_SMA_GPIO_N <= txoutclk;

OBUFDS_inst9 : OBUFDS
	generic map(IOSTANDARD => "LVDS_25")
   port map ( 
    I               => rxdata(0), --USER_SMA_GPIO,
    O         		=>  Tout_P(7),
	OB				=>  Tout_N(7) 
);	



end architecture structure; 

