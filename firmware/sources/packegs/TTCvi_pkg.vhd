--------------------------------------------------------------------------------
-- Object        : Package work.VirgoPhasecamera_pkg
-- Version       : 5399 (Subversion)
-- Last modified : Tue Sep 19 13:09:36 2017.
--------------------------------------------------------------------------------



library ieee, work;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

package TTCvi_pkg is

--==============================
  TYPE si570_ctrl_record IS RECORD
      si570_reg7                      : std_logic_vector(7 DOWNTO 0);
      si570_reg8                      : std_logic_vector(7 DOWNTO 0);
      si570_reg9                      : std_logic_vector(7 DOWNTO 0);
      si570_reg10                     : std_logic_vector(7 DOWNTO 0);
      si570_reg11                     : std_logic_vector(7 DOWNTO 0);
      si570_reg12                     : std_logic_vector(7 DOWNTO 0);
      si570_reg135                     : std_logic_vector(7 DOWNTO 0);
      si570_reg137                     : std_logic_vector(7 DOWNTO 0);
      si570_mux_en                     	: std_logic;
      si570_read_en                     	: std_logic;
      si570_write_en                     	: std_logic;
      si570_iic_reset                     	: std_logic;
      si570_new_freq                     	: std_logic;
      Fan_Control                     : std_logic_vector(15 DOWNTO 0);
  END record;        

  constant  si570_reg7_init     : std_logic_vector(7 DOWNTO 0):=  X"01"; --"01"; --"01"; --"01"; --"01"; --"60"; --X"03";
  constant  si570_reg8_init     : std_logic_vector(7 DOWNTO 0):=  X"c2"; --"c2"; --"c2"; --"c2"; --"c2"; --"C3"; --X"C2";
  constant  si570_reg9_init     : std_logic_vector(7 DOWNTO 0):=  X"ce"; --"ce"; --"ce"; --"ce"; --"ce"; --"0F"; --X"CC";
  constant  si570_reg10_init     : std_logic_vector(7 DOWNTO 0):= X"86"; --"38"; --"86"; --"86"; --"38"; --"FE"; -- X"CE";
  constant  si570_reg11_init     : std_logic_vector(7 DOWNTO 0):= X"35"; --"ae"; --"35"; --"59"; --"ae"; --"FA"; --X"50";
  constant  si570_reg12_init     : std_logic_vector(7 DOWNTO 0):= X"c3"; --"14"; --"c3"; --"02"; --"14"; --"EC"; --X"F1";
  constant  si570_reg135_init    : std_logic_vector(7 DOWNTO 0):= X"00"; --"00"; --"00"; --"00"; --"00"; --"00";
  constant  si570_reg137_init    : std_logic_vector(7 DOWNTO 0):= X"08"; --"08"; --"08"; --"08"; --"08"; --"08";

  constant  si570_mux_en_init     : std_logic := '0';
  constant  si570_read_en_init     : std_logic:= '0';
  constant  si570_write_en_init     : std_logic:= '0';
  constant  si570_iic_reset_init     : std_logic:= '0';
  constant  si570_new_freq_init     : std_logic:= '0';
  constant  Fan_Control_init     : std_logic_vector(15 DOWNTO 0):= X"FFFF";
  -- initial value for Io control
  CONSTANT si570_ctrl_init  : si570_ctrl_record :=(
	si570_reg7_init,
	si570_reg8_init,
	si570_reg9_init,
	si570_reg10_init,
	si570_reg11_init,
	si570_reg12_init,
	si570_reg135_init,
	si570_reg137_init,
	si570_mux_en_init,
	si570_read_en_init,
	si570_write_en_init,
	si570_iic_reset_init,
	si570_new_freq_init,
	Fan_Control_init
      );                         

  TYPE si570_status_record IS RECORD
      si570_reg7_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg8_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg9_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg10_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg11_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg12_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg135_out    : std_logic_vector(7  DOWNTO 0);
      si570_reg137_out    : std_logic_vector(7  DOWNTO 0);
      si570_debug_read    : std_logic_vector(7  DOWNTO 0);
      si570_debug_write    : std_logic_vector(7  DOWNTO 0);
      si570_debug_mux    : std_logic_vector(7  DOWNTO 0);
      si570_debug_iic    : std_logic_vector(7  DOWNTO 0);
  END record;

type si5324_reg_type is array (0 to 42) of std_logic_vector(15 downto 0);
constant  si5324_init    : si5324_reg_type := (
X"5400",
X"E401",
X"4202",
X"1503",
X"9204",
X"ED05",
X"3F06",
X"2A07",
X"0008",
X"C009",
X"000A",
X"400B",
X"2913",
X"3E14",
X"FE15",
X"DF16",
X"1F17",
X"3F18",
X"8019",
X"001F",
X"0020",
X"0321",
X"0022",
X"0023",
X"0324",
X"0128",
X"3829",
X"552A",
X"002B",
X"1B2C",
X"D42D",
X"002E",
X"1B2F",
X"D430",
X"0037",
X"1F83",
X"0284",
X"0189",
X"0F8A",
X"FF8B",
X"008E",
X"008F",
X"4088"
);
--=============================


end package TTCvi_pkg ;
