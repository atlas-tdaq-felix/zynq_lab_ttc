--!------------------------------------------------------------------------------
--!                                                             
--!           NIKHEF - National Institute for Subatomic Physics 
--!
--!                       Electronics Department                
--!                                                             
--!-----------------------------------------------------------------------------

library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
use ieee.std_logic_1164.all;

entity TTC_FSM is
  port (
    clk_160M            : in     std_logic;
    sys_rst_n            : in     std_logic;
	ECR_r				 : in     std_logic;
	trigger_r			 : in     std_logic;
	BCR_r				 : in     std_logic;
	BCR_count_en         : out    std_logic;
	TTC_out			     : out    std_logic
  );
end entity TTC_FSM;


architecture structure of TTC_FSM is

signal hmg_br : std_logic_vector(4 downto 0) := (others => '0');
signal d_br : std_logic_vector(7 downto 0) := (others => '0');
signal s_br : std_logic_vector(4 downto 0) := (others => '0');
signal HMG : std_logic_vector(4 downto 0) := (others => '0');
signal DATA2HMG : std_logic_vector(7 downto 0) := (others => '0');
signal DATA2SEND : std_logic_vector(15 downto 0) := (others => '0');
signal trigger_is : std_logic := '0';
signal ECR_is : std_logic := '0';
signal BCR_is : std_logic := '0';
signal trigger_rst : std_logic := '0';
signal ECR_rst : std_logic := '0';
signal BCR_rst : std_logic := '0';
signal TTC_out_t : std_logic := '0';
signal test_2b : std_logic_vector(1 downto 0) := "00";
signal command_4b : std_logic_vector(3 downto 0) := "0000";
constant zero_cnst : std_logic := '0';
constant start_bit2_cnst : std_logic_vector(1 downto 0) := "00";
constant stop_bit1 : std_logic := '0';
signal bit_count : integer := 15;
signal hamming : std_logic_vector(4 downto 0) := "00000";


type TTCvi_type is (idle0,idle1,idle2,idle3, DATABit0, DATABit1, DATABit2, DATABit3) ;
signal TTCvi_st  : TTCvi_type  := idle0;
signal Sim_TTCvi_st  : TTCvi_type  := idle0;


--  2.1. Calculation of Hamming Code for 8 Bit Data
--  If we take 8 bit data, then 4 parity bit is needed because
--  20, 21, 22, 23 , these 4 position are reserved for parity bit.
--  Let Data Word = D1 D2 D3 D4 D5 D6 D7 D8 and
--  Parity bit = P1, P2, P4, P8.
--  P1 = D1  D2  D4  D5  D7
--  P2 = D1  D3  D4  D6  D7
--  P4 = D2  D3  D4  D8
--  P8 = D5  D6  D7  D8
--  CODE WORD: P1 P2 D1 P4 D2 D3 D4 P8 D5 D6
--  D7 D8
--  Then check the parity bit for detecting the error by check
--  bit.
--  C1 = P1  D1  D2  D4  D5  D7
--  C2 = P2  D1  D3  D4  D6  D7
--  C3 = P3  D2  D3  D4  D8
--  C4 = P4  D5  D6  D7  D8

--  If the result, C = C8 C4 C2 C1 =0000, indicates that no error
--  has occurred. However, if C ≠ 0 , then 4 bit binary number
--  is formed and gives the location of the error bit
--
--  The basic Hamming code can detect and correct an error in only a single bit.
--  Some multiple-bit errors are detected, but they may be corrected erroneously, as if
--  they were single-bit errors. By adding another parity bit to the coded word, the
--  Hamming code can be used to correct a single error and detect double errors. If
--  we include this additional parity bit, the previous 12-bit coded word becomes
--  001110010100P13 , where P13 is evaluated from the exclusive-OR of the other 12
--  bits. This produces the 13-bit word 0011100101001 (even parity). When this word
--  is read from memory, the check bits and also the parity bit P are evaluated over
--  the entire 13 bits. If P  0, the parity is correct (even parity), but if P  1, the par-
--  ity over the 13 bits is in correct (odd parity). The following four cases can occur:

begin
-- ttc hamming encoding for broadcast (d8/h5)
	-- /* build Hamming bits */
process(clk_160M)
begin
	if(rising_edge(clk_160M)) then
		DATA2SEND <= '1' & HMG(0) & HMG(1) & HMG(2) & HMG(3) & HMG(4) & BCR_is & ECR_is & command_4b(0)  & command_4b(1)  & command_4b(2) & command_4b(3) & test_2b(0) & test_2b(1) & "00";	
--		DATA2SEND <= '1' & HMG(4 downto 0) & BCR_is & ECR_is & command_4b(3 downto 0) & test_2b(1 downto 0) & "00";	
--		DATA2SEND <= '1' & HMG(4) & BCR_is & ECR_is & command_4b(3) & command_4b(2) & HMG(3)  & command_4b(1) & command_4b(0) & test_2b(1) & HMG(2) & test_2b(0) & HMG(1) & HMG(0) & "00";	
--		DATA2HMG <= BCR_is & ECR_is & command_4b & test_2b;
--		DATA2HMG <= BCR_is & ECR_is & command_4b(0)  & command_4b(1)  & command_4b(2) & command_4b(3) & test_2b(0) & test_2b(1);
		DATA2HMG <= test_2b(1) & test_2b(0) & command_4b(3) & command_4b(2) & command_4b(1) & command_4b(0) & ECR_is & BCR_is ;
	end if;
end process;	
--		HMG(0) <= DATA2HMG(0) XOR DATA2HMG(1) XOR DATA2HMG(3) XOR DATA2HMG(4) XOR DATA2HMG(6);
--		HMG(1) <= DATA2HMG(0) XOR DATA2HMG(2) XOR DATA2HMG(3) XOR DATA2HMG(5) XOR DATA2HMG(6);
--		HMG(2) <= DATA2HMG(1) XOR DATA2HMG(2) XOR DATA2HMG(3) XOR DATA2HMG(7);
--		HMG(3) <= DATA2HMG(4) XOR DATA2HMG(5) XOR DATA2HMG(6) XOR DATA2HMG(7);
		HMG(0) <= DATA2HMG(0) XOR DATA2HMG(1) XOR DATA2HMG(2) XOR DATA2HMG(3);
		HMG(1) <= DATA2HMG(0) XOR DATA2HMG(4) XOR DATA2HMG(5) XOR DATA2HMG(6);
		HMG(2) <= DATA2HMG(1) XOR DATA2HMG(2) XOR DATA2HMG(4) XOR DATA2HMG(5) XOR DATA2HMG(7);
		HMG(3) <= DATA2HMG(1) XOR DATA2HMG(3) XOR DATA2HMG(4) XOR DATA2HMG(6) XOR DATA2HMG(7);
		HMG(4) <= HMG(0) XOR HMG(1) XOR HMG(2) XOR HMG(3) XOR DATA2HMG(0) XOR DATA2HMG(1) XOR DATA2HMG(2) XOR DATA2HMG(3) XOR DATA2HMG(4) XOR DATA2HMG(5) XOR DATA2HMG(6) XOR DATA2HMG(7); --eric: d(0]^d(2]^d(3]^d(5]^d(6]^d(7];--

process(clk_160M)
begin
	if(rising_edge(clk_160M)) then
		TTC_out <= TTC_out_t;
		Sim_TTCvi_st <= TTCvi_st;
	end if;
end process;	

process(clk_160M, sys_rst_n)
begin
	if(rising_edge(clk_160M)) then
		if(sys_rst_n = '0') then
			TTCvi_st <= idle0;
			TTC_out_t <= '1';
			trigger_rst <= '0';
			BCR_rst <= '0';
			ECR_rst <= '0';
			trigger_is <= '0';
			BCR_is <= '0';
			ECR_is <= '0';
			BCR_count_en <= '0';
		else

			if(trigger_r = '1') then
				trigger_is <= '1';
			elsif(trigger_rst = '1') then
				trigger_is <= '0';
			else 
				trigger_is <= trigger_is;
			end if;
	
			if(BCR_r = '1') then
				BCR_is <= '1';
			elsif(BCR_rst = '1') then
				BCR_is <= '0';
			else 
				BCR_is <= BCR_is;
			end if;
			if(ECR_r = '1') then
				ECR_is <= '1';
			elsif(ECR_rst = '1') then
				ECR_is <= '0';
			else 
				ECR_is <= ECR_is;
			end if;
			trigger_rst <= '0';
			BCR_rst <= '0';
			ECR_rst <= '0';
			case TTCvi_st is
				when idle0 => 	
					TTC_out_t <= not TTC_out_t;
					TTCvi_st <= idle1;
					BCR_count_en <= '1';
				when idle1 =>
					BCR_count_en <= '0';
						if(trigger_is = '1') then
							TTC_out_t <= not TTC_out_t;
							trigger_rst <= '1';		
						else
						    TTC_out_t <= TTC_out_t;
						end if;
					TTCvi_st <= idle2;
				when idle2 => 	
					TTC_out_t <= not TTC_out_t;
					TTCvi_st <= idle3;
				when idle3 => 	
					TTC_out_t <= not TTC_out_t;
					if(BCR_is = '1' or ECR_is = '1') then
						TTCvi_st <= DATABit0;
						 bit_count <= 0;
					else
						TTCvi_st <= idle0;
					end if;
				when DATABit0 =>	
					BCR_count_en <= '1';
					TTC_out_t <= not TTC_out_t;
					TTCvi_st <= DATABit1;
				when DATABit1 =>	
					BCR_count_en <= '0';
						if(trigger_is = '1') then
							TTC_out_t <= not TTC_out_t;
							trigger_rst <= '1';
						else
						    TTC_out_t <= TTC_out_t;
						end if;
					TTCvi_st <= DATABit2;
				when DATABit2 =>   
					TTC_out_t <= not TTC_out_t;
					TTCvi_st <= DATABit3;
				when DATABit3 =>   
					if(DATA2SEND(bit_count) = '1') then
						TTC_out_t <= not TTC_out_t;
					else
						TTC_out_t <= TTC_out_t;
					end if;
					if(bit_count /= 15) then
					    bit_count <= bit_count + 1;
						TTCvi_st <= DATABit0;
					else
						TTCvi_st <= idle0;
						BCR_rst <= '1';
						ECR_rst <= '1';
					end if;
				when others => NULL;
			end case;	
		end if;
	end if;
end process;	


--Check the H code

-- Check the decoder
d_br <= DATA2HMG;


p_hammingbits8 : process(d_br, hmg_br)
begin
  hmg_br(0) <= d_br(0) xor d_br(1) xor d_br(2) xor d_br(3);
  hmg_br(1) <= d_br(0) xor d_br(4) xor d_br(5) xor d_br(6);
  hmg_br(2) <= d_br(1) xor d_br(2) xor d_br(4) xor d_br(5) xor d_br(7);
  hmg_br(3) <= d_br(1) xor d_br(3) xor d_br(4) xor d_br(6) xor d_br(7); 
  hmg_br(4) <= hmg_br(0) xor hmg_br(1) xor hmg_br(2) xor hmg_br(3) xor d_br(0) xor d_br(1) xor d_br(2) xor 
            d_br(3) xor d_br(4) xor d_br(5) xor d_br(6) xor d_br(7); 
end process p_hammingbits8;
------------------------------------------------------------------------------------
s_br  <= hmg_br  xor HMG;                  



end architecture structure; 

