library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use xil_defaultlib.TTC709_pkg.all;

entity clk_8t49n242_i2c is
  port (
    clk             		: in     std_logic;
    iic_mux_reset 		    : out    std_logic;
    powerup_reset	        : out    std_logic;
    clk_8t49n242_control    : in     clk_8t49n242_reg_type;
    rd_en_pb	 			: in 	  std_logic;	
    wr_en_pb     			: in       std_logic;    
	sda       				: INOUT  STD_LOGIC;
	scl       				: INOUT  STD_LOGIC;
    clk_8t49n242_status    	: out    clk_8t49n242_reg_type);
end entity clk_8t49n242_i2c;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of clk_8t49n242_i2c is

  signal reset_n   :  STD_LOGIC;                   
  signal ena       :  STD_LOGIC;                   
  signal addr      :  STD_LOGIC_VECTOR(6 DOWNTO 0);
  signal rw        :  STD_LOGIC;                   
  signal data_wr   :  STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal busy      :  STD_LOGIC;                   
  signal data_rd   :  STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal ack_error :  STD_LOGIC;                   
  signal RESET_COUNT: STD_LOGIC_VECTOR(15 DOWNTO 0):= X"0000";
  signal RESET_MUX_COUNT: STD_LOGIC_VECTOR(31 DOWNTO 0):= X"00000000";

  component i2c_master_rd_wr_ver0
	GENERIC(
		input_clk : INTEGER := 50000000; --input clock speed from user logic in Hz
		bus_clk   : INTEGER := 400000);   --speed the i2c bus (scl) will run at in Hz
	PORT(
		clk       : IN     STD_LOGIC;                    --system clock
		reset_n   : IN     STD_LOGIC;                    --active low reset
		ena       : IN     STD_LOGIC;                    --latch in command
		addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
		rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
		data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
		busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
		data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
		ack_error : OUT    STD_LOGIC;                    --flag if improper acknowledge from slave
		sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
		scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
  end component i2c_master_rd_wr_ver0;


  component i2c_8t49n242_rd_wr
	PORT(
		clk       : IN     STD_LOGIC;                    --system clock
		reset_n   : IN     STD_LOGIC;                    --active low reset
		ena       : OUT    STD_LOGIC;                    --latch in command
		addr      : OUT    STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
		rw        : OUT    STD_LOGIC;                    --'0' is write, '1' is read
		rd_en_pb  : in 	   std_logic;	
		wr_en_pb  : in     std_logic;    
		data_wr   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
		busy      : IN     STD_LOGIC;                    --indicates transaction in progress
		data_rd   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
		ack_error : IN     STD_LOGIC);                    --flag if improper acknowledge from slave
  end component i2c_8t49n242_rd_wr;

component ila_test_rd_8t49n242
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe8 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
     probe9 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     probe10 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
 end component;



begin

ila_test_rd_8t49n242_inst0: ila_test_rd_8t49n242
  PORT map(
      clk => clk,
      probe0(0) => reset_n, 
      probe1(0) => ena,
      probe2(0) => busy,
      probe3(0) => rd_en_pb,
      probe4(0) => wr_en_pb,
      probe5(0) => rw,
      probe6(0) => busy,
      probe7(0) => ack_error,
      probe8 	=> addr,
      probe9 	=> data_wr,
      probe10 	=> data_rd
      );

  master_rd_wr: i2c_master_rd_wr_ver0
    generic map(
      input_clk => 50000000,
      bus_clk     => 400000)
    port map(
		clk        =>   clk, 
        reset_n    =>	reset_n,
        ena        =>	ena,
        addr       =>	addr,
        rw         =>	rw,
        data_wr    =>	data_wr,
        busy       =>	busy,
        data_rd    =>	data_rd,
        ack_error  =>	ack_error,
        sda        =>	sda,
        scl        =>	scl);

  master_8t49n242_wr_rd: i2c_8t49n242_rd_wr
    port map(
		clk        =>   clk, 
        reset_n    =>	reset_n,
        ena        =>	ena,
        addr       =>	addr,
        rw         =>	rw,
        rd_en_pb   =>   rd_en_pb,
        wr_en_pb   =>   wr_en_pb,
        data_wr    =>	data_wr,
        busy       =>	busy,
        data_rd    =>	data_rd,
        ack_error  =>	ack_error);

	PROCESS(CLK)
	BEGIN
	 IF(rising_edge(clk)) then	
		IF(RESET_COUNT /= X"FFFF") THEN
			RESET_COUNT <= RESET_COUNT + 1;
			reset_n <= '0';
		ELSE
			RESET_COUNT <= RESET_COUNT;
			reset_n <= '1';
		END IF; 
	END IF;	
   END PROCESS;
   powerup_reset <= not reset_n;
   
	PROCESS(CLK)
	BEGIN
	 IF(rising_edge(clk)) then	
		IF(ena = '1' ) THEN
			RESET_MUX_COUNT <= (OTHERS => '0');
			iic_mux_reset <= '1';
		ELSIF(RESET_MUX_COUNT /= X"00FFFFFF") THEN
			RESET_MUX_COUNT <= RESET_MUX_COUNT + 1;
			iic_mux_reset <= '1';
		ELSE
			RESET_MUX_COUNT <= RESET_MUX_COUNT;
			iic_mux_reset <= '0';
		END IF; 
	END IF;	
   END PROCESS;
 
   
end architecture structure ; -- of 8t49n242_i2c
