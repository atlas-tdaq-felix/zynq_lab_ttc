// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Fri Jan 17 11:12:44 2020
// Host        : mesfin-Precision-T3600 running 64-bit Ubuntu 18.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /mnt/ad/project/ET/Atlas/Atlas_Felix/mgebyehu/TTC_ZYNQ/firmware/sources/ip_cores/TTC_PICOZED30_BD/ip/TTC_PICOZED30_BD_TTC_FSM_0_0/TTC_PICOZED30_BD_TTC_FSM_0_0_stub.v
// Design      : TTC_PICOZED30_BD_TTC_FSM_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "TTC_FSM,Vivado 2018.1" *)
module TTC_PICOZED30_BD_TTC_FSM_0_0(clk_160M, sys_rst_n, ECR_r, triger_r, BCR_r, 
  TTC_out)
/* synthesis syn_black_box black_box_pad_pin="clk_160M,sys_rst_n,ECR_r,triger_r,BCR_r,TTC_out" */;
  input clk_160M;
  input sys_rst_n;
  input ECR_r;
  input triger_r;
  input BCR_r;
  output TTC_out;
endmodule
