--
-- VHDL Architecture rada_comp_lib.i2c_master_v01.arc
--
-- Created:
--          by - elis@(ELIS-WXP)
--          at - 15:30:47 01/03/2009
--
-- using Mentor Graphics HDL Designer(TM) 2008.1 (Build 17)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
Library UNISIM;
use UNISIM.vcomponents.all;

ENTITY i2c_master_rd_wr_ver0 IS
   GENERIC( 
      CLK_FREQ : natural := 25000000;
      BAUD     : natural := 100000
   );
   PORT( 
      --INPUTS
      sys_clk    : IN     std_logic;
      sys_rst    : IN     std_logic;
      start      : IN     std_logic;
      stop       : IN     std_logic;
      read       : IN     std_logic;
      write      : IN     std_logic;
      send_ack   : IN     std_logic;
      --######
      free       : OUT    std_logic;
      rec_ack    : OUT    std_logic;
      ready      : OUT    std_logic;
      core_state : OUT    std_logic_vector (5 DOWNTO 0);  --for debug purpose     
      write_state: IN     std_logic_vector(5 downto 0);

      --#######
      mstr_ctri_io : out    std_logic;
      mstr_cin_io : out    std_logic;
      mstr_cout_io : in    std_logic;
      mstr_dtri_io : out    std_logic;
      mstr_din_io : out    std_logic;
      mstr_dout_io : in    std_logic;
      mstr_din   : IN     std_logic_vector (7 DOWNTO 0);
      mstr_dout  : OUT    std_logic_vector (7 DOWNTO 0)
   );

-- Declarations

END i2c_master_rd_wr_ver0 ;

ARCHITECTURE arc OF i2c_master_rd_wr_ver0 IS
  
  constant FRAME     : natural := 11; -- number of bits in frame: start, stop, 8 bits data, 1 bit acknoledge
 -- constant BAUD      : natural := 100000;
  constant FULL_BIT  : natural := ( (CLK_FREQ / BAUD) - 1 ); -- / 2;  -- 249=(50000000/100000 -1)/2
  constant HALF_BIT  : natural := FULL_BIT / 2;                 -- 124=249/2
  constant GAP_WIDTH : natural := FULL_BIT * 4;                 -- 994=249*4

  signal i_free     : std_logic;
  signal i_ready    : std_logic;
  signal i_sda_mstr : std_logic;
  signal i_sca_mstr : std_logic;
  signal i_scl_cntr : natural;
  signal i_bit_cntr_mstr : natural range 0 to 7;
  signal i_ack_mstr : std_logic;
  signal i_mstr_rd_data : std_logic_vector( 7 downto 0 );
  
  signal i_mstr_din  : std_logic_vector( 7 downto 0 ); --latched address and data
  signal stm_mstr : std_logic_vector(5 downto 0):= "000000";
  signal ila_stm_mstr : std_logic_vector(5 downto 0):= "000000";
  constant mstr_idle 					: std_logic_vector(5 downto 0):= "00"& x"0";
  constant mstr_wait_st0 				: std_logic_vector(5 downto 0):= "00"& x"1";
  constant mstr_wait_st1 				: std_logic_vector(5 downto 0):= "00"& x"2";
  constant mstr_start_st0				: std_logic_vector(5 downto 0):= "00"& x"3";
  constant mstr_start_st1	    		: std_logic_vector(5 downto 0):= "00"& x"4";
  constant mstr_write_st0 		        : std_logic_vector(5 downto 0):= "00"& x"5";
  constant mstr_write_st1 				: std_logic_vector(5 downto 0):= "00"& x"6";
  constant mstr_ack_st0 				: std_logic_vector(5 downto 0):= "00"& x"7";
  constant mstr_ack_st1             	: std_logic_vector(5 downto 0):= "00"& x"8";
  constant mstr_stop_st0			 	: std_logic_vector(5 downto 0):= "00"& x"9";
  constant mstr_stop_st1 				: std_logic_vector(5 downto 0):= "00"& x"A";
  constant mstr_rd_st0      			: std_logic_vector(5 downto 0):= "00"& x"B";
  constant mstr_rd_st1 					: std_logic_vector(5 downto 0):= "00"& x"C";
  constant mstr_rd_ack_st0					: std_logic_vector(5 downto 0):= "00"& x"D";
  constant mstr_rd_ack_st1					: std_logic_vector(5 downto 0):= "00"& x"E";
  constant mstr_rd_ack_st2			 		: std_logic_vector(5 downto 0):= "00"& x"F";
  constant mstr_rd_ack_st3		 			: std_logic_vector(5 downto 0):= "01"& x"0";
  constant mstr_rd_ack_st4 				: std_logic_vector(5 downto 0):= "01"& x"1";
--  constant mstr_restart 				: std_logic_vector(5 downto 0):= "01"& x"2";
--  constant mstr_gap 					: std_logic_vector(5 downto 0):= "01"& x"3";
--  constant mstr_stop_1 					: std_logic_vector(5 downto 0):= "01"& x"4";
--  constant mstr_rd_wait_last_half 		: std_logic_vector(5 downto 0):= "01"& x"5";
--  constant mstr_restart_clk_high 		: std_logic_vector(5 downto 0):= "01"& x"6";
--  constant mstr_restart_clk_full 		: std_logic_vector(5 downto 0):= "01"& x"7";

  signal i_in_state : natural;
  signal i_sda_mstr_in : std_logic:= '0';
  signal i_sda_mstr_out : std_logic:= '0';
  signal i_sda_mstr_tri : std_logic:= '0';

  signal i_sca_mstr_in : std_logic:= '0';
  signal i_sca_mstr_out : std_logic:= '0';
  signal i_sca_mstr_tri : std_logic:= '0';

  signal ila_i_sda_mstr_in : std_logic:= '0';
  signal ila_i_sda_mstr_out : std_logic:= '0';
  signal ila_i_sda_mstr_tri : std_logic:= '0';

  signal ila_i_sca_mstr_in : std_logic:= '0';
  signal ila_i_sca_mstr_out : std_logic:= '0';
  signal ila_i_sca_mstr_tri : std_logic:= '0';
  signal i_sca_mstr_tri_n   : std_logic:= '0';
  signal i_sda_mstr_tri_n   : std_logic:= '0';

  signal i_mstr_dout   : std_logic:= '0';
  signal start_t   : std_logic:= '0';


component ila_debug_scl_sda
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(5 DOWNTO 0); 
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe12 : IN STD_LOGIC_VECTOR(5 DOWNTO 0));
 end component;
  
--component i2c_clk_div
--port
-- (clk_out1          : out    std_logic;
--  reset             : in     std_logic;
--  locked            : out    std_logic;
--  clk_in1           : in     std_logic);
--end component;

signal clk_out1 : std_logic; 
signal i2c_clk_reset   : std_logic:= '0';
  
BEGIN

--i2c_clk_div_inst: i2c_clk_div
--  PORT map(
--      clk_in1 => sys_clk,
--      reset 	=> i2c_clk_reset,
--      locked => open,
--      clk_out1 => clk_out1);


ila_i2c_scl_inst: ila_debug_scl_sda
  PORT map(
      clk => sys_clk, --clk_out1,
      probe0 	=> stm_mstr,
      probe1(0) => start,
      probe2(0) => stop,
      probe3(0) => write,
      probe4(0) => i_free,
      probe5(0) => i_ready,
      probe6(0) => i_sca_mstr_in,
      probe7(0) => i_sda_mstr_in,
      probe8(0) => i_sca_mstr_out,
      probe9(0) => i_sda_mstr_out,
      probe10(0) => i_sda_mstr_tri,
      probe11(0) => i_sca_mstr_tri,
      probe12 => write_state);
      

  mstr_cin_io <= i_sca_mstr_out;
  mstr_ctri_io <= i_sca_mstr_tri;
  i_sca_mstr_in <= mstr_cout_io;

  mstr_din_io <= i_sda_mstr_out;
  mstr_dtri_io <= i_sda_mstr_tri;
  i_sda_mstr_in <= mstr_dout_io;

  free    <= i_free;
  ready   <= i_ready;
  rec_ack <= i_ack_mstr;

-- Debug i2c state
  core_state <= stm_mstr;--conv_std_logic_vector( i_in_state , 6 );
  
-- Out signals
  
  i2c_master: 
  process( sys_clk , sys_rst )
    begin
      if ( sys_rst = '1' ) then
        stm_mstr   <= mstr_idle;
        i_free     <= '1';
        i_ready    <= '1';  

		i_sca_mstr_tri <= '1';
		i_sda_mstr_tri <= '1';

		i_sca_mstr_out <= '1';
		i_sca_mstr_out <= '1';

        i_scl_cntr <= 0;
        i_bit_cntr_mstr <= 7;

        i_ack_mstr <= '0';
		start_t <= '0';

--        i_mstr_dout <= ( others => '0' ); 
        i_mstr_din  <= ( others => '0' );   

      elsif rising_edge( sys_clk ) then      
       case stm_mstr is
          -------------------  
          when mstr_idle => 		-- 0 idle

			i_sca_mstr_tri <= '1';
			i_sda_mstr_tri <= '1';
	
			i_sda_mstr_out <= '1';
			i_sca_mstr_out <= '1';

            i_free <= '1';
            i_ready <= '1';

        	i_scl_cntr <= 0;
        	i_bit_cntr_mstr <= 7;
			
			if ( start = '1' ) then
				stm_mstr <= mstr_wait_st0;
				start_t <= '1';
			end if;
        	
          when mstr_wait_st0 => 		-- 1 
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
			i_sda_mstr_out <= '1';

            i_free <= '0';
            i_ready <= '1';
            
            i_bit_cntr_mstr <= 7;

            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_wait_st0;
            else  
				i_scl_cntr <= 0;
				stm_mstr <= mstr_wait_st1;
            end if;
            
          when mstr_wait_st1 => 		-- 2 
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= i_sca_mstr_out;
			i_sda_mstr_out <= i_sda_mstr_out;

            i_free <= '0';
            i_ready <= '1';

			if ( start_t = '1' ) then
				start_t <= '0';
				stm_mstr <= mstr_start_st0; 
			elsif ( write = '1' ) then
				i_mstr_din <= mstr_din;
				stm_mstr <= mstr_write_st0; 
--			elsif ( read = '1' ) then 
--				stm_mstr <= mstr_rd_wait_low;
			elsif ( stop = '1' ) then
				stm_mstr <= mstr_stop_st0;
            else
              stm_mstr <= mstr_wait_st1;
			end if;
          -------------------
          when mstr_start_st0 => -- 3 send start condition
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
			i_sda_mstr_out <= '1';

            i_free <= '0';          
            i_ready <= '0';   

            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_start_st0;
            else  
              i_scl_cntr <= 0;
              stm_mstr <= mstr_start_st1;
            end if;
          when mstr_start_st1=>  -- 4
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
			i_sda_mstr_out <= '0';

            i_free <= '0';          
            i_ready <= '0';   

            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_start_st1;
            else  
				i_scl_cntr <= 0;
				stm_mstr <= mstr_wait_st1;
            end if;
          --####################
          --##### WRITE ########
          --####################
          when mstr_write_st0 =>   -- 5
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '0';
		    i_sda_mstr_out <= i_mstr_din(i_bit_cntr_mstr);  -- no shift reg 

            i_free <= '0';          
            i_ready <= '0';   
			
			if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_write_st0;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_write_st1;
            end if;
          when mstr_write_st1 =>   --6
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
		    i_sda_mstr_out <= i_sda_mstr_out;

            i_free <= '0';          
            i_ready <= '0';   
            if ( i_scl_cntr < HALF_BIT ) then 
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_write_st1;
            else  
              i_scl_cntr <= 0;              
              if ( i_bit_cntr_mstr = 0 ) then
				i_bit_cntr_mstr <= 7;
                stm_mstr <= mstr_ack_st0;                
              else
                i_bit_cntr_mstr <= i_bit_cntr_mstr - 1;
                stm_mstr <= mstr_write_st0;
              end if;                                          
            end if;
          --------------------
          --####################
          --#### ACKNOWLEDGE ###
          --####################
          when mstr_ack_st0 =>  --7
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '1';
	
			i_sca_mstr_out <= '0';
		    i_sda_mstr_out <= 'Z';

            i_free <= '0';          
            i_ready <= '0';   

            i_ack_mstr <= '0'; 

            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_ack_st0;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_ack_st1;
            end if;
          --------------------
          when mstr_ack_st1 =>   --8
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '1';
	
			i_sca_mstr_out <= '1';
		    i_sda_mstr_out <= 'Z';

            i_free <= '0';          
            i_ready <= '0';   
		    
		    i_ack_mstr <= i_sda_mstr_in;

            i_scl_cntr <= 0;

            i_free <= '0';          

            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
				stm_mstr <= mstr_ack_st1;
            else
            
              i_scl_cntr <= 0;
				if(i_sda_mstr_in = '1') then
					stm_mstr <= mstr_wait_st1;
				else
					stm_mstr <= mstr_idle;
				end if;		
            end if;
--          --------------------  
--          --####################
--          --###### READ ########
--          --####################
--          when mstr_rd_st0 =>  --5
--			if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_rd_st0;
--            else                           
--              i_scl_cntr <= 0;
--              stm_mstr <= mstr_rd_st1;  
--                i_mstr_rd_data <= i_mstr_rd_data( 6 downto 0 ) & to_x01( i_sda_mstr_in ); --sda );
--			end if;
--          when mstr_rd_st1 =>   --13
--		    i_sca_mstr_out <= '1';
--		    i_sda_mstr_out <= 'Z';
--		    i_sda_mstr_tri <= '1';
--			if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_rd_st1;
--            else                           
----              i_mstr_rd_data <= i_mstr_rd_data( 6 downto 0 ) & to_x01( i_sda_mstr_in ); --sda );
--              i_scl_cntr <= 0;               
--              if ( i_bit_cntr_mstr > 0 ) then
--                i_bit_cntr_mstr <= i_bit_cntr_mstr - 1;
--                stm_mstr <= mstr_rd_st1;  
--              else
--                mstr_dout <= i_mstr_rd_data;               
--                stm_mstr <= mstr_rd_ack_st0;
--              end if;
--            end if;      
--          ---------------------
--          --#######################
--          --### SEND ACKNOWELEDGE #
--          --#######################
--          when mstr_rd_ack_st0 =>   --13
--		    i_sca_mstr_out <= '0';
--		    i_sda_mstr_out <= not send_ack;
--		    i_sda_mstr_tri <= '0';
--            if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_rd_ack_st0;
--            else                           
--              i_scl_cntr <= 0;
--              stm_mstr <= mstr_rd_ack_st1;
--            end if;
--          ----------------------              
--          when mstr_rd_ack_st1 =>   --14
--		    i_sda_mstr_out <= not send_ack;
--	        i_sca_mstr_out <= '0';
--	        i_scl_mstr_tri <= '0';
--            if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_ack_st1;
--            else
--              i_scl_cntr <= 0;
--              --i_ack_mstr <= sda;
--              stm_mstr <= mstr_rd_ack_st2; -- mstr_active;
--            end if;
--          when mstr_rd_ack_st2 =>   --14
--		    i_sca_mstr_out <= '1';
--		    i_sda_mstr_out <= i_sda_mstr_out;
--		    i_sda_mstr_tri <= '0';
--            if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_rd_ack_st2;
--            else
--              i_scl_cntr <= 0;
--              --i_ack_mstr <= sda;
--              stm_mstr <= mstr_rd_ack_st3;
--            end if;  
--          when mstr_rd_ack_st3 =>   --14
--		    i_sca_mstr_out <= '0';
--		    i_sda_mstr_out <= '0';
--		    i_sda_mstr_tri <= '0';
--            if ( i_scl_cntr < HALF_BIT ) then
--              i_scl_cntr <= i_scl_cntr + 1;
--              stm_mstr <= mstr_rd_ack_st3;
--            else
--              i_scl_cntr <= 0;
--              --i_ack_mstr <= sda;
--              stm_mstr <= mstr_write_st0;
--            end if;  
          ----------------------
          --######################
          --######## STOP ########
          --###################### 
          when mstr_stop_st0 =>   --17
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
		    i_sda_mstr_out <= '1';
		    
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_stop_st0;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_stop_st1;
            end if;
          when mstr_stop_st1 =>  --18
			i_sca_mstr_tri <= '0';
			i_sda_mstr_tri <= '0';
	
			i_sca_mstr_out <= '1';
		    i_sda_mstr_out <= '1'; --'0';


            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_stop_st1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_idle;
            end if;

          when others => stm_mstr <= mstr_idle;
          end case;       
      end if;
    end process i2c_master;
END ARCHITECTURE arc;
