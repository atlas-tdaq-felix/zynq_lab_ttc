#close_sim
#launch_simulation
#open_wave_config /home/mesfin/felix/mgebyehu/test_zynq_repo/zynq_lab_ttc/firmware/sources/ttc_felix_sim/test_wcfg.wcfg

add_force {/sim_ttc_output/clk_160M} -radix hex {0 0ns} {1 3125ps} -repeat_every 6250ps
add_force {/sim_ttc_output/sys_rst_n} -radix hex {0 0ns} {1 5000000ps}
add_force {/sim_ttc_output/ECR_push} -radix hex {0 0ns}  {1 10000000ps} {0 10100000ps}
add_force {/sim_ttc_output/BCR_push} -radix hex {0 0ns}  {1 12000000ps} {0 12100000ps}
add_force {/sim_ttc_output/triger_push} -radix hex {0 0ns} {1 13000000ps} {0 13100000ps} {1 15000100ps} {0 15001000ps}

add_force {/sim_ttc_output/L1A_PERIOD} -radix hex {0 0ns} {10000 14000000ps}
add_force {/sim_ttc_output/BCR_PERIOD} -radix hex {0 0ns} {00004 14000000ps} {00008 14100000ps}  {00002 14200000ps}

add_force {/sim_ttc_output/TTC_CONFIG} -radix hex {0 0ns} {2 15000000ps}

add_force {/sim_ttc_output/L1A_COUNT_IN} -radix hex {0 0ns} {20 5000000000ps}
add_force {/sim_ttc_output/LUT_CONT_EN} -radix hex {0 0ns} 
add_force {/sim_ttc_output/BUSY_EN} -radix hex {0 0ns} 
add_force {/sim_ttc_output/L1A_AUTO_HAL} -radix hex {0 0ns} 


run  30000000 ps
