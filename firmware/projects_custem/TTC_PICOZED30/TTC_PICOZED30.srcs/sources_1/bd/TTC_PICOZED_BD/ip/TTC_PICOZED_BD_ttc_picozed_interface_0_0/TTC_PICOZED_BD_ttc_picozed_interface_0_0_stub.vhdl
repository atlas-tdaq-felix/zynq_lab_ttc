-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
-- Date        : Tue Jul 26 12:10:16 2022
-- Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/mesfin/felix/mgebyehu/test_zynq_repo/zynq_lab_ttc/firmware/projects_custem/TTC_PICOZED30/TTC_PICOZED30.srcs/sources_1/bd/TTC_PICOZED_BD/ip/TTC_PICOZED_BD_ttc_picozed_interface_0_0/TTC_PICOZED_BD_ttc_picozed_interface_0_0_stub.vhdl
-- Design      : TTC_PICOZED_BD_ttc_picozed_interface_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030sbg485-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TTC_PICOZED_BD_ttc_picozed_interface_0_0 is
  Port ( 
    L1A_out : out STD_LOGIC;
    BCR_out : out STD_LOGIC;
    ECR_out : out STD_LOGIC;
    BCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    BUSY_in : in STD_LOGIC;
    L1A_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    LUT_CONT_EN : out STD_LOGIC;
    L1A_AUTO_HALT : out STD_LOGIC;
    L1A_AUTO_READY : in STD_LOGIC;
    BUSY_EN : out STD_LOGIC;
    BUSY_PULSE_COUNT : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );

end TTC_PICOZED_BD_ttc_picozed_interface_0_0;

architecture stub of TTC_PICOZED_BD_ttc_picozed_interface_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "L1A_out,BCR_out,ECR_out,BCR_PERIOD_out[31:0],OCR_PERIOD_out[31:0],L1A_PERIOD_out[31:0],TTC_CONFIG_out[31:0],L1A_COUNT_out[31:0],BUSY_in,L1A_COUNT[31:0],OCR_COUNT[31:0],LUT_CONT_EN,L1A_AUTO_HALT,L1A_AUTO_READY,BUSY_EN,BUSY_PULSE_COUNT[31:0],s00_axi_awaddr[5:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[5:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "ttc_picozed_interface_v1_0,Vivado 2018.1";
begin
end;
