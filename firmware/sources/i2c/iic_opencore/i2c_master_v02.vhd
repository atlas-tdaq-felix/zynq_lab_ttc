--
-- VHDL Architecture rada_comp_lib.i2c_master_v01.arc
--
-- Created:
--          by - elis@(ELIS-WXP)
--          at - 15:30:47 01/03/2009
--
-- using Mentor Graphics HDL Designer(TM) 2008.1 (Build 17)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
Library UNISIM;
use UNISIM.vcomponents.all;

ENTITY i2c_master_v01 IS
   GENERIC( 
      CLK_FREQ : natural := 25000000;
      BAUD     : natural := 100000
   );
   PORT( 
      --INPUTS
      sys_clk    : IN     std_logic;
      sys_rst    : IN     std_logic;
      start      : IN     std_logic;
      stop       : IN     std_logic;
      read       : IN     std_logic;
      write      : IN     std_logic;
      send_ack   : IN     std_logic;
      mstr_din   : IN     std_logic_vector (7 DOWNTO 0);
      --OUTPUTS
      sda        : INOUT  std_logic;
      scl        : INOUT  std_logic;
      free       : OUT    std_logic;
      rec_ack    : OUT    std_logic;
      ready      : OUT    std_logic;
      core_state : OUT    std_logic_vector (5 DOWNTO 0);  --for debug purpose
      mstr_dout  : OUT    std_logic_vector (7 DOWNTO 0)
   );

-- Declarations

END i2c_master_v01 ;

--
ARCHITECTURE arc OF i2c_master_v01 IS
  
  constant FRAME     : natural := 11; -- number of bits in frame: start, stop, 8 bits data, 1 bit acknoledge
 -- constant BAUD      : natural := 100000;
  constant FULL_BIT  : natural := ( CLK_FREQ / BAUD - 1 ); -- / 2;  -- 249=(50000000/100000 -1)/2
  constant HALF_BIT  : natural := FULL_BIT / 2;                 -- 124=249/2
  constant GAP_WIDTH : natural := FULL_BIT * 4;                 -- 994=249*4

  signal i_free     : std_logic;
  signal i_ready    : std_logic;
  signal i_sda_mstr : std_logic;
  signal i_scl_mstr : std_logic;
  signal i_scl_cntr : natural;
  signal i_bit_cntr_mstr : natural range 0 to 7;
  signal i_ack_mstr : std_logic;
  signal i_mstr_rd_data : std_logic_vector( 7 downto 0 );
  signal ila_i_mstr_rd_data : std_logic_vector( 7 downto 0 );
  
  signal i_mstr_ad  : std_logic_vector( 7 downto 0 ); --latched address and data
  signal ila_i_mstr_ad  : std_logic_vector( 7 downto 0 ); --latched address and data
  alias  fld_rd_wr  : std_logic is i_mstr_ad( 0 ); --1 - read, 0 - write

--  type i2c_master_state is ( 	mstr_idle, 
--  								mstr_start_cnt , 
--  								mstr_active , 
--  								mstr_wait_first_half , 
--  								mstr_wait_second_half ,
--                             	mstr_wait_full , 
--                             	mstr_wait_ack , 
--                             	mstr_wait_ack_second_half , 
--                             	mstr_wait_ack_third_half ,
--                             	mstr_wait_ack_fourth_half , 
--                             	mstr_rd_wait_low , 
--                             	mstr_rd_wait_half , 
--                             	mstr_rd_read , 
--                             	mstr_stop ,
--                             	mstr_rd_wait_ack_bit , 
--                             	mstr_rd_wait_ack , 
--                             	mstr_rd_get_ack , 
--                             	mstr_restart , 
--                             	mstr_gap , 
--                             	mstr_stop_1 ,
--                             	mstr_rd_wait_last_half , 
--                             	mstr_restart_clk_high );
--  signal stm_mstr : i2c_master_state;
	-- for debuging
  signal stm_mstr : std_logic_vector(5 downto 0):= "000000";
  signal ila_stm_mstr : std_logic_vector(5 downto 0):= "000000";
  constant mstr_idle 					: std_logic_vector(5 downto 0):= "00"& x"0";
  constant mstr_start_cnt0 				: std_logic_vector(5 downto 0):= "00"& x"1";
  constant mstr_start_cnt1 				: std_logic_vector(5 downto 0):= "00"& x"2";
  constant mstr_active 					: std_logic_vector(5 downto 0):= "00"& x"3";
  constant mstr_wait_first_half 		: std_logic_vector(5 downto 0):= "00"& x"4";
  constant mstr_wait_second_half 		: std_logic_vector(5 downto 0):= "00"& x"5";
  constant mstr_wait_full 				: std_logic_vector(5 downto 0):= "00"& x"6";
  constant mstr_wait_ack 				: std_logic_vector(5 downto 0):= "00"& x"7";
  constant mstr_wait_ack_second_half 	: std_logic_vector(5 downto 0):= "00"& x"8";
  constant mstr_wait_ack_third_half 	: std_logic_vector(5 downto 0):= "00"& x"9";
  constant mstr_wait_ack_fourth_half 	: std_logic_vector(5 downto 0):= "00"& x"A";
  constant mstr_rd_wait_low 			: std_logic_vector(5 downto 0):= "00"& x"B";
  constant mstr_rd_wait_half 			: std_logic_vector(5 downto 0):= "00"& x"C";
  constant mstr_rd_read 				: std_logic_vector(5 downto 0):= "00"& x"D";
  constant mstr_stop 					: std_logic_vector(5 downto 0):= "00"& x"E";
  constant mstr_rd_wait_ack_bit 		: std_logic_vector(5 downto 0):= "00"& x"F";
  constant mstr_rd_wait_ack 			: std_logic_vector(5 downto 0):= "01"& x"0";
  constant mstr_rd_get_ack 				: std_logic_vector(5 downto 0):= "01"& x"1";
  constant mstr_restart 				: std_logic_vector(5 downto 0):= "01"& x"2";
  constant mstr_gap 					: std_logic_vector(5 downto 0):= "01"& x"3";
  constant mstr_stop_1 					: std_logic_vector(5 downto 0):= "01"& x"4";
  constant mstr_rd_wait_last_half 		: std_logic_vector(5 downto 0):= "01"& x"5";
  constant mstr_restart_clk_high 		: std_logic_vector(5 downto 0):= "01"& x"6";
  constant mstr_restart_clk_full 		: std_logic_vector(5 downto 0):= "01"& x"7";

  
  signal i_in_state : natural;
  signal i_sda_mstr_in : std_logic:= '0';
  signal i_sda_mstr_in_d : std_logic:= '0';
  signal i_sda_mstr_out : std_logic:= '0';
  signal i_sda_mstr_tri : std_logic:= '0';

  signal i_scl_mstr_in : std_logic:= '0';
  signal i_scl_mstr_out : std_logic:= '0';
  signal i_scl_mstr_tri : std_logic:= '0';

  signal ila_i_sda_mstr_in : std_logic:= '0';
  signal ila_i_sda_mstr_out : std_logic:= '0';
  signal ila_i_sda_mstr_tri : std_logic:= '0';

  signal ila_i_scl_mstr_in : std_logic:= '0';
  signal ila_i_scl_mstr_out : std_logic:= '0';
  signal ila_i_scl_mstr_tri : std_logic:= '0';
  signal i_scl_mstr_tri_n   : std_logic:= '0';
  signal i_sda_mstr_tri_n   : std_logic:= '0';

component ila_debug_scl_sda
   PORT (
     clk :    IN STD_LOGIC;
     probe0 : IN STD_LOGIC_VECTOR(5 DOWNTO 0); 
     probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0));
 end component;
  
    
BEGIN

ila_i2c_scl_inst: ila_debug_scl_sda
  PORT map(
      clk => sys_clk,
      probe0 	=> ila_stm_mstr,
      probe1(0) => start,
      probe2(0) => stop,
      probe3(0) => read,
      probe4(0) => write);

--process(sys_clk)
--begin
--	if rising_edge(sys_clk) then
--      ila_i_sda_mstr_in		<= i_sda_mstr_in;
--      ila_i_sda_mstr_out	<= i_sda_mstr_out;
--      ila_i_sda_mstr_tri	<= i_sda_mstr_tri;
--      ila_i_scl_mstr_in		<= i_scl_mstr_in;
--      ila_i_scl_mstr_out	<= i_scl_mstr_out;
--      ila_i_scl_mstr_tri	<= i_scl_mstr_tri;
      ila_stm_mstr			<= stm_mstr;
--      ila_i_mstr_rd_data	<= i_mstr_rd_data;
--	  ila_i_mstr_ad			<= i_mstr_ad;
--	end if;
--end process;	

i_scl_mstr_tri_n <= i_scl_mstr_tri;
i_sda_mstr_tri_n <= i_sda_mstr_tri;
  IOBUF0_inst : IOBUF
--   generic map (
--      DRIVE => 12,
--      SLEW => "SLOW")
   port map (
      O => i_scl_mstr_in,     -- Buffer output
      IO => scl,   -- Buffer inout port (connect directly to top-level port)
      I => i_scl_mstr_out,     -- Buffer input
      T => i_scl_mstr_tri      -- 3-state enable input, high=input, low=output 
   );
  IOBUF1_inst : IOBUF
--   generic map (
--      DRIVE => 12,
--      SLEW => "SLOW")
   port map (
      O => i_sda_mstr_in,     -- Buffer output
      IO => sda,   -- Buffer inout port (connect directly to top-level port)
      I => i_sda_mstr_out,     -- Buffer input
      T => i_sda_mstr_tri      -- 3-state enable input, high=input, low=output 
   );
--  sda <= i_sda_mstr_out when i_sda_mstr_tri = '0' else 'Z';
--  i_sda_mstr_in  <= sda;
--  
--  scl <= i_scl_mstr_out when i_scl_mstr_tri = '0' else 'Z';
--  i_scl_mstr_in  <= scl;

--  sda     <= i_sda_mstr;
--  scl     <= i_scl_mstr;
  free    <= i_free;
  ready   <= i_ready;
  rec_ack <= not i_ack_mstr;
  i_sda_mstr_in_d <= i_sda_mstr_in;
  core_state <= stm_mstr;--conv_std_logic_vector( i_in_state , 6 );
  
  i2c_master: 
  process( sys_clk , sys_rst )
    begin
      if ( sys_rst = '1' ) then
        stm_mstr   <= mstr_idle;
        i_free     <= '0';
        i_ready    <= '0';  
--        i_sda_mstr <= 'Z';
		i_sda_mstr_out <= 'Z';
		i_sda_mstr_tri <= '1';
--        i_scl_mstr <= 'Z';
		i_scl_mstr_out <= 'Z';
		i_scl_mstr_tri <= '1';
        i_scl_cntr <= 0;
        i_bit_cntr_mstr <= 7;
        i_ack_mstr <= '1';
        i_mstr_rd_data <= ( others => '0' ); 
        mstr_dout  <= ( others => '0' ); 
        i_mstr_ad  <= ( others => '0' );   
        i_in_state <= 0;   
      elsif rising_edge( sys_clk ) then      
          case stm_mstr is
          -------------------  
          when mstr_idle => --0
			i_sda_mstr_out <= '0';
			i_sda_mstr_tri <= '0';
			i_scl_mstr_out <= '0';
			i_scl_mstr_tri <= '0';
            i_free <= '1';
            i_ready <= '1';
        	i_scl_cntr <= 0;
            if ( start = '1' ) then
              stm_mstr <= mstr_start_cnt0;
              i_free <= '0';          
            else
              stm_mstr <= mstr_idle;
            end if;
          -------------------
          when mstr_start_cnt0 => --1 send start condition

			i_sda_mstr_out <= '1';
			i_sda_mstr_tri <= '0';
			i_scl_mstr_out <= '1';
			i_scl_mstr_tri <= '0';

            i_ready <= '0';   
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_start_cnt0;
            else  
              i_scl_cntr <= 0;
              stm_mstr <= mstr_start_cnt1; --mstr_start_cnt1;
            end if;
          when mstr_start_cnt1 => --2

			i_sda_mstr_out <= '0';
			i_sda_mstr_tri <= '0';
			i_scl_mstr_out <= '1';
			i_scl_mstr_tri <= '0';

--            i_ready <= '0';   
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_start_cnt1;
            else  
              i_scl_cntr <= 0;
              stm_mstr <= mstr_active;
            end if;
          when mstr_active =>  --3

		    i_scl_mstr_out <= i_scl_mstr_out;
		    i_scl_mstr_tri <= i_scl_mstr_tri;
			i_sda_mstr_out <= i_sda_mstr_out;
			i_sda_mstr_tri <= i_sda_mstr_tri;

            i_ready <= '1';   
            i_bit_cntr_mstr <= 7;
            i_in_state <= 1;
            if ( read = '1' ) then 
              stm_mstr <= mstr_rd_wait_low;
              i_ready <= '0';
              i_in_state <= 3;
            elsif ( write = '1' ) then
              i_in_state <= 2;
              i_mstr_ad <= mstr_din;
              i_ready <= '0';
              stm_mstr <= mstr_wait_first_half; 
            elsif ( stop = '1' ) then
              i_in_state <= 4;
              i_ready <= '0';
              stm_mstr <= mstr_stop_1;
            elsif ( start = '1' ) then
              i_in_state <= 5;
              i_ready <= '0';
              stm_mstr <= mstr_restart;
            end if;
          --------------------
          --####################
          --##### WRITE ########
          --####################
          when mstr_wait_first_half =>   --4
		    
			i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= i_sda_mstr_out;
		    i_sda_mstr_tri <= '0';
            
			if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_wait_second_half;
            end if;
          when mstr_wait_second_half =>   --5
		    
			i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= i_mstr_ad( i_bit_cntr_mstr );                     
		    i_sda_mstr_tri <= '0';
			if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_wait_full;
            end if;
          ---------------------
          when mstr_wait_full =>   --5
			i_scl_mstr_out <= '1';
			i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= i_sda_mstr_out;
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < FULL_BIT ) then 
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_wait_full;
            else  
              i_scl_cntr <= 0;              
              if ( i_bit_cntr_mstr >= 1 ) then
                i_bit_cntr_mstr <= i_bit_cntr_mstr - 1;
                stm_mstr <= mstr_wait_first_half;
              elsif ( i_bit_cntr_mstr = 0 ) then
                --i_sda_mstr <= 'Z';              
                stm_mstr <= mstr_wait_ack;                
              end if;                                          
            end if;
          --------------------
          --####################
          --#### ACKNOWLEDGE ###
          --####################
          when mstr_wait_ack =>  --6
			  
			  i_scl_mstr_out <= '0';
			  i_scl_mstr_tri <= '0';
			  i_sda_mstr_out <= 'Z';
			  i_sda_mstr_tri <= '1';
              i_ack_mstr <= '1'; --i_sda_mstr_in;
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_wait_ack_second_half;
            end if;
          --------------------
          when mstr_wait_ack_second_half =>   --7
			  i_scl_mstr_out <= '0';
			  i_scl_mstr_tri <= '0';
			  i_sda_mstr_out <= 'Z';
			  i_sda_mstr_tri <= '1';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_wait_ack_third_half; --mstr_active;
            end if;               
          when mstr_wait_ack_third_half =>   --7

			  i_scl_mstr_out <= '1';
			  i_scl_mstr_tri <= '0';
			  i_sda_mstr_out <= 'Z';
			  i_sda_mstr_tri <= '1';
			if(i_sda_mstr_in_d = '0') then
			  i_ack_mstr <= i_sda_mstr_in_d;
			end if;  
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_wait_ack_fourth_half;
            end if;               
          when mstr_wait_ack_fourth_half =>
			  i_scl_mstr_out <= '0';
			  i_scl_mstr_tri <= '0';
			  i_sda_mstr_out <= '0';
			  i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_active;
            end if;     
          --------------------  
          --####################
          --###### READ ########
          --####################
          when mstr_rd_wait_low =>  --12
		    i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= 'Z';
		    i_sda_mstr_tri <= '1';
          if ( i_scl_cntr < FULL_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_wait_low;
            else                           
              i_scl_cntr <= 0;
              stm_mstr <= mstr_rd_wait_half;  
            end if;
          --------------------
          when mstr_rd_wait_half =>   --13

		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= 'Z';
		    i_sda_mstr_tri <= '1';

          if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_wait_half;
            else                           
              i_scl_cntr <= 0;
              stm_mstr <= mstr_rd_read;  
                i_mstr_rd_data <= i_mstr_rd_data( 6 downto 0 ) & to_x01( i_sda_mstr_in_d ); --sda );
          end if;
          when mstr_rd_read =>   --13
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= 'Z';
		    i_sda_mstr_tri <= '1';
			if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_read;
            else                           
--              i_mstr_rd_data <= i_mstr_rd_data( 6 downto 0 ) & to_x01( i_sda_mstr_in ); --sda );
              i_scl_cntr <= 0;               
              if ( i_bit_cntr_mstr > 0 ) then
                i_bit_cntr_mstr <= i_bit_cntr_mstr - 1;
                stm_mstr <= mstr_rd_wait_low;  
              else
                i_mstr_ad <= ( others => '0' );  
                mstr_dout <= i_mstr_rd_data;               
                stm_mstr <= mstr_rd_wait_ack;
              end if;
            end if;      
          ---------------------
          --#######################
          --### SEND ACKNOWELEDGE #
          --#######################
          when mstr_rd_wait_ack =>   --13
		    i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= not send_ack;
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_wait_ack;
            else                           
              i_scl_cntr <= 0;
              stm_mstr <= mstr_rd_get_ack;
            end if;
          ----------------------              
          when mstr_rd_get_ack =>   --14
		    i_sda_mstr_out <= not send_ack;
		    i_sda_mstr_tri <= '0';
	        i_scl_mstr_out <= '0';
	        i_scl_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_get_ack;
            else
              i_scl_cntr <= 0;
              --i_ack_mstr <= sda;
              stm_mstr <= mstr_rd_wait_ack_bit; -- mstr_active;
            end if;
          when mstr_rd_wait_ack_bit =>   --14
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= i_sda_mstr_out;
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_wait_ack_bit;
            else
              i_scl_cntr <= 0;
              --i_ack_mstr <= sda;
              stm_mstr <= mstr_rd_wait_last_half;
            end if;  
          when mstr_rd_wait_last_half =>   --14
		    i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= '0';
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_rd_wait_last_half;
            else
              i_scl_cntr <= 0;
              --i_ack_mstr <= sda;
              stm_mstr <= mstr_active;
            end if;  
          ----------------------
          --######################
          --######## STOP ########
          --###################### 
          when mstr_stop_1 =>   --17
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= '0';
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < HALF_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_stop_1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_stop;
            end if;
          ----------------------                                     
          when mstr_stop =>  --18
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= '1';
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < FULL_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_stop;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_gap;
            end if;                                                  
          ---------------------  
          when mstr_gap =>  --19
		    i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= '0';
		    i_sda_mstr_tri <= '0';
            if ( i_scl_cntr < GAP_WIDTH ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_gap;
            else
              i_scl_cntr <= 0;
              i_in_state <= 0;
              stm_mstr <= mstr_idle;
            end if;
          --#####################
          --###### RESTART ######        
          --#####################
          when mstr_restart =>  --20
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
		    i_sda_mstr_out <= '1';
		    i_sda_mstr_tri <= '0';
            i_ready <= '0';
            if ( i_scl_cntr < FULL_BIT ) then
              i_scl_cntr <= i_scl_cntr + 1;
              stm_mstr <= mstr_restart;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_restart_clk_high;
            end if;                      
          ----------------------
          when mstr_restart_clk_high =>  --21
		    i_scl_mstr_out <= '1';
		    i_scl_mstr_tri <= '0';
	        i_sda_mstr_out <= '0';
		    i_sda_mstr_tri <= '0';
            i_ready <= '0';
            if ( i_scl_cntr < FULL_BIT ) then
              stm_mstr <= mstr_restart_clk_high;
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_restart_clk_full;
            end if;                              
          when mstr_restart_clk_full =>  --21
		    i_scl_mstr_out <= '0';
		    i_scl_mstr_tri <= '0';
	        i_sda_mstr_out <= '0';
		    i_sda_mstr_tri <= '0';
            i_ready <= '0';
            if ( i_scl_cntr < FULL_BIT ) then
              stm_mstr <= mstr_restart_clk_full;
              i_scl_cntr <= i_scl_cntr + 1;
            else
              i_scl_cntr <= 0;
              stm_mstr <= mstr_active;
            end if;                              
          when others => stm_mstr <= mstr_idle;
          end case;       
      end if;
    end process i2c_master;
END ARCHITECTURE arc;
