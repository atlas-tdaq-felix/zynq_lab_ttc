#!/bin/sh

# 
# Vivado(TM)
# runme.sh: a Vivado-generated Runs Script for UNIX
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
# 

if [ -z "$PATH" ]; then
  PATH=/home/mesfin/tools/xilinx/SDK/2018.1/bin:/home/mesfin/tools/xilinx/Vivado/2018.1/ids_lite/ISE/bin/lin64:/home/mesfin/tools/xilinx/Vivado/2018.1/bin
else
  PATH=/home/mesfin/tools/xilinx/SDK/2018.1/bin:/home/mesfin/tools/xilinx/Vivado/2018.1/ids_lite/ISE/bin/lin64:/home/mesfin/tools/xilinx/Vivado/2018.1/bin:$PATH
fi
export PATH

if [ -z "$LD_LIBRARY_PATH" ]; then
  LD_LIBRARY_PATH=/home/mesfin/tools/xilinx/Vivado/2018.1/ids_lite/ISE/lib/lin64
else
  LD_LIBRARY_PATH=/home/mesfin/tools/xilinx/Vivado/2018.1/ids_lite/ISE/lib/lin64:$LD_LIBRARY_PATH
fi
export LD_LIBRARY_PATH

HD_PWD='/home/mesfin/felix/mgebyehu/TTC_ZYNQ/firmware/projects_custem/TTC_PICOZED30/TTC_PICOZED30.runs/i2c_clk_div_synth_1'
cd "$HD_PWD"

HD_LOG=runme.log
/bin/touch $HD_LOG

ISEStep="./ISEWrap.sh"
EAStep()
{
     $ISEStep $HD_LOG "$@" >> $HD_LOG 2>&1
     if [ $? -ne 0 ]
     then
         exit
     fi
}

EAStep vivado -log i2c_clk_div.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source i2c_clk_div.tcl
