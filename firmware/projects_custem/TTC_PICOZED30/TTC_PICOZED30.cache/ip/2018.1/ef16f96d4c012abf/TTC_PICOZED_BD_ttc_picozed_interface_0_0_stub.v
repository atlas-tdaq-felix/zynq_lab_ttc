// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.1 (lin64) Build 2188600 Wed Apr  4 18:39:19 MDT 2018
// Date        : Thu Jul 14 14:44:38 2022
// Host        : mesfin-V-P8H61E running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ TTC_PICOZED_BD_ttc_picozed_interface_0_0_stub.v
// Design      : TTC_PICOZED_BD_ttc_picozed_interface_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030sbg485-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ttc_picozed_interface_v1_0,Vivado 2018.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(L1A_out, BCR_out, ECR_out, BCR_PERIOD_out, 
  OCR_PERIOD_out, L1A_PERIOD_out, TTC_CONFIG_out, L1A_COUNT_out, BUSY_in, L1A_COUNT, OCR_COUNT, 
  LUT_CONT_EN, SPER_PORT, s00_axi_awaddr, s00_axi_awprot, s00_axi_awvalid, s00_axi_awready, 
  s00_axi_wdata, s00_axi_wstrb, s00_axi_wvalid, s00_axi_wready, s00_axi_bresp, 
  s00_axi_bvalid, s00_axi_bready, s00_axi_araddr, s00_axi_arprot, s00_axi_arvalid, 
  s00_axi_arready, s00_axi_rdata, s00_axi_rresp, s00_axi_rvalid, s00_axi_rready, 
  s00_axi_aclk, s00_axi_aresetn)
/* synthesis syn_black_box black_box_pad_pin="L1A_out,BCR_out,ECR_out,BCR_PERIOD_out[31:0],OCR_PERIOD_out[31:0],L1A_PERIOD_out[31:0],TTC_CONFIG_out[31:0],L1A_COUNT_out[31:0],BUSY_in,L1A_COUNT[31:0],OCR_COUNT[31:0],LUT_CONT_EN,SPER_PORT,s00_axi_awaddr[5:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[5:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,s00_axi_aclk,s00_axi_aresetn" */;
  output L1A_out;
  output BCR_out;
  output ECR_out;
  output [31:0]BCR_PERIOD_out;
  output [31:0]OCR_PERIOD_out;
  output [31:0]L1A_PERIOD_out;
  output [31:0]TTC_CONFIG_out;
  output [31:0]L1A_COUNT_out;
  input BUSY_in;
  input [31:0]L1A_COUNT;
  input [31:0]OCR_COUNT;
  output LUT_CONT_EN;
  output SPER_PORT;
  input [5:0]s00_axi_awaddr;
  input [2:0]s00_axi_awprot;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [1:0]s00_axi_bresp;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [5:0]s00_axi_araddr;
  input [2:0]s00_axi_arprot;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rvalid;
  input s00_axi_rready;
  input s00_axi_aclk;
  input s00_axi_aresetn;
endmodule
